#!/bin/bash

#./Release/xChat
#./backup_db.sh

ulimit -n 999999
echo 24000 > /proc/sys/kernel/threads-max
echo 24000 > /sys/fs/cgroup/pids/user.slice/user-0.slice/pids.max

result=1
while [ $result -ne 0 ]; do
    echo "`date +%d-%m-%Y_%H:%M:%S` Start loop_server<br />" >> error_log.txt
    ./Release/xChat
    result=$?
    echo "`date +%d-%m-%Y_%H:%M:%S` Server Error ${result}<br />" >> error_log.txt
    sleep 1
done
