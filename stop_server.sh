#/bin/bash
cd "$(dirname "$0")"

ps aux|grep restart.sh|awk '{print $2}'|xargs sudo kill -9
ps aux|grep loop_server.sh|awk '{print $2}'|xargs sudo kill -9
ps aux|grep xChat|awk '{print $2}'|xargs sudo kill -9

echo "server is stop"
