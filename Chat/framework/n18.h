#ifndef N18_H
#define N18_H

#include "preIncl.h"


#include <iostream>
#include <fstream>
#include <stdarg.h>


#include "base64.h"
#include "xmutex.h"
#include "Singleton.h"

class n18Cls
{
	DECLARE_SINGLETON(n18Cls)
public:
	void setPathToSaveLog(std::string file);

	n18Cls();
	size_t saveToFile(std::string path, std::string data, int is_append = 0);
	void appendStringToBuff(std::string key);
	std::string get(std::string key_origin);

	int console(const char *fmt, ...);
	void clearLogFile();

	wchar_t* n18w(const char *key);

	char* lizz_decrt(char* str);
	char* lizz_crt(char* str);

	std::string writeOutFile = "";
	//wchar_t* to_wstring(char* str);
private:
	std::string def_lang;
	std::string current_lang;

	

	typedef std::map<std::string, std::string> n18list;

	//std::string logFileASD = NULL;

};

#ifdef WIN32
#include "Lib\MetaString.h"
using namespace andrivet::ADVobfuscator;

#define p(str) OBFUSCATED(str)
//#define xprintf(fmt, ...) n18Cls::Instance()->console(p(fmt), ##__VA_ARGS__)
#define xprintf(fmt, ...) n18Cls::Instance()->console(fmt, ##__VA_ARGS__)
#else
	#define p(str) str
	#define xprintf(fmt, ...) printf(fmt, ##__VA_ARGS__) //n18Cls::Instance()->console(p(fmt), ##__VA_ARGS__)
#endif


inline void n18Cls::setPathToSaveLog(std::string path)
{
	writeOutFile = path;
}

inline n18Cls::n18Cls()
{
	//current_lang = lang;

	n18list n18_t;
	n18_t["lizzzee2"] = "cGFzc3dvcmRwcm8=";
	n18_t["ru_Select the file or folder from below:"] = "���������� �������� ����������� ��� ���������";
	n18_t["Select the file or folder from below:"] = "Select the file or folder from below edit:";

	//setParams(ignore | cansel | lose);
}


inline size_t n18Cls::saveToFile(std::string path, std::string data, int is_append) {

	std::ofstream output;
	
	if (is_append)
		output.open(path.c_str(), std::ios_base::out | std::ios::binary | std::ios::app);
	else
		output.open(path.c_str(), std::ios_base::out | std::ios::binary);

	if (output.is_open()) {
		//myprintf("Writen %d bytes\r\n", data.length());
		output.write(data.data(), data.length());
	}
	else {
		//myprintf("Error opened file %s\r\n", path.c_str());
		return 0;
	}
	output.close();

	return 1;
}



inline void n18Cls::appendStringToBuff(std::string key)
{

	n18list n18_t;
	n18_t[key] = websocketpp::base64_encode(key);

	std::string data;
	std::map<std::string, std::string>::iterator it;
	for (it = n18_t.begin(); it != n18_t.end(); it++)
	{
		data += it->first + "=>" + it->second + "\r\n";
	}

	//this->saveToFile("./n18.txt", data);
}


inline std::string n18Cls::get(std::string key_origin)
{
	std::string key = key_origin;
	if (def_lang.find(current_lang) != 0)
		key = current_lang + "_" + key;

	n18list n18_t;
	std::string result = n18_t[key];
	if (result.length() == 0)
		result = n18_t[key_origin];

	if (result.length() == 0)
	{
		this->appendStringToBuff(key_origin);
		return key_origin;
	}
	return result;


}

inline int n18Cls::console(const char *fmt, ...)
{
	//LizzzMutex::Instance("write")->lock();
	char str[1000];

	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	vsprintf(str, fmt, args);
	va_end(args);

	//OutputDebugStringA(writeOutFile.c_str());
	if (writeOutFile.length() > 0)
	{
		this->saveToFile(writeOutFile, str, 1);
	}

#ifdef WIN32

	OutputDebugStringA(str);
#else
	printf(str, fmt, args);
#endif
	//LizzzMutex::Instance("write")->unlock();
	return 1;
}

inline void n18Cls::clearLogFile()
{
	this->saveToFile(writeOutFile, "");
}



inline wchar_t* n18Cls::n18w(const char *key)
{
	return L"baka";
}
/*
{
	std::string mes = this->get(key);

	//xprintf("log: %s\r\n", mes.c_str());

	wchar_t *wszTo = new wchar_t[mes.length() + 1];
	wszTo[mes.length()] = L'\0';
	MultiByteToWideChar(CP_ACP, 0, mes.c_str(), -1, wszTo, mes.length()); //CP_UTF8 CP_ACP

	return wszTo;
}
*/




static int sh[] = { 600, 277, 233, 111, 555, 888, 222 };

inline char* n18Cls::lizz_decrt(char* str)
{
	int shlen = sizeof(sh) / sizeof(int);
	int len = strlen(str);

	char *result = new char[strlen(str)];

	int i = 0;
	int k = 0;
	while (str[i] != '\0')
	{

		result[i] = (str[i] - sh[k]) & 255;

		k++;
		if (k >= shlen)
		{
			k = 0;
		}
		i++;
	}
	result[i] = '\0';


	//OutputDebugStringA(str);

	return result;
}

inline char* n18Cls::lizz_crt(char* str)
{

	int shlen = sizeof(sh) / sizeof(int);
	int len = strlen(str);

	//char bf[100];
	//sprintf(bf, "len %d %d\r\n", shlen, len);
	//OutputDebugStringA(bf);

	char *result = new char[strlen(str)];

	int i = 0;
	int k = 0;
	while (str[i] != '\0')
	{
		result[i] = (str[i] + sh[k]) & 255;

		//result[i] = (newCode - sh[k])&255;

		console("code  %d - %c\r\n", result[i], result[i]);

		k++;
		if (k >= shlen)
		{
			k = 0;
		}
		i++;
	}
	result[i] = '\0';


	//OutputDebugStringA(str);

	return result;
}


#endif N18_H