#pragma once
#include "n18.h"
#include "Singleton.h"

#ifdef WIN32
#else
#include <sys/types.h>
#include <dirent.h>
#endif

#include <fstream>

class FileTemplate
{
	DECLARE_SINGLETON(FileTemplate)
public:

	int getdir(std::string dir, std::vector<std::string> &files);
	static std::string fileGetContents(std::string path);
	int reload();
	int loadScripts(std::string path);
	int setPath(std::string path);
	std::string getFile(std::string path);
	int file_put_contents(std::string file_name, std::string data);
    //uint64_t timeSinceEpochMillisec();

private:
	std::vector<std::string> list;

	typedef std::map<std::string, std::string> FILES_MAP;
	FILES_MAP fm;

	std::string dir;
};

	

inline int FileTemplate::getdir(std::string dir, std::vector<std::string> &files)
{
#ifdef WIN32


	std::string pattern(dir);
	pattern.append("\\*");

	std::wstring stemp = std::wstring(pattern.begin(), pattern.end());
	LPCWSTR sw = stemp.c_str();

	LPCWSTR path;


	//std::wcout << "patter " << sw << "\r\n";

	WIN32_FIND_DATA data;
	HANDLE hFind;
	int n = 0;
	if ((hFind = FindFirstFileW(sw, &data)) != INVALID_HANDLE_VALUE) {
		do {
			//data.cFileName
			LPCWSTR fileName = data.cFileName;
			//std::string name = data.cFileName;

			if (n >= 2) { // && data.dwFileAttributes == 32
				//std::wcout << fileName << "\r\n";


				std::wstring tempWstring(fileName);
				std::string tempString(tempWstring.begin(), tempWstring.end());

				files.push_back(tempString);
			}

			n++;

		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
#else
	DIR* dp;
	struct dirent* dirp;
	if ((dp = opendir(dir.c_str())) == NULL) {
		std::cout << "Error(" << errno << ") opening " << dir << std::endl;
		return errno;
	}

	int n = 0;
	while ((dirp = readdir(dp)) != NULL) {
		std::string file = std::string(dirp->d_name);
		//if(file.find(".js") != std::string::npos)printf("name %s\r\n", file.c_str());
		if (file.length() > 2)
			files.push_back(file);
	}
	closedir(dp);
#endif

	return 0;
}



inline std::string FileTemplate::fileGetContents(std::string path) {
	std::string data = "";
	char *buffer;

	int length = 0;
	std::ifstream input(path.c_str(), std::ios::binary);

	char buf[1024];
	//int length = 0;
	if (input) {
		// get length of file:
		input.seekg(0, input.end);
		length = input.tellg();
		input.seekg(0, input.beg);


		while (true) {
			int part = sizeof(buf);
			if (length < part) {
				part = length;
			}
			input.read(buf, part);
			data.append(buf, part);

			length -= part;
			if (length == 0) {
				break;
			}
		}

		return data;
	}

	return "";
}



inline int FileTemplate::reload()
{
	std::vector<std::string> fileList;
	this->getdir(this->dir, fileList);
	std::cout << "load files " << fileList.size() << "\r\n";

	for (int i = 0; i < fileList.size(); i++) {
		std::string fullPathToScript = this->dir + "/" + fileList[i];
		std::cout << fullPathToScript.c_str() << "\r\n";

		fm[fileList[i]] = fileGetContents(fullPathToScript);
	}


	return 1;
}

inline int FileTemplate::loadScripts(std::string path) {
	this->setPath(path);

	this->reload();

	//std::cout << "Dir " << path.c_str() << "\r\n";

	return 1;
}

inline int FileTemplate::setPath(std::string path) {
	dir = path;
	return 1;
}

//std::vector<std::string, std::string> files;

inline std::string FileTemplate::getFile(std::string path) {
	return fm[path];

	std::string mkPath = dir;
	//mkPath += "\\";
	mkPath += path;
	printf("Script %s\r\n", mkPath.c_str());
	//return "";
	return fileGetContents(mkPath).c_str();
}

inline int FileTemplate::file_put_contents(std::string file_name, std::string data) {
	FILE* fp;

	if (!(fp = fopen(file_name.c_str(), "w"))) {
		printf("Error writing to private key file");
	}

	fputs(data.data(), fp);

	fclose(fp);
	return 1;
}


