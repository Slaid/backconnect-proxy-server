#ifndef XANAL_H
#define XANAL_H

#include "preIncl.h"
#include "WS32_helper.h"
#include "safe_ptr.h"
#include <map>

#ifdef WIN32
#else
#define HANDLE pthread_t
#endif

class connection
{
public:
	int index;
	int socket;
	std::string socket_ip;
	int socket_port;

	std::string first_request;

	std::string hostname;
	std::string remote_ip;
	int remote_port;
	int sremote;

	int lock;
	int is_auth;
	int isClosed;

	int proxyType;

	int status;
	int state;
	int is_bot;
	int max_threads;

	connection* child;

	HANDLE pt;
	volatile int done;

	sockaddr_in saddr;

	std::map<std::string, std::string> attrList;
	//attrList attr_list;
	void setAttribute(std::string key, std::string val)
	{
		attrList[key] = val;
	}

	std::string getAttribute(std::string key)
	{
		return attrList[key];
	}

	std::vector<std::string> getAttributeList()
	{
		std::vector<std::string> result;

		std::string line;
		for (auto it = attrList.begin(); it != attrList.end(); ++it)
		{
			line = it->first + ": " + it->second;
			result.push_back(line);
		}
		return result;
	}

};


class ConnectionManager
{
public:
	static ConnectionManager* Instance(std::string name);
	std::string currentIndex;
	
	connection* addConnection(connection *con);
	//connection* getConnection(int fd = 0);

	//int addConnection(connection** con);
	int closeConnection(connection* con);
	int closeConnection(int socket);
	//connection** getConnection(int socket = 0);
	//int countConnection();

	std::vector<std::string> dumpConnectionList(int group = 0);
	sf::safe_ptr< std::map< int, connection* > > connection_list_safe;
	
	std::map< int, connection* > connection_list;

	connection* getConnectionByFilter();

	int connection_size = 0;
};




typedef std::map<std::string, ConnectionManager*> managerList;
static managerList manager_list;


// ��������� � ����������� �� ����������

//typedef std::map<int, connection*> connectionList;
//connectionList connection_list_SMD;



//inline int ConnectionManager::countConnection()
//{
//	(*connection_list_safe).size();
//	return connection_list_SMD.size();
//}

inline ConnectionManager* ConnectionManager::Instance(std::string name)
{
	

	if (manager_list[name])
	{
		return manager_list[name];
	}

	printf("Declare sigleton manager %s\r\n", name.c_str());
	ConnectionManager* clr = new ConnectionManager;

	clr->currentIndex = name;

	manager_list[name] = clr;

	return clr;
}

/*
inline connection* ConnectionManager::getConnection(int fd)
{
	//if (connection_list_SMD[fd])
	//{
	//	return connection_list_SMD[fd];
	//}

	if ((*connection_list_safe)[fd])
	{
		return (*connection_list_safe)[fd];
	}

	return NULL;
}
*/

inline connection* ConnectionManager::addConnection(connection *con)
{
	//
	//if (connection_list[fd])
	//{
	//	delete connection_list[fd];
	//	connection_list.erase(fd);
	//	
	//}

	//connection_list[fd] = new connection();
	//connection_list[fd]->socket = fd;
	//connection *tmp = new connection();

	

	//connection *con = new connection();
	//con->socket = socket;

	
	//LizzzMutex::Instance("connection")->lock();
	(*connection_list_safe)[con->socket] = con;
	connection_size++;
	//LizzzMutex::Instance("connection")->unlock();



	//printf("Accept %s %d pull %d\r\n", con->socket_ip.c_str(), con->socket, connection_size);
	

	//printf("addr %d\r\n", con);
	return con;
}

/*
inline connection* ConnectionManager::addConnection(int fd)
{
	if ((*connection_list_safe)[fd])
	{

		//delete connection_list_SMD[fd];
		//connection_list_SMD.erase(fd);

		delete (*connection_list_safe)[fd];
		connection_list_safe->erase(fd);
	}

	//connection_list_SMD[fd] = new connection();
	//connection_list_SMD[fd]->socket = fd;
	//printf("memory %d\r\n", &connection_list_SMD[fd]);

	//connection_list_safe->emplace(fd, new connection());

	connection *con = new connection();
	con->socket = fd;

	(*connection_list_safe)[fd] = con;

	//for (auto it = connection_list_safe->begin(); it != connection_list_safe->end(); ++it)
	//{
	//	std::cout << it->first << " => " << it->second->socket << ", ";
	//}


	//(*connection_list_safe)[fd]->socket = fd;

	//printf("Dta: %d\r\n", (*connection_list_safe)[fd]->socket);
	
	return (*connection_list_safe)[fd];
}
*/

inline connection* ConnectionManager::getConnectionByFilter()
{
	
	LizzzMutex::Instance("get_connection")->lock();
	for (auto it = connection_list_safe->begin(); it != connection_list_safe->end(); ++it)
	{
		connection* con = it->second;
		if (con && con->is_bot && con->status == 0)
		{
			LizzzMutex::Instance("get_connection")->unlock();
			return con;
		}
	}

	LizzzMutex::Instance("get_connection")->unlock();
	return NULL;
}


inline int ConnectionManager::closeConnection(connection* con)
{
	
	if (con == NULL)
		return 1;

	//

	ServerHelper::Close(con->socket);

	//printf("Size %d - %d\r\n", connection_size, connection_list_safe->size());

	int socket = con->socket;

	

	//LizzzMutex::Instance("connection")->lock();
	connection_list_safe->erase(socket);
	connection_size--;
	//LizzzMutex::Instance("connection")->unlock();

	printf("Close connection %d pull %d\r\n", socket, connection_size);
	
	delete con;
	
	
	//printf("Memory clean\r\n");

	
	return 1;
}


inline int ConnectionManager::closeConnection(int socket)
{
	
	if (socket <= 0)
		return 1;

	//LizzzMutex::Instance("close")->lock();

	connection* con = (*connection_list_safe)[socket];
	if (!con) {
		return 1;
	}
		
	ServerHelper::Close(socket);
	//printf("e");

	connection_list_safe->erase(socket);
	connection_size--;
	//connection_list_SMD.erase(socketTmp);
	

	printf("Thread end %d size %d ", socket, connection_list_safe->size());


	//LizzzMutex::Instance("close")->unlock();
	
	return 1;
}



inline std::vector<std::string> ConnectionManager::dumpConnectionList(int group)
{
	
	std::vector<std::string> result;
	result.clear();

	
	LizzzMutex::Instance("dump")->lock();
	for (auto it = connection_list_safe->begin(); it != connection_list_safe->end(); ++it)
	{
		std::string line = "";
		std::vector<std::string> attrs = it->second->getAttributeList();
		for (int i = 0; i < attrs.size(); i++)
		{
			line += attrs[i] +  "\r\n";;
		}
		
		result.push_back(line);
		//std::cout << it->first << " => " << it->second->socket << ", ";
	}
	LizzzMutex::Instance("dump")->unlock();
	/*

	std::map<int, connection*>::iterator cl;

	//printf("dump size %d\r\n", connection_list_SMD.size());
	for (cl = connection_list_safe->begin(); cl != connection_list_safe.end(); cl++)
	{
		connection* con = cl->second;
		int socket = cl->first;
		//printf("socket %d = %d\r\n", socket, con->socket);

		result.push_back(con);
		//if ((*con)->socket == socket)
		//{
		//	return con;
		//}
	}
	*/
	return result;
}



#endif XANAL_H