#ifndef XANAL_H
#define XANAL_H

#include "preIncl.h"
#include "WS32_helper.h"
//#include "safe_ptr.h"
#include <map>

#ifdef WIN32
#else
#define HANDLE pthread_t
#endif

#include "../TrafficAnalize.h"

class connection
{
public:
	int index;
	int socket;
	std::string socket_ip;
	int socket_port;

	std::string key;

	std::string first_request;


	//int is_auth;
	int isClosed;


	int status;
	int state; //for socketAuth state
	int is_bot;
	//int max_threads;
	int countTraffic;

	connection* revers;


	//sockaddr_in saddr;

	void setIpPort(std::string ip, int port)
    {
        this->socket_ip = ip;
        this->socket_port = port;
        this->key = ip + ":" + utils::xitoa(port, 10);
    }

	std::map<std::string, std::string> attrList;
	//attrList attr_list;
	void setAttribute(std::string key, std::string val)
	{
		attrList[key] = val;
	}

	std::string getAttribute(std::string key)
	{
		return attrList[key];
	}

	std::vector<std::string> getAttributeList()
	{
		std::vector<std::string> result;

		std::string line;
		for (auto it = attrList.begin(); it != attrList.end(); ++it)
		{
			line = it->first + ": " + it->second;
			result.push_back(line);
		}
		return result;
	}

    connection()
    {
		index = -1;
		socket = -1;
		socket_ip = "";
		socket_port = 0;

		std::string key = "";

		std::string first_request = "";

		//int is_auth = 0;
		int isClosed = 0;


		int status = 0;
		int state = 0; //for socketAuth state
		int is_bot = 0;
		//int max_threads = 0;
		int countTraffic = 0;

		connection* revers = NULL;


		//sockaddr_in saddr;
    }

	~connection()
	{
		TrafficAnalize::Instance()->appendTrafficIP(this->socket_ip, this->countTraffic);
	}
	
	void appendTraffic(int bytes)
    {
		countTraffic += bytes;
    }

};


class ConnectionManager
{
public:
	static ConnectionManager* Instance(std::string name);
	std::string currentIndex;
	
	connection* addConnection(connection *con);
	connection* getConnection(int socket);
	int removeConnection(int socket);

	//int addConnection(connection** con);
	//int closeConnection(connection* con);
	int closeConnection(int socket);
	//connection** getConnection(int socket = 0);
	//int countConnection();

	std::vector<std::string> dumpConnectionList(int group = 0);
	//sf::safe_ptr< std::map< int, connection* > > connection_list_safe;
	
	std::map< int, connection* > connection_list;

	connection* getConnectionByFilter();

	int connection_size = 0;
};




typedef std::map<std::string, ConnectionManager*> managerList;
static managerList manager_list;


// ��������� � ����������� �� ����������

//typedef std::map<int, connection*> connectionList;
//connectionList connection_list_SMD;



//inline int ConnectionManager::countConnection()
//{
//	(*connection_list_safe).size();
//	return connection_list_SMD.size();
//}

inline ConnectionManager* ConnectionManager::Instance(std::string name)
{
	

	if (manager_list[name])
	{
		return manager_list[name];
	}

	printf("Declare sigleton manager %s\r\n", name.c_str());
	ConnectionManager* clr = new ConnectionManager;

	clr->currentIndex = name;

	manager_list[name] = clr;

	return clr;
}


inline connection* ConnectionManager::getConnection(int socket)
{
	LizzzMutex::Instance("connection")->lock();
	connection* con = NULL;
	if (connection_list[socket])
	{
		con = connection_list[socket];
	}
	LizzzMutex::Instance("connection")->unlock();

	return con;
}

inline connection* ConnectionManager::addConnection(connection *con)
{
	//
	//if (connection_list[fd])
	//{
	//	delete connection_list[fd];
	//	connection_list.erase(fd);
	//	
	//}

	//connection_list[fd] = new connection();
	//connection_list[fd]->socket = fd;
	//connection *tmp = new connection();

	

	//connection *con = new connection();
	//con->socket = socket;

	
	LizzzMutex::Instance("connection")->lock();
	connection_list[con->socket] = con;
	//(*connection_list_safe)[con->socket] = con;
	//connection_size++;
	LizzzMutex::Instance("connection")->unlock();
	


	//printf("Accept %s %d pull %d\r\n", con->socket_ip.c_str(), con->socket, connection_size);
	

	//printf("addr %d\r\n", con);
	return con;
}

/*
inline connection* ConnectionManager::addConnection(int fd)
{
	if ((*connection_list_safe)[fd])
	{

		//delete connection_list_SMD[fd];
		//connection_list_SMD.erase(fd);

		delete (*connection_list_safe)[fd];
		connection_list_safe->erase(fd);
	}

	//connection_list_SMD[fd] = new connection();
	//connection_list_SMD[fd]->socket = fd;
	//printf("memory %d\r\n", &connection_list_SMD[fd]);

	//connection_list_safe->emplace(fd, new connection());

	connection *con = new connection();
	con->socket = fd;

	(*connection_list_safe)[fd] = con;

	//for (auto it = connection_list_safe->begin(); it != connection_list_safe->end(); ++it)
	//{
	//	std::cout << it->first << " => " << it->second->socket << ", ";
	//}


	//(*connection_list_safe)[fd]->socket = fd;

	//printf("Dta: %d\r\n", (*connection_list_safe)[fd]->socket);
	
	return (*connection_list_safe)[fd];
}
*/

inline connection* ConnectionManager::getConnectionByFilter()
{
	
	LizzzMutex::Instance("connection")->lock();
	connection* result = NULL;
	for (auto it = connection_list.begin(); it != connection_list.end(); ++it)
	{
		connection* con = it->second;
		if (!con)
		{
			removeConnection(it->first);
			continue;
		}

		if (con->is_bot && con->status == 0)
		{
			result = con;
			break;
		}
	}

	LizzzMutex::Instance("connection")->unlock();
	return result;
}

/*
inline int ConnectionManager::closeConnection(connection* con)
{
	
	if (con == NULL)
		return 1;

	//

	ServerHelper::Close(con->socket);

	//printf("Size %d - %d\r\n", connection_size, connection_list_safe->size());

	int socket = con->socket;

	
	LizzzMutex::Instance("connection")->lock();
	connection_list.erase(socket);
	connection_size--;
	LizzzMutex::Instance("connection")->unlock();

	printf("Close connection %d pull %d\r\n", socket, connection_size);
	
	delete con;
	
	
	//printf("Memory clean\r\n");

	
	return 1;
}
*/

inline int ConnectionManager::removeConnection(int socket)
{
	LizzzMutex::Instance("connection")->lock();
	connection_list.erase(socket);
	//connection_size--;

	printf("Thread end %d size %d\r\n", socket, connection_list.size());
	LizzzMutex::Instance("connection")->unlock();
	return 1;
}

inline int ConnectionManager::closeConnection(int socket)
{
	
	if (socket <= 0)
		return 1;

	connection* con = getConnection(socket);
	if (con) {

		ServerHelper::Close(socket);
		removeConnection(socket);
		return 1;
	}
		
	
	return 0;
}



inline std::vector<std::string> ConnectionManager::dumpConnectionList(int group)
{
	
	std::vector<std::string> result;
	
	result.clear();

	
	LizzzMutex::Instance("dump")->lock();
	for (auto it = connection_list.begin(); it != connection_list.end(); ++it)
	{
		std::string line = "";
		connection* con = it->second;

		if (con)
		{
			std::vector<std::string> attrs = con->getAttributeList();
			for (int i = 0; i < attrs.size(); i++)
			{
				line += attrs[i] + "\r\n";;
			}

			result.push_back(line);
		}
		else {
			printf("Connection *NULL socket %d\r\n", it->first);
		}
		//std::cout << it->first << " => " << it->second->socket << ", ";
	}
	LizzzMutex::Instance("dump")->unlock();
	

	
	return result;
}



#endif XANAL_H