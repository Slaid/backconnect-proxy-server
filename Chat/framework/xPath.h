#pragma once

#include "preIncl.h"
#include <string>
#include "Singleton.h"


class xPath
{
	DECLARE_SINGLETON(xPath)
public:
	
	//xPath();
	std::string getExePath();
	int findSymbolMirror(std::string &str, char split);
};

inline int xPath::findSymbolMirror(std::string &str, char split) {
	//std::cout << str;
	for (int i = str.length(); i >= 0; i--) {
		if (str[i] == split) {
			return i;
		}
	}
	return -1;
}

inline std::string xPath::getExePath() {

#ifdef WIN32

	char ownPth[MAX_PATH];

	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule == NULL)
	{
		printf("Executable path NULL\r\n");
		return "";

	}

	// When passing NULL to GetModuleHandle, it returns handle of exe itself
	GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));

	// Use above module handle to get the path using GetModuleFileName()
	std::string pathS = ownPth;


	int pos = findSymbolMirror(pathS, '\\');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		pathS = pathS.substr(0, pos);
	}

	//printf("path %s\r\n", pathS.c_str());
	/*
	if (merge.length() > 0) {
	pathS += "\\";
	pathS.append(merge.c_str(), merge.length());
	}
	printf("Usage %s\r\n", pathS.c_str());
	*/
	return pathS;
	
#else
	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string toRunable = std::string(result, (count > 0) ? count : 0);

	int pos = findSymbolMirror(toRunable, '/');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		toRunable = toRunable.substr(0, pos);
	}

	return toRunable;
#endif


}
