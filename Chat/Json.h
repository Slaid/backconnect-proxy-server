#pragma once
#include "framework/preIncl.h"
#include "framework/n18.h"
#include "framework/rapidjson/document.h"
using namespace rapidjson;

class JSON
{
private:
	Document DOM;
public:
	JSON(std::string str)
	{
		if (str.length() > 0)
		{
			if (DOM.Parse(str.c_str()).HasParseError()) {
				xprintf("Error parse post data");
			}
		}
		else {
			if (DOM.Parse("{}").HasParseError()) {
				xprintf("Error parse post data");
			}
		}
	}

	void set(std::string key, std::string val)
	{
		if (!DOM.HasMember(key.c_str())) {
			Value uid;
			uid.SetString(val.data(), val.length(), DOM.GetAllocator());
			Value newKey(key.c_str(), DOM.GetAllocator());

			DOM.AddMember(newKey, uid, DOM.GetAllocator());
		}
		else {
			DOM[key.c_str()].SetString(val.data(), DOM.GetAllocator());
		}
	}

	std::string get(std::string key) {
		if (!DOM.HasMember(key.c_str())) {
			return "";
		}
		else {
			return DOM[key.c_str()].GetString();
			
		}
	}
};