#include "framework/Singleton.h"
#include "framework/xThread.h"
#include "Object.h"
#include "sblist.h"
#include "Revers_proxy.h"
#include "TrafficAnalize.h"

enum socksstate {
    SS_1_CONNECTED,
    SS_2_NEED_AUTH,
    SS_3_AUTHED,
};

//struct client {
 //   struct sockaddr_in addr;
 //   int port;
 //   int fd;
//};

struct thread {
    HANDLE pt;
    //struct client client;
    enum socksstate state;
    volatile int  done;
    int port;
    struct sockaddr_in addr;
    int fd;
};

#ifdef WIN32
#else
#define HANDLE pthread_t
#endif


#ifndef MAX
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif
#undef PTHREAD_STACK_MIN
#define PTHREAD_STACK_MIN 64*1024

class ManagerPorts
{
    DECLARE_SINGLETON(ManagerPorts)
public:
    ManagerPorts();

    int bind_port(struct ServerObject* port_t);
    int close_port(int fd);

    int addPort(int port, int user_id);
    int removePort(int port, int user_id);
    int looper();

    std::map< int, ServerObject* > port_status;

private:
    pthread_mutex_t mutex;
};

#pragma once

static void* clientthread(void *data) {
    connection *con = (connection*)data;


    
    ReversProxy* proxy = new ReversProxy();
    proxy->Start(con);
    delete proxy;
    
    ConnectionManager::Instance()->sb_erase(con->socket);
    con->clear();
    

    //printf("Close con %d\r\n", con->socket);
    con->done = 1;
    delete con;

    return 0;
}

//static int server_waitclient(int server_fd, struct client* client) {
//    socklen_t clen = sizeof client->addr;
//    return ((client->fd = accept(server_fd, (struct sockaddr *)&client->addr, &clen)) == -1)*-1;
//}
/*
static void collect(sblist *threads) {

    size_t i;
    //int n = sblist_getsize(threads); //Даже не пытайтесь нарушите всю логику
    //printf("Thread pull size %d\r\n", n);
    for(i=0;i<sblist_getsize(threads);) {
        connection *con = *((connection**)sblist_get(threads, i));

        if(con->done) {
            joinThread(con->ptr);
            sblist_delete(threads, i);
            //delete con;
        } else
            i++;
    }
    printf("[%d]Thread pull size\r\n", i);
}
*/

static void* create_server(void *data)
{
    struct ServerObject *serv = (struct ServerObject*)data;
    //ServerObject* serv = (ServerObject*)lpParameter;
    printf("Bind %d\r\n", serv->port);
    serv->fd = -1;
 

    serv->fd = socket(AF_INET, SOCK_STREAM, 0);
    if (serv->fd < 0)
    {
        printf("Error create server socket\r\n", serv->port);
        return 0;
    }

    //#ifdef LINUX
    //https://stackoverflow.com/questions/47179793/how-to-gracefully-handle-accept-giving-emfile-and-close-the-connection

    int enable = 1;
    int opt = TRUE;
    if (setsockopt(serv->fd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(int)) < 0)
        printf("setsockopt(SO_REUSEADDR) failed");
    else
    {
        //printf("Overbind enable\r\n");
    }

    //#endif

    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof serv_addr);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(serv->port);


    if (bind(serv->fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Error bind on: %d port\r\n", serv->port);
        close(serv->fd);
        return 0;
    }

    if (listen(serv->fd, SOMAXCONN) < 0)
    {
        printf("Error listen on: %d port\r\n", serv->port);
        return 0;
    }


    //sblist *threads = sblist_new(sizeof (struct thread*), 8);
    //size_t stacksz = MAX(8192, PTHREAD_STACK_MIN);

    while(1) {
        //collect(threads);
        //struct client c;
        //c.port = serv->port;
        //struct thread *curr = (struct thread*) malloc(sizeof (struct thread));
        //if(!curr) goto oom;

        connection *con = new connection();

        con->socket_port = serv->port;
        con->done = 0;

        socklen_t clen = sizeof con->addr;
        con->socket = accept(serv->fd, (struct sockaddr *)&con->addr, &clen);

        if(con->socket <= 0)
        {
            //printf("Listener down %d\r\n", serv->fd);
            //break;
            continue;
        }

        //printf("Mem %d kb\r\n", getValue() / 1024);

        //curr->client = c;
        /*
        if(!sblist_add(threads, &con)) {
            closesocket(con->socket);
            delete con;
            //oom:
            printf("rejecting connection due to OOM\n");
            utils::xsleep(1);//  usleep(16); 
            exit(0);
            continue;
        }
        */

        sprintf(con->socket_ip, "%s", inet_ntoa(con->addr.sin_addr));
        printf("Accept socket %d %s:%d ip_len %d\r\n", con->socket, con->socket_ip, con->socket_port, strlen(con->socket_ip));
        sprintf(con->key, "%s:%i", con->socket_ip, con->socket_port);
        ConnectionManager::Instance()->add(con);

        con->ptr = createThread((LPTHREAD_START_ROUTINE)clientthread, con);
        
        if (!con->ptr)
        {
            printf("CreateThread failed. OOM?\n");
        }
 

    }

    closesocket(serv->fd);

    /*
    printf("Port closed %d\r\n", serv->port);
    for(int i=0;i<sblist_getsize(threads);i++) {
        struct thread* thread = *((struct thread**)sblist_get(threads, i));
        
        shutdown(thread->fd, SHUT_RDWR);
        thread->done = 1;
    }
    serv->is_created = 0;

    */
    printf("Port cleaned %d\r\n", serv->port);
    //delete serv;

    return 0;
}

inline ManagerPorts::ManagerPorts()
{
    pthread_mutex_init(&mutex, NULL);
}

inline int ManagerPorts::bind_port(ServerObject* port_t)
{
    printf("Bind port %d\r\n", port_t->port);

    createThread((LPTHREAD_START_ROUTINE)create_server, port_t);
	return 1;
}

inline int ManagerPorts::close_port(int fd)
{
    shutdown(fd, SHUT_RDWR);
    printf("port closed %d\r\n", fd);
    return 1;
}

inline int ManagerPorts::addPort(int port, int user_id) {
    if(!port_status[port])
    {
        
        struct ServerObject* port_t = (struct ServerObject*)malloc(sizeof(struct ServerObject));
        port_t->port = port;
        port_t->count_users = 0;
        port_t->is_created = 0;
        port_status[port] = port_t;
    }
    port_status[port]->count_users++;

    printf("User %d add to port %d pull_size %d\r\n", user_id, port, port_status[port]->count_users);
    return 1;
}

inline int ManagerPorts::removePort(int port, int user_id) {

    
    pthread_mutex_lock(&mutex);
    struct ServerObject* port_t = port_status[port];
    if(port_t)
    {
        port_t->count_users--;
    }
    pthread_mutex_unlock(&mutex);
    printf("Remove port %d users %d\r\n", port, port_t->count_users);
    return 1;
}

inline int ManagerPorts::looper()
{
    struct ServerObject* port_t = (struct ServerObject*)malloc(sizeof(struct ServerObject));
    port_t->port = 5111;
    port_t->count_users = 1;
    port_t->is_created = 0;
    port_status[5111] = port_t;

    printf("loop ports 5sec delay\r\n");

    while(true)
    {
        pthread_mutex_lock(&mutex);

        std::map< int, ServerObject* >::iterator it;
        for (it = port_status.begin(); it != port_status.end(); it++) {
            int port = it->first;
            ServerObject* port_t = it->second;

            if(port_t->count_users > 0 && port_t->is_created == 0)
            {
                port_t->is_created = 1;
                ManagerPorts::Instance()->bind_port(port_t);
            }

            if(port_t->count_users == 0 && port_t->is_created == 1)
            {
                //port_t->is_created = 0;
                ManagerPorts::Instance()->close_port(port_t->fd);
            }
        }

        pthread_mutex_unlock(&mutex);

        utils::xsleep(5000);
        break;
    }
    return 1;
}