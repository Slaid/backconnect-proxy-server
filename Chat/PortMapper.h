
#include "framework/Singleton.h"
#include "framework/xLinux.h"
#include "ReversStore.h"
#include "sblist.h"

int revers_max_clients = 2;

class pullConnects
{
public:
	std::string ip;
	int port;
	std::string key;

	std::string login;

	//int index;
	int count_clients;
	int current_speed;

	std::map<int, connection*> connections;

    sblist *con_list;

	std::string last_speedtest;
	std::string info;

	int group_id;
	int total_request;

	Revers* revers;

    User *user;

	int is_close;



	int current_revers_num;

	pullConnects()
	{
        current_revers_num = 0;
		count_clients = 0;
		current_speed = 0;
        total_request = 0;
        is_close = 0;

        con_list = sblist_new(sizeof (connection*), 8);
	}

    int con_delete(int socket)
    {
        connection *tmp = NULL;
        size_t i;
        for(i=0;i<sblist_getsize(con_list);) {
            tmp = *((struct connection **) sblist_get(con_list, i));
            if(tmp->socket == socket)
            {
                sblist_delete(con_list, i);
            } else
                i++;
        }

        return 1;
    }

    connection *con_get_last()
    {
	    if(sblist_getsize(con_list) == 0) return 0;

        connection *con = *((struct connection **) sblist_get(con_list, sblist_getsize(con_list) - 1));
        return con;
    }
};


class infoTransfer
{
public:
	pullConnects* client;
	pullConnects* revers;

	int current_speed;
	int status;
};


class PortMapper
{
	DECLARE_SINGLETON(PortMapper)
public:
    PortMapper();

	std::map<std::string, pullConnects*> distinct_revers;
	std::map<std::string, pullConnects*> distinct_clients;
    std::map<std::string, infoTransfer* > route_connects;

	//sf::safe_ptr< std::map< std::string, pullConnects* > > distinct_revers_ibm;
	//sf::safe_ptr< std::map< std::string, pullConnects* > > distinct_clients_ibm;

	pullConnects* addSocketReversIBM(connection* con);

	pullConnects* getSocketReversIBM(int index);
	connection* getFreeSocket(pullConnects *pull);


	//std::map<std::string, std::string> route_connects;

	/* related ipClient_ipRevers*/

    int removeConnections(int socket);
	std::string getAllRelated();

//	pullConnects* getPullReversByAttributes(connection* con);
	connection* getSocketRevers(int client_port);

    std::string webViewShowFreeReverse();

	infoTransfer* setTransfer(pullConnects* pull_client, pullConnects* pull_revers);

	int disconnectSocketRevers(connection *con);
	pullConnects* addSocketRevers(connection* con);

	pullConnects* addSocketClientIBM(connection *con);
	int disconnectSocketClient(connection *con);

    static void cronCleanRelation();
    void cronCleanRelation_static();

	void clearRelation();
    int closeAllPullClients(pullConnects* pull);

	pullConnects* createBridge(pullConnects* clientPull);
    pullConnects* selectRevers(pullConnects* clientPull);

    pthread_mutex_t mutex;
};

#pragma once



inline PortMapper::PortMapper()
{
    pthread_mutex_init(&mutex, NULL);
}

static void UserSetReversId(pullConnects* clientPull, pullConnects* reversPull)
{
    /*
    if(!clientPull->revers)
    {
        //Update client revers id sql
        clientPull->revers = reversPull->revers;

        //std::string sql = "UPDATE `revers_users` SET `revers_id` = '" + utils::xitoa(reversPull->revers->id) + "' WHERE `id` = '" + utils::xitoa(clientPull->user->id) + "';";
        //printf("sql %s\r\n", sql.c_str());
        //Db::Instance()->query(sql.c_str());
    }
     */
}

static int selectNewRevers()
{
    printf("Start find revers\r\n");
}

inline pullConnects* PortMapper::selectRevers(pullConnects* clientPull)
{
    pullConnects* reversPull = NULL;


    int revers_id = clientPull->user->revers_id;
    printf("Finded reversById: %d\t\n", revers_id);

    std::map<std::string, pullConnects*>::iterator it;
    for (it = distinct_revers.begin(); it != distinct_revers.end(); ++it)
    {
        if (revers_id == it->second->revers->id)
        {
            reversPull = it->second;
            break;
        }
    }

    if(!reversPull)
    {
        printf("Revers not found id: %d\r\n", revers_id);

        if(clientPull->user->type == 1) {
            selectNewRevers();
        }
    }


    /*
    if(clientPull->user->type == 1)
    {
        std::vector< pullConnects* > filtered_revers;

        std::map<std::string, pullConnects*>::iterator it;
        for (it = distinct_revers.begin(); it != distinct_revers.end(); ++it)
        {
            //printf("reverse %d = %d count %d\r\n", it->second->revers->group_id, clientPull->group_id, it->second->count_clients);
            if (it->second->revers->group_id == clientPull->group_id) // && it->second->count_clients < 3 //it->second->count_clients < it->second->revers->max_clients
            {
                filtered_revers.push_back(it->second);
            }
        }

        int count_revers = filtered_revers.size();
        if(count_revers > 0)
        {
            for(int i = 0; i < count_revers; i++)
            {
                if(filtered_revers[i]->count_clients < filtered_revers[i]->revers->max_clients)
                {
                    reversPull = filtered_revers[i];
                    printf("Multiple mode selected revers id: %d\r\n", reversPull->revers->id);
                    break;
                }

            }

        }


    }
     */


    return reversPull;
}

inline pullConnects* PortMapper::createBridge(pullConnects* clientPull)
{
	pullConnects* reversPull = NULL;

	printf("Client Key %s\r\n", clientPull->key.c_str());

    pthread_mutex_lock(&mutex);



	std::map<std::string, infoTransfer*>::iterator it;
	for (it = route_connects.begin(); it != route_connects.end(); it++)
	{
		infoTransfer* info = it->second;
		//printf("Compare %s == %s revers_key = %s pull_size %d\r\n", info->client->key.c_str(), clientPull->key.c_str(), info->revers->key.c_str(), info->revers->connections.size());
		if (clientPull->key.compare(info->client->key) == 0)
		{
			reversPull = info->revers;
			break;
		}
	}

	if (!reversPull)
	{
		printf("Bridge not found %s find free reverse\r\n", clientPull->key.c_str());

        reversPull = selectRevers(clientPull);


        
		if (reversPull)
		{
			
			std::string key = clientPull->key + "_" + reversPull->key;
			printf("Bridge created %s\r\n", key.c_str());
			reversPull->count_clients++;

			infoTransfer* info = new infoTransfer();
			info->client = clientPull;
			info->revers = reversPull;
			route_connects[key] = info;

            UserSetReversId(clientPull, reversPull);


		}

	}
    pthread_mutex_unlock(&mutex);



	return reversPull;
}


inline pullConnects* PortMapper::addSocketReversIBM(connection* con)
{
	con->is_bot = 1;

    pthread_mutex_lock(&mutex);

	pullConnects* pull = distinct_revers[con->key];
	if (!pull)
	{
		pull = new pullConnects();
		pull->ip = con->socket_ip;
		pull->port = con->socket_port;
		pull->key = con->key;
		pull->info = con->info;
		pull->revers = ReversStore::Instance()->get(pull->info, pull->ip, pull->port);
		if(!pull->revers)
        {
		    printf("rever not add\r\n");
		    exit(0);
        }

		distinct_revers[con->key] = pull;
		
	}

    sblist_add(pull->con_list, &con);

	//pull->connections[con->socket] = con;

	printf("Add Revers_Ip: %s(%d), sockets_count: %d\r\n", con->key, con->socket, sblist_getsize(pull->con_list));
    pthread_mutex_unlock(&mutex);
	return pull;
}

inline connection* PortMapper::getFreeSocket(pullConnects* pull)
{
	connection* result = NULL;

    pthread_mutex_lock(&mutex);
	if (pull)
	{
	    /*
		std::map<int, connection*>::reverse_iterator it;
		for (it = pull->connections.rbegin(); it != pull->connections.rend(); it++)
		{
            result = it->second;
            result->status = 1;
            pull->is_transfer = 1;
            break;
		}
	     */

        result = pull->con_get_last();
	}
    pthread_mutex_unlock(&mutex);

	return result;
}


/*
inline pullConnects* PortMapper::getPullReversByAttributes(connection* con)
{

	int n = 0;
    int index = con->socket_port - 1001;
	int group_revers = con->user->group_id;

    if(index == 2000)
        index = 0;

	pullConnects* reversPull = NULL;

    pthread_mutex_lock(&mutex);
	std::map<std::string, pullConnects*>::iterator it;
	for (it = distinct_revers.begin(); it != distinct_revers.end(); ++it)
	{

		//printf("Pull Finded size %d\r\n", it->second->count_clients);
		if (it->second && it->second->revers->group_id == con->user->group_id && it->second->count_clients < it->second->revers->max_clients)
		{
			reversPull = it->second;
			break;
		}

		n++;
	}
    pthread_mutex_unlock(&mutex);

	return reversPull;
}
*/

inline void PortMapper::clearRelation()
{
    std::map<std::string, infoTransfer*>::iterator it;
    //std::map<std::string, infoTransfer*> route_connects = this->route_connects;

    int count_clean = 0;
    //printf("[revers disconnect_switch] relation_size %d\r\n", route_connects.size());
    for (it = route_connects.begin(); it != route_connects.end(); it++) {
        infoTransfer *info = it->second;

        if(info->client && info->client->is_close == 1)
        {
            info->revers->count_clients--;

            PortMapper::Instance()->route_connects.erase(it->first);
            delete info;
        }

        if(info->revers && info->revers->is_close == 1)
        {

            PortMapper::Instance()->closeAllPullClients(info->client);

            PortMapper::Instance()->route_connects.erase(it->first);
            delete info;
        }

    }
}

inline int PortMapper::closeAllPullClients(pullConnects* pull)
{
    connection* tmp = 0;
    int i;
    for(i=0;i<sblist_getsize(pull->con_list);) {
        tmp = *((struct connection **) sblist_get(pull->con_list, i));

        printf("Revers Shutdown close client %d\r\n", tmp->socket);

        shutdown(tmp->socket, SHUT_RDWR);

        i++;
    }

    /*
    std::map<int, connection*>::iterator it;
    for (it = pull->connections.begin(); it != pull->connections.end(); it++) {

        int socket = it->second->socket;
        printf("Revers Shutdown close client %d\r\n", socket);

        shutdown(socket, SHUT_RDWR);
        //ServerHelper::Close(socket);

    }
    */

    return 1;
}

inline int PortMapper::disconnectSocketRevers(connection* con)
{
	if (!con)
	{
		return 0;
	}

    pthread_mutex_lock(&mutex);
	pullConnects* pull = distinct_revers[con->key];

	if (pull)
	{
		//pull->connections.erase(con->socket);
		pull->con_delete(con->socket);

		printf("Revers disconnect %d %s socket_count %d\r\n", con->socket, con->key, pull->connections.size());
		if (sblist_getsize(pull->con_list) == 0) // && pull->is_transfer == 1
		{
            pull->is_close = 1;
            clearRelation();

            pull->revers->is_online = 0;
            PortMapper::Instance()->distinct_revers.erase(pull->key);
            delete pull;

            //distinct_revers.erase(pull->key);
            //delete info->revers;

		}
	} else {
        printf("Pull Revers not found %s\r\n", con->key);
	}

    pthread_mutex_unlock(&mutex);

	return 1;
}



inline infoTransfer* PortMapper::setTransfer(pullConnects* pull_client, pullConnects* pull_revers)
{
	//if (!pull_client || !pull_revers)
	//	return 0;

	std::string key = pull_client->key + "_" + pull_revers->key;
	//printf("Key %s\r\n", key.c_str());


	//info->traffic = 0;

    pthread_mutex_lock(&mutex);
	infoTransfer* info = route_connects[key];
	if (!info)
	{

		infoTransfer* info = new infoTransfer();
		info->client = pull_client;
		info->revers = pull_revers;
		route_connects[key] = info;
		pull_revers->count_clients++;
	}

    pthread_mutex_unlock(&mutex);

	return info;
}

inline pullConnects* PortMapper::addSocketClientIBM(connection* con)
{
    con->is_bot = 0;

    //std::string key = con->login + "@" + con->key;

    pthread_mutex_lock(&mutex);
    pullConnects * pull = distinct_clients[con->key]; //pullConnects* pull = (*distinct_clients_ibm)[con->key];
    if (!pull)
    {
        pull = new pullConnects();
        pull->ip = con->socket_ip;
        pull->port = con->socket_port;
        pull->key = con->key;
        pull->login = con->login;
		//pull->group_id = con->user->group_id;

		//pull->user = con->user;

        distinct_clients[con->key] = pull;
    }

    pull->total_request++;
    //pull->connections[con->socket] = con;

    sblist_add(pull->con_list, &con);

    printf("Add Client: %s(%d), sockets_count: %d\r\n", con->key, con->socket, sblist_getsize(pull->con_list));
    pthread_mutex_unlock(&mutex);
    return pull;
}

inline int PortMapper::disconnectSocketClient(connection *con)
{
    if (!con)
    {
        return 0;
    }

    pthread_mutex_lock(&mutex);
    pullConnects* pull = distinct_clients[con->key];

    if (pull)
    {

        pull->con_delete(con->socket);
        //pull->connections.erase(con->socket);
        //this->distinct_revers[con->key] = pull;
        printf("Client disconnect %d %s socket_count %d\r\n", con->socket, con->key, sblist_getsize(pull->con_list));
        if (sblist_getsize(pull->con_list) == 0)
        {

            pull->is_close = 1;
            clearRelation();

            PortMapper::Instance()->distinct_clients.erase(pull->key);
            delete pull;

        }
    }
    pthread_mutex_unlock(&mutex);

    return 1;
}




inline std::string PortMapper::webViewShowFreeReverse()
{
    std::string result = "<h4>Revers INFO</h4>";

    pthread_mutex_lock(&mutex);

    int n = 0;

    std::map<std::string, pullConnects*>::iterator it;
    for (it = distinct_revers.begin(); it != distinct_revers.end(); it++)
    {
        pullConnects* info = it->second;
        if (info && sblist_getsize(info->con_list) > 0)
        {
            result += utils::xitoa(n, 10) + ") " + info->key + " Sockets_open:" + utils::xitoa(sblist_getsize(info->con_list), 10) + " clients: " + utils::xitoa(info->count_clients) + "/" + utils::xitoa(info->revers->max_clients) + " group_id: " + utils::xitoa(info->revers->group_id);
            result += "<br />";
            n++;
            printf("Revers n=%d\r\n", n);
        }
    }

    result += "<br />------------<br />";
    pthread_mutex_unlock(&mutex);

    return result;
}

/*
@ return all rules in web interfaces
*/
inline std::string PortMapper::getAllRelated()
{
    std::string result = "<h4>All connection</h4>";
    pthread_mutex_lock(&mutex);

    std::map<std::string, infoTransfer*>::iterator it;

    for (it = route_connects.begin(); it != route_connects.end(); it++)
    {
        infoTransfer* info = it->second;
        //if (!info->client || !info->revers)
        //	continue;
        if(info->client && info->revers)
        {
            result += "Client: " + info->client->key + "(" + utils::xitoa(sblist_getsize(info->client->con_list)) + ")";
            result += " => ";

			result += "Revers: " + info->revers->key + "(" + utils::xitoa(sblist_getsize(info->revers->con_list)) + ")";
			result += "<br />";
        } else {
			result += "EMPTY NULL CLient or Revers<br />";
        }
		/*
        if(info->revers)
        {
			result += "Revers: " + info->revers->key + "(" + utils::xitoa(info->revers->connections.size()) + ")";
            result += "<br />";
        }
		*/


    }

    pthread_mutex_unlock(&mutex);

    return result;
}



inline void PortMapper::cronCleanRelation()
{
    /*
    while(1)
    {

        LizzzMutex::Instance("mapper_connection")->lock();
        std::map<std::string, infoTransfer*>::iterator it;
        std::map<std::string, infoTransfer*> route_connects = PortMapper::Instance()->route_connects;

        int count_clean = 0;
        printf("[cron] Clean relations total %d\r\n", route_connects.size());
        for (it = route_connects.begin(); it != route_connects.end(); it++) {
            infoTransfer *info = it->second;
            //printf("Con %d\r\n", info->client->connections.size());
            if(info->client->connections.size() == 0)
            {
				info->revers->count_clients--;
                PortMapper::Instance()->distinct_clients.erase(info->client->key);
                delete info->client;
                PortMapper::Instance()->route_connects.erase(it->first);
                delete info;
                //return ;
                count_clean++;
            }
        }
        if(count_clean > 0)
        {
            printf("[cron] Cleaned %d\r\n", count_clean);
        }
        LizzzMutex::Instance("mapper_connection")->unlock();
        usleep(1000 * 1000 * 10);
    }
     */
}

inline void PortMapper::cronCleanRelation_static()
{
    Thread t1;
    t1.Start((LPTHREAD_START_ROUTINE)PortMapper::cronCleanRelation, 0);
}