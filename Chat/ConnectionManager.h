#include "framework/WS32_helper.h"
#include <map>
#include "TrafficAnalize.h"
#include "Object.h"

class ConnectionManager
{
    DECLARE_SINGLETON(ConnectionManager)
public:
	//static ConnectionManager* Instance(std::string name);
	//std::string currentIndex;
    ConnectionManager();
	void add(connection *con);
	connection* get(int socket);
    int sb_erase(int socket);
    //int remove(connection* con);

	//connection* createConnection(int new_socket, std::string ip, int port);
	//int removeConnection(connection *con);

	//std::vector< connection* > connection_list;
	//inline connection* search(connection* con);

	//int addConnection(connection** con);
	//int closeConnection(connection* con);
	//int closeConnection(int socket);
	
	//int closeConnectionThread(int socket);
	//connection** getConnection(int socket = 0);
	//int countConnection();

	std::string dumpConnectionList(int group = 0);
	//sf::safe_ptr< std::map< int, connection* > > connection_list_safe;
    

    std::string print(int user_id);

    int clearTTLConnections();

	//connection* getConnectionByFilter();

	int connection_size = 0;

	sblist *con_list;

    pthread_mutex_t mutex;

};

#pragma once

//static std::map< int, connection* > connection_list;


inline int ConnectionManager::clearTTLConnections()
{

    pthread_mutex_lock(&mutex);


    connection *con = NULL;

    int timestamp = utils::timeSinceEpochMillisec() / 1000;

    size_t i;
    size_t max = sblist_getsize(con_list);
    
    for(i=0;i<max; i++) {
        connection *con = *((struct connection **) sblist_get(con_list, i));


        int diff = timestamp - con->last_active;

        if(con->is_bot)
        {
            if(diff > 30) 
            {
                //con->close_s();
                //closesocket(con->socket);
                shutdown(con->socket, SHUT_RDWR);
            } 
        } else {
            if(diff > 10 && con->stage == 0)
            {
                printf("TTL 5 Stage 0 closed\r\n");
                //closesocket(con->socket);
                //con->close_s();
                shutdown(con->socket, SHUT_RDWR);
            }
            if(diff > 60)
            { 
                printf("TTL 60 closed\r\n");
                //closesocket(con->socket);
                //con->close_s();
                shutdown(con->socket, SHUT_RDWR);
            }
        }

    }


    pthread_mutex_unlock(&mutex);


    return 0;
}

static void clear()
{
    while(1)
    {
        ConnectionManager::Instance()->clearTTLConnections();
        utils::xsleep(10 * 1000);
    }
}

inline ConnectionManager::ConnectionManager()
{
    pthread_mutex_init(&mutex, NULL);
	con_list = sblist_new(sizeof (connection*), 8);

    Thread t1;
    t1.Start((LPTHREAD_START_ROUTINE)clear, (void*)0);

}

inline connection* ConnectionManager::get(int socket)
{
    connection *con = NULL;

    pthread_mutex_lock(&mutex);


    size_t i;
    for(i=0;i<sblist_getsize(con_list);) {
        connection *tmp = *((struct connection **) sblist_get(con_list, i));

        if(tmp->socket == socket)
        {
            con = tmp;
            break;
        }
        i++;
            
    }

    pthread_mutex_unlock(&mutex);

    return con;

}


inline void ConnectionManager::add(connection *con)
{

    pthread_mutex_lock(&mutex);
    size_t i;
    for(i=0;i<sblist_getsize(con_list);) {
        connection *tmp = *((struct connection **) sblist_get(con_list, i));
        if(tmp->done)
        {
            joinThread(tmp->ptr);
            sblist_delete(con_list, i);
        } else
            i++;
    }
    sblist_add(con_list, &con);

    printf("[%d] threads connection\r\n", sblist_getsize(con_list));
    pthread_mutex_unlock(&mutex);

}




inline int ConnectionManager::sb_erase(int socket)
{
    pthread_mutex_lock(&mutex);
    size_t i;
    for(i=0;i<sblist_getsize(con_list);) {
        connection *tmp = *((struct connection **) sblist_get(con_list, i));
        if(tmp->socket == socket)
        {
            sblist_delete(con_list, i);
        } else
            i++;
    }
    pthread_mutex_unlock(&mutex);

    return 1;
    
}

/*
inline int ConnectionManager::remove(connection *con)
{

    sb_erase(con->socket);
    


	return 0;
}
*/

inline std::string ConnectionManager::print(int user_id)
{
    std::string result = "<table>";
    pthread_mutex_lock(&mutex);

    connection *con = NULL;

    int timestamp = utils::timeSinceEpochMillisec() / 1000;

    size_t i;
    for(i=0;i<sblist_getsize(con_list);i++) {
        connection *con = *((struct connection **) sblist_get(con_list, i));

        if(con && con->user_id == user_id)
        {
            int diff = timestamp - con->last_active;
            result += "<tr><td>" + utils::xitoa(con->socket) + "</td><td>" + utils::xitoa(con->revers_id) + "</td><td>" + utils::xitoa(con->user_id) + "</td><td>" + con->key + "</td><td>" + std::string(con->hostname) + "</td><td>" + utils::xitoa(diff) + "</td><td>" + utils::xitoa(con->countTrafficRequest) + "/" + utils::xitoa(con->countTrafficResponse) + "</td></tr>";
        }
    }

    pthread_mutex_unlock(&mutex);
    result += "</table>";

    return result;
}

inline std::string ConnectionManager::dumpConnectionList(int group)
{



    pthread_mutex_lock(&mutex);



    std::string line = "<div>Connection pull " + utils::xitoa(sblist_getsize(con_list)) + "</div>";
    line =+ "<table><tr><th>Socket</th><th>Key</th><th>Is Bot</th><th>Hostname</th><th>TTL</th><th>Traffic</th></tr>";

    connection *con = NULL;

    int timestamp = utils::timeSinceEpochMillisec() / 1000;

    size_t i;
    for(i=0;i<sblist_getsize(con_list);i++) {
        connection *con = *((struct connection **) sblist_get(con_list, i));

        if(con)
        {
            int diff = timestamp - con->last_active;
            line += "<tr><td>" + utils::xitoa(con->socket) + "</td><td>" + con->key + "</td><td>" + utils::xitoa(con->progress_code) + ":" + utils::xitoa(con->is_close) + ":" + utils::xitoa(con->done) + "</td><td>" + std::string(con->hostname) + "</td><td>" + utils::xitoa(diff) + "</td><td>" + utils::xitoa(con->countTrafficRequest) + "/" + utils::xitoa(con->countTrafficResponse) + "</td></tr>";

            //con->index = i;

        } else {
            line += "<tr><td>Null connection</td></tr><br />";
        }



    }


    pthread_mutex_unlock(&mutex);
    line += "</table>";
	
	return line;
}

