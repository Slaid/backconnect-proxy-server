#ifndef SBLIST_H
#define SBLIST_H

/* this file is part of libulz, as of commit 8ab361a27743aaf025323ee43b8b8876dc054fdd
modified for direct inclusion in microsocks. */




/*
* simple buffer list.
*
* this thing here is basically a generic dynamic array
* will realloc after every blockitems inserts
* can store items of any size.
*
* so think of it as a by-value list, as opposed to a typical by-ref list.
* you typically use it by having some struct on the stack, and pass a pointer
* to sblist_add, which will copy the contents into its internal memory.
*
*/

typedef struct {
	size_t itemsize;
	size_t blockitems;
	size_t count;
	size_t capa;
	char* items;
} sblist;

#define sblist_getsize(X) ((X)->count)
#define sblist_get_count(X) ((X)->count)
#define sblist_empty(X) ((X)->count == 0)

// for dynamic style
sblist* sblist_new(size_t itemsize, size_t blockitems);
void sblist_free(sblist* l);

//for static style
void sblist_init(sblist* l, size_t itemsize, size_t blockitems);
void sblist_free_items(sblist* l);

// accessors
void* sblist_get(sblist* l, size_t item);
// returns 1 on success, 0 on OOM
int sblist_add(sblist* l, void* item);
int sblist_set(sblist* l, void* item, size_t pos);
void sblist_delete(sblist* l, size_t item);
char* sblist_item_from_index(sblist* l, size_t idx);
int sblist_grow_if_needed(sblist* l);



#ifndef __COUNTER__
#define __COUNTER__ __LINE__
#endif

#define __sblist_concat_impl( x, y ) x##y
#define __sblist_macro_concat( x, y ) __sblist_concat_impl( x, y )
#define __sblist_iterator_name __sblist_macro_concat(sblist_iterator, __COUNTER__)

// use with custom iterator variable
#define sblist_iter_counter(LIST, ITER, PTR) \
	for(size_t ITER = 0; (PTR = sblist_get(LIST, ITER)), ITER < sblist_getsize(LIST); ITER++)

// use with custom iterator variable, which is predeclared
#define sblist_iter_counter2(LIST, ITER, PTR) \
	for(ITER = 0; (PTR = sblist_get(LIST, ITER)), ITER < sblist_getsize(LIST); ITER++)

// use with custom iterator variable, which is predeclared and signed
// useful for a loop which can delete items from the list, and then decrease the iterator var.
#define sblist_iter_counter2s(LIST, ITER, PTR) \
	for(ITER = 0; (PTR = sblist_get(LIST, ITER)), ITER < (ssize_t) sblist_getsize(LIST); ITER++)


// uses "magic" iterator variable
#define sblist_iter(LIST, PTR) sblist_iter_counter(LIST, __sblist_iterator_name, PTR)



#define MY_PAGE_SIZE 4096

sblist* sblist_new(size_t itemsize, size_t blockitems) {
	sblist* ret = (sblist*)malloc(sizeof(sblist));
	sblist_init(ret, itemsize, blockitems);
	return ret;
}

static void sblist_clear(sblist* l) {
	l->items = NULL;
	l->capa = 0;
	l->count = 0;
}

void sblist_init(sblist* l, size_t itemsize, size_t blockitems) {
	if (l) {
		l->blockitems = blockitems ? blockitems : MY_PAGE_SIZE / itemsize;
		l->itemsize = itemsize;
		sblist_clear(l);
	}
}

void sblist_free_items(sblist* l) {
	if (l) {
		if (l->items) free(l->items);
		sblist_clear(l);
	}
}

void sblist_free(sblist* l) {
	if (l) {
		sblist_free_items(l);
		free(l);
	}
}

char* sblist_item_from_index(sblist* l, size_t idx) {
	return l->items + (idx * l->itemsize);
}

void* sblist_get(sblist* l, size_t item) {
	if (item < l->count) return (void*)sblist_item_from_index(l, item);
	return NULL;
}

int sblist_set(sblist* l, void* item, size_t pos) {
	if (pos >= l->count) return 0;
	memcpy(sblist_item_from_index(l, pos), item, l->itemsize);
	return 1;
}

int sblist_grow_if_needed(sblist* l) {
	char* temp;
	if (l->count == l->capa) {
		temp = (char*)realloc(l->items, (l->capa + l->blockitems) * l->itemsize);
		if (!temp) return 0;
		l->capa += l->blockitems;
		l->items = temp;
	}
	return 1;
}

int sblist_add(sblist* l, void* item) {
	if (!sblist_grow_if_needed(l)) return 0;
	l->count++;
	return sblist_set(l, item, l->count - 1);
}

void sblist_delete(sblist* l, size_t item) {
	if (l->count && item < l->count) {
		memmove(sblist_item_from_index(l, item), sblist_item_from_index(l, item + 1), (sblist_getsize(l) - (item + 1)) * l->itemsize);
		l->count--;
	}
}


#endif


