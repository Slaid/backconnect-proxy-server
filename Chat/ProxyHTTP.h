#pragma once



#include "framework/WS32_helper.h"
#include "framework/ConnectionManager.h"

#include "compressor.h"
#include "brotli/decode.h"
#include "brotli/encode.h"

#ifdef WIN32
#pragma comment(lib,"brotli.lib")
#endif



class ProxyHTTP {
public:

	static void send_error(int fd) {
		/* position 4 contains ATYP, the address type, which is the same as used in the connect
		request. we're lazy and return always IPV4 address type in errors. */
		char buf[10] = { 5, 6, 0, 1 /*AT_IPV4*/, 0,0,0,0, 0,0 };
		send(fd, buf, 10, 0);
	}


	static int proxyRequest(void* lpParameter)
	{
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;

		std::string req; int partAlive = 0; int pos;

		while (TRUE) {

			char buffer[4096];
			int n = recv(pSvcInfo->slocal, buffer, 4096, 0);
			if (n <= 0) {
				//printf("Resp %d exit\r\n", n);
				break;
			}

			if(pSvcInfo->con->remote_port != 443)
			{
				req.append(buffer, n);

				if ((req.find("GET ") == 0 || req.find("POST ") == 0) && req.find("\r\n\r\n") != std::string::npos) {

					ProxyHTTP::parseRequest(req, pSvcInfo);
					req = "";
				}

			}

			int len = send(pSvcInfo->sremote, buffer, n, 0);
			//if (len <= 0)
			//	break;
		}


		if (!pSvcInfo->isClosed)
		{
			pSvcInfo->isClosed = TRUE;
			send(pSvcInfo->sremote, "", 0, 0);
			ServerHelper::Close(pSvcInfo->sremote);
			ConnectionManager::Instance("mitm")->closeConnection(pSvcInfo->con);
			
		}

		return 0;
	}

	static int proxyResponse(void* lpParameter)
	{
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;

		while (TRUE) {

			char buffer[4096];
			int n = recv(pSvcInfo->sremote, buffer, 4096, 0);
			if (n <= 0) {
				break;
			}
			//if (!chechHttp(pSvcInfo, buffer, n, false)) {
				int len = send(pSvcInfo->slocal, buffer, n, 0);
				//if (len <= 0)
				//	break;
			//}
		}


		if (!pSvcInfo->isClosed)
		{
			pSvcInfo->isClosed = TRUE;
			send(pSvcInfo->slocal, "", 0, 0);
			ServerHelper::Close(pSvcInfo->sremote);
			ConnectionManager::Instance("mitm")->closeConnection(pSvcInfo->con);
			
		}
		return 0;
	}


	static int tunel(SERVICE_INFO* pSvc) {


		pSvc->isClosed = FALSE;

		Thread t1, t2;

		printf("Start Proxy HTTP\r\n");
		t1.Start((LPTHREAD_START_ROUTINE)proxyRequest, pSvc);
		t2.Start((LPTHREAD_START_ROUTINE)proxyResponse, pSvc);


		t1.WaitForEnd();
		t2.WaitForEnd();

		printf("Threads finish %d\r\n", pSvc->slocal);

		return 0;

		//ServerHelper::Close(pSvc->sremote);

		/*
		return 0;

		int status = copyloop(pSvc->slocal, pSvc->sremote, pSvc);

		printf("end copyloop status %d\r\n", status);

		if (pSvc->sremote != -1)
			ServerHelper::Close(pSvc->sremote);

		//ServerHelper::Close(pSvc->slocal);

		//con->done = 1;
		ConnectionManager::Instance("mitm")->closeConnection(pSvc->con);

		//t->done = 1;

		//delete pSvc;
		//delete con->thread1;

		//printf("end thread\r\n");
		
		return 0;

		*/
		
	}

	static int connectToHost(std::string ip, int port)
	{
		return 1;
	}

	static bool parseRequest(std::string header, void* lpParameter)
	{
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;

		pSvcInfo->keep_alive_cnt++;

		Processor *proc = new Processor(header);
		proc->parseRequest();
		pSvcInfo->uri = proc->uri;
		pSvcInfo->referer = proc->getHeader("referer");

		pSvcInfo->con->setAttribute("GET", pSvcInfo->uri);

		printf("%s(keep:%d) %s:%d (%s) %s\r\n", proc->method.c_str(), pSvcInfo->keep_alive_cnt, pSvcInfo->con->remote_ip.c_str(), pSvcInfo->con->remote_port, proc->host.c_str(), pSvcInfo->uri.c_str());

		return 1;
	}

	static int modeSend(SERVICE_INFO* pSvcInfo, std::string html)
	{
		int len = (pSvcInfo->is_ssl) ? SSL_write(pSvcInfo->ssl, html.data(), html.length()) : send(pSvcInfo->slocal, html.data(), html.length(), 0);
		return len;
	}

	static int modeRecv(SERVICE_INFO* pSvcInfo, char *buffer, int buffer_len)
	{
		int len = (pSvcInfo->is_ssl) ? SSL_read(pSvcInfo->ssl_remote, buffer, buffer_len) : recv(pSvcInfo->sremote, buffer, buffer_len, 0);
		return len;
	}

	static bool chechHttp(void* lpParameter, const char *buffer, int n, bool is_ssl) {

		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;
		pSvcInfo->is_ssl = is_ssl;

		if (n >= 5 && buffer[0] == 'H' && buffer[1] == 'T' &&buffer[2] == 'T' &&buffer[3] == 'P' &&buffer[4] == '/') {

			
			int htmlStart = 0;
			pSvcInfo->startBody = -1;
			char bufferTwo[4096];

			int isHeaderReaded = 0;
			pSvcInfo->html = "";

			pSvcInfo->html.append(buffer, n);
			if ((pSvcInfo->startBody = pSvcInfo->html.find("\r\n\r\n")) != std::string::npos) {
				pSvcInfo->startBody += 4;
				isHeaderReaded = 1;
			}


			//printf("read header ok %d\r\n", isHeaderReaded);

			if (isHeaderReaded == 0) {

				while (true) {
					int n2 = modeRecv(pSvcInfo, bufferTwo, 4096); // (is_ssl) ? SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096) : recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
					if (n2 <= 0)
					{
						printf("recv Error! GG %d\r\n", WSAGetLastError());
						break;
					}
					n += n2;

					pSvcInfo->html.append(bufferTwo, n2);
					if ((pSvcInfo->startBody = pSvcInfo->html.find("\r\n\r\n")) != std::string::npos) {
						pSvcInfo->startBody += 4;
						isHeaderReaded = 1;
						break;
					}

				}
			}

			pSvcInfo->proc = new Processor(pSvcInfo->html);
			pSvcInfo->proc->parseResponse();


			std::string contentType = pSvcInfo->proc->getHeader("content-type");
			std::string contentEncode = pSvcInfo->proc->getHeader("content-encoding");
			std::string transferEncoding = pSvcInfo->proc->getHeader("transfer-encoding");
			int contentLength = atoi(pSvcInfo->proc->getHeader("content-length").c_str());
			int code = pSvcInfo->proc->code;


			//printf("Data %s\r\n", proc->responce->header.c_str());

			if (1 == 2 && contentType.find("text/html") == std::string::npos) { // && proc->responce->contentType.find("application/x-") == std::string::npos) {
																					  //printf("read more and sended not mitm\r\n");

				modeSend(pSvcInfo, pSvcInfo->html); // (is_ssl) ? SSL_write(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length()) : send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);
				//send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);
				delete pSvcInfo->proc;
				return true;
			} else {


				if (transferEncoding.find("chunked") == 0) {

					//pSvcInfo->startBody

					printf("Chunk content\r\n");
					int chunkLen = 0;
					int startChunkBlock = 0;
					int endChunkBlock = 0;
					std::string chunk = "";

					while (true) {
						int n = (endChunkBlock) ? endChunkBlock : pSvcInfo->startBody;

						//printf("start chunk block %d %d htmllen\r\n", n, pSvcInfo->html.length());

						if (n >= pSvcInfo->html.length()) {
							int len = modeRecv(pSvcInfo, bufferTwo, 4096);  //(is_ssl) ? SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096) : recv(pSvcInfo->sremote, bufferTwo, 4096, 0); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
							if (len <= 0) {
								break;
							}
							pSvcInfo->html.append(bufferTwo, len);
							//printf("loaded +%d=%d\r\n", len, pSvcInfo->html.length());
						}

						chunk = "";

						while (true) {
							if (pSvcInfo->html[n] == 13 && pSvcInfo->html[n + 1] == 10) {
								chunkLen = strtol(chunk.c_str(), NULL, 16);
								startChunkBlock = n + 2;
								endChunkBlock = startChunkBlock + chunkLen;
								break;
							}
							chunk += (char)pSvcInfo->html[n];
							n++;

							if (n >= pSvcInfo->html.length()) {
								int len = modeRecv(pSvcInfo, bufferTwo, 4096); //(is_ssl) ? SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096) : recv(pSvcInfo->sremote, bufferTwo, 4096, 0);  //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
								if (len <= 0) {
									break;
								}
								pSvcInfo->html.append(bufferTwo, len);
								//printf("loaded +%d=%d\r\n", len, pSvcInfo->html.length());
							}
						}

						//printf("Chunk(%d) %s %d (%d - %d)\r\n", chunk.length(), chunk.c_str(), chunkLen, startChunkBlock, endChunkBlock);

						if (chunkLen == 0) {
							break;
						}

						//if (endChunkBlock > pSvcInfo->html.length()) {
						//printf("%d > %d\r\n", endChunkBlock, pSvcInfo->html.length());
						while (endChunkBlock >= pSvcInfo->html.length()) {

							int len = modeRecv(pSvcInfo, bufferTwo, 4096); //(is_ssl) ? SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096) : recv(pSvcInfo->sremote, bufferTwo, 4096, 0); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
							if (len <= 0) {
								break;
							}

							pSvcInfo->html.append(bufferTwo, len);
							//printf("loaded2 +%d=%d\r\n", len, pSvcInfo->html.length());
						}

						//}



						endChunkBlock += 2;

					}

					ProxyHTTP::modify(pSvcInfo);
					modeSend(pSvcInfo, pSvcInfo->html);  //(is_ssl) ? SSL_write(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length()) : send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0); //send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);
					delete pSvcInfo->proc;
					return true;
				
				} else if (contentLength > 0) {
					printf("Length content\r\n");


					int lenBody = pSvcInfo->html.length() - pSvcInfo->startBody;
					//printf("len body %d\r\n", lenBody);
					//printf("diff %d == %d  nCout %d startBody %d\r\n", lenBody, proc->header.contentLen, nCount, startBody);
					if (lenBody >= pSvcInfo->proc->contentLength) {
						//printf("gege");
						//return 1;
					}
					else {
						int contentLength2 = n;
						int n = 0;

						//int exit = 0;
						while (true) {
							n = modeRecv(pSvcInfo, bufferTwo, 4096); //(is_ssl) ? SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096) : recv(pSvcInfo->sremote, bufferTwo, 4096, 0); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
							//n = recv(pSvcInfo->sremote, buffer2, sizeof(buffer2), 0);
							//printf("qqqq %d\r\n", n);
							if (n <= 0) {
								break;
							}

							pSvcInfo->html.append(bufferTwo, n);
							contentLength2 += n;


							int lenSize = contentLength2 - pSvcInfo->startBody;

							//printf("exit %d >= %d\r\n", lenSize, proc->header.contentLen);
							if (lenSize >= pSvcInfo->proc->contentLength) {
								break;
							}


						}
						//return 1;
					}

					ProxyHTTP::modify(pSvcInfo);
					modeSend(pSvcInfo, pSvcInfo->html);  //(is_ssl) ? SSL_write(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length()) : send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0); //send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);

					delete pSvcInfo->proc;
					return true;
				}
				else if (pSvcInfo->proc->code == 304 || pSvcInfo->proc->code == 204) {

					modeSend(pSvcInfo, pSvcInfo->html);  //(is_ssl) ? SSL_write(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length()) : send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0); //send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);

					delete pSvcInfo->proc;
					return TRUE;

				}
				else if (!pSvcInfo->proc->is_chunked && pSvcInfo->proc->contentLength <= 0) {
					//printf("NO chunk no len %s\r\n", pSvcInfo->host);

					modeSend(pSvcInfo, pSvcInfo->html);  //(is_ssl) ? SSL_write(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length()) : send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0); //send(pSvcInfo->slocal, pSvcInfo->html.data(), pSvcInfo->html.length(), 0);
					delete pSvcInfo->proc;
					return TRUE;
				}
				//printf("Sended %s %d/%d\r\n", pSvcInfo->host, sended, pSvcInfo->html.length());
			}

		}
		else {
			return FALSE;
		}
	}

	static int modify(SERVICE_INFO* &info) {
		
		//printf("return 0\r\n");
		//return 0;

		std::string contentType = info->proc->contentType;
		if (contentType.find("text/html;") != 0)
		{
			return false;
		}

		printf("modyfy length: %d contentType: %s\r\n", info->html.length(), contentType.c_str());

		std::string header = info->html.substr(0, info->startBody - 4);
		std::string body = info->html.substr(info->startBody, info->html.length());
		int bodyLen = info->html.length() - header.length();

		std::string bodyNoChunk;

		//printf("Referer: %s\r\n", proc->getHeader("referer").c_str());
		//printf("Referer: %s\r\n", proc->responce->header.c_str());

		if (info->proc->is_chunked) {
			printf("isChunk\r\n");


			int chunkPart = 0;
			int chunkLen = 0;
			int startChunkBlock = 0;
			int endChunkBlock = 0;

			std::string block;
			int posBlock = 0;


			std::string chunk = "";
			printf("Read chunk | allLength %d\r\n", body.length());

			//for (int i = 0; i < body.length(); i++) {
			//	if (body[i] == 13 && body[i + 1] == 10) {

			//		chunkLen = strtol(chunk.c_str(), NULL, 16);
			//		startChunkBlock = i + 3;
			//		endChunkBlock = startChunkBlock + chunkLen;
			//		printf("Chunk %s %d (%d - %d)\r\n", chunk.c_str(), chunkLen, startChunkBlock, endChunkBlock);
			//	}
			//}

			while (true) {
				int n = (endChunkBlock) ? endChunkBlock : info->startBody;

				//printf("start chunk block %d %d htmllen\r\n", n, pSvcInfo->html.length());

				if (n >= info->html.length()) {
					printf("break 1\r\n");
					break;
				}

				chunk = "";

				while (true) {
					if (info->html[n] == 13 && info->html[n + 1] == 10) {
						chunkLen = strtol(chunk.c_str(), NULL, 16);
						startChunkBlock = n + 2;
						endChunkBlock = startChunkBlock + chunkLen;
						break;
					}
					chunk += (char)info->html[n];
					n++;

					if (n >= info->html.length()) {
						printf("break 2\r\n");
						break;
					}
				}

				printf("Chunk(%d) %s %d (%d - %d)\r\n", chunk.length(), chunk.c_str(), chunkLen, startChunkBlock, endChunkBlock);

				if (chunkLen == 0) {
					break;
				}

				for (int i = startChunkBlock; i < endChunkBlock; i++) {
					bodyNoChunk += (char)info->html[i];
				}



				endChunkBlock += 2;

			}



		}
		else {
			//printf("%s\r\n", header.c_str());
			bodyNoChunk = body;
		}

		//printf("%s\r\n", bodyNoChunk.length());


		//script += "<script src=\"https://aferon.com/\" type=\"text/javascript\"></script>";
		//script += "</body>";

		//script = "";

		//if (bodyNoChunk.length() <= 0)
		//{
		//	info->html = "";
		//	return 1;
		//}

		std::string contentEncode = info->proc->contentEncode;
		printf("Content-Encoding %s Body: %d byte\r\n", contentEncode.c_str(), bodyNoChunk.length());


		if (contentEncode.find("gzip") != std::string::npos) {
			printf("Add gzip to %s\r\n", info->hostname.c_str());

			std::string decoded;

			printf("Source data: %d\r\n", bodyNoChunk.length());

			if (!Comp::gzipInflate(bodyNoChunk, decoded)) {
				//log("Error decompres\r\n");
				printf("Error decompressing file.");
				return 0;
			}


			pageMod(decoded, info);

			//decoded = ReplaceAll(decoded, std::string("</body>"), script);
			std::string dataF;

			printf("Decompress data %d\r\n", decoded.length());
			Comp::Compress(decoded, dataF, 1);

			printf("After Compress data %d\r\n", dataF.length());

			if (info->proc->is_chunked) {
				addChunk(dataF, header, info);


			}
			else {
				addNewContentLength(dataF, header, info);



			}
		}

		else if (contentEncode.find("br") != std::string::npos) {
			//printf("content Encoding BR need realization or remove brotli\r\n");

			
			std::string output;
			ProxyHTTP::brotliDec(bodyNoChunk, output);
			printf("Brotli decode len %d\r\n", output.length());



			pageMod(output, info);


			if (output.length() <= 0)
			{
				info->html = "";
				return 1;
			}

			//std::cout << output.c_str();

			bodyNoChunk = "";
			ProxyHTTP::brotliEnc(output, bodyNoChunk);

			//printf("is chunk %d\r\n", proc->responce->isChunk);
			if (info->proc->is_chunked) {
				addChunk(bodyNoChunk, header, info);
			}
			else {
				addNewContentLength(bodyNoChunk, header, info);
			}
			

			//std::cout << info->html.c_str();

			//SslHelper::brotliDec(output, data);
		}
		else {

			printf("Add Not Gzip/Br Content-length: %d, to %s\r\n", info->proc->contentLength, info->hostname.c_str());
			pageMod(bodyNoChunk, info);

			//printf("%s\r\n", bodyNoChunk.c_str());
			//bodyNoChunk = ReplaceAll(bodyNoChunk, std::string("</body>"), script);
			

			if (info->proc->is_chunked) {
				addChunk(bodyNoChunk, header, info);
				/*
				char hex[10];
				itoa(bodyNoChunk.size(), hex, 16);

				info->html = header;
				info->html += "\r\n\r\n";
				info->html += hex;
				info->html += "\r\n";
				info->html += bodyNoChunk;
				info->html += "\r\n0\r\n\r\n";
				*/
			}
			else {
				addNewContentLength(bodyNoChunk, header, info);
				/*
				char newLen[11];
				sprintf(newLen, "%d", bodyNoChunk.length());

				std::string newLenContent;
				newLenContent += "Content-Length: ";
				newLenContent += newLen;
				newLenContent += "\r\nContent-Length-Old: ";
				header = ReplaceAll(header, std::string("Content-Length: "), newLenContent);

				info->html = header;
				info->html += "\r\n\r\n";
				info->html += bodyNoChunk;
				*/
			}

		}


		printf("output %d\r\n", info->html.length());

		//info->html = "";
		/*output = fopen("F:\\close2.txt", "a+b");
		fwrite(info->sbuf.data(), sizeof(char), info->sbuf.length(), output);
		fclose(output);*/


		return 1;
	}
	
	static std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
		size_t start_pos = 0;
		while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
			str.replace(start_pos, from.length(), to);
			start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
		}
		return str;
	}

	static char* itoa(unsigned long long  value, char str[], int radix)
	{
		char        buf[66];
		char*       dest = buf + sizeof(buf);
		bool     sign = false;

		if (value == 0) {
			memcpy(str, "0", 2);
			return str;
		}

		if (radix < 0) {
			radix = -radix;
			if ((long long)value < 0) {
				value = -value;
				sign = true;
			}
		}

		*--dest = '\0';

		switch (radix)
		{
		case 16:
			while (value) {
				*--dest = '0' + (value & 0xF);
				if (*dest > '9') *dest += 'A' - '9' - 1;
				value >>= 4;
			}
			break;
		case 10:
			while (value) {
				*--dest = '0' + (value % 10);
				value /= 10;
			}
			break;

		case 8:
			while (value) {
				*--dest = '0' + (value & 7);
				value >>= 3;
			}
			break;

		case 2:
			while (value) {
				*--dest = '0' + (value & 1);
				value >>= 1;
			}
			break;

		default:            // The slow version, but universal
			while (value) {
				*--dest = '0' + (value % radix);
				if (*dest > '9') *dest += 'A' - '9' - 1;
				value /= radix;
			}
			break;
		}

		if (sign) *--dest = '-';

		memcpy(str, dest, buf + sizeof(buf) - dest);
		return str;
	}



	static int addChunk(std::string body, std::string header, SERVICE_INFO* &info) {
		char hex[10];

		ProxyHTTP::itoa(body.size(), hex, 16);

		//header = ReplaceAll(header, std::string("Access-Control-Allow-Origin:"), std::string("Access-Control-Allow-Origin-Old:"));
		//header = ReplaceAll(header, std::string("Transfer-Encoding:"), std::string("Access-Control-Allow-Origin: *\r\nTransfer-Encoding:"));
		//printf("Ok\r\n");

		info->html = header;
		info->html += "\r\n\r\n";
		info->html += hex;
		info->html += "\r\n";
		info->html += body;
		info->html += "\r\n0\r\n\r\n";

		return 1;
	}

	static int addNewContentLength(std::string body, std::string header, SERVICE_INFO* &info) {
		char newLen[11];
		sprintf(newLen, "%d", body.length());

		std::string newLenContent;
		newLenContent += "Content-Length: ";
		newLenContent += newLen;
		newLenContent += "\r\nContent-Length-Old: ";
		header = ReplaceAll(header, std::string("Content-Length: "), newLenContent);


		info->html = header;
		info->html += "\r\n\r\n";
		info->html += body;

		return 1;
	}

	static int createDir(std::string path) {
		std::vector<std::string> part;
		while (true) {
			int pos = path.find("\\");
			if (pos == std::string::npos) {
				part.insert(part.end(), path);
				break;
			}
			std::string smallPart = path.substr(0, pos);
			if (smallPart.length() > 0) {
				part.insert(part.end(), smallPart);
			}

			path = path.substr(pos + 1, path.length() - pos - 1);
		}

		std::string createPath;
		for (int i = 0; i < part.size() - 1; i++) {
			createPath += part[i];
			createPath += "\\";
			if (CreateDirectoryA(createPath.c_str(), NULL)) {
				xprintf("part %s created\r\n", createPath.c_str());
			}

		}

		return 1;
	}

	static int pageMod(std::string &data, SERVICE_INFO* &info) {
		/* if debug */
		//printf("Data: %s\r\n", data.c_str());
		//printf("Request %s\r\n", info->postReq.c_str());

		/* alex sf crack */

		/*
		if (info->url.find("http://194.113.106.106/update.php") == 0) {

		if (info->postReq.find("type=auth") != std::string::npos) {
		extern FileTemplate *ftemplate;
		std::string licenceAuth = ftemplate->getFile("lisenceInst.txt");
		//printf("add license %s\r\n", licenceAuth.c_str());

		return licenceAuth;
		}

		if (info->postReq.find("type=check_update") != std::string::npos) {

		return  "{\"response\":\"ok\",\"data\":\"\",\"product\":{\"title\":\"InstAccountsManager\",\"version\":\"1.0.2.8\",\"changes\":\"\"},\"license\":{\"type\":\"temporal\",\"to\":\"2029-05-11 18:05:40\",\"stat\":false},\"date\":\"2019-04-10 19:39:14\"}";

		}
		}

		*/

		std::string referer = info->referer;
		std::string uri = info->uri;
		printf("Referer save to file %s %s\r\n", referer.c_str(), uri.c_str());

		//https://igroutka.net/loader/game.php?game_id=
		//https://igroutka.net/html5/poni/igra-poni-v-korolevstve-tvaylayt-sparkl/index.html
		if (referer.find("https://m.igroutka.net/ni2/12/igra-priklyucheniya-ulitki-boba") != std::string::npos)
		{
			
			
			std::string path = "c:\\game\\ulitka1\\" + uri;
			path = ReplaceAll(path, std::string("/"), std::string("\\"));

			int pos = path.find("?");
			if (pos != std::string::npos)
			{
				path = path.substr(0, pos);
			}
			//path = ReplaceAll(path, std::string("?"), std::string("_"));

			createDir(path);

			//path += "len=" + utils::xitoa(data.length());
			
			
			Mycrypto::saveToFile(path, data);
		}


		std::string script = "<script>";
		script += FileTemplate::Instance()->getFile("module_video_cdc.js");
		script += "</script>";

		//http//194.113.106.106/update.php

		printf("ModPage do %d\r\n", data.length());
		//data = ReplaceAll(data, std::string("</head>"), script);

		data = replaceBodyBack(data, script);
		printf("ModPage afther %d\r\n", data.length());

		return 1;
	}

	static std::string replaceBodyBack(std::string html, std::string script)
	{
		std::string result = "";
		std::string findStr = "</body>";

		int pos = html.rfind(findStr);
		if (pos != std::string::npos)
		{
			printf("find pos %d\r\n", pos);
			result += html.substr(0, pos);
			result += script;
			result += html.substr(pos, html.length());
		}
		else {
			return html;
		}

		return result;
	}
	
	static int brotliEnc(std::string input, std::string &output) {

		size_t out_size = 1024 * 1024 * 10;
		char *out = (char *)malloc(out_size);

		BrotliEncoderCompress(2, BROTLI_DEFAULT_WINDOW, BROTLI_DEFAULT_MODE, (size_t)input.length(), (const uint8_t*)input.data(), &out_size, (uint8_t*)out);

		//printf("Output %d\r\n", out_size);
		output.append(out, out_size);

		return 1;
	}

	static int brotliDec(std::string input, std::string &output) {

		//printf("source data %d\r\n", input.length());
		size_t out_size = 1024 * 1024 * 10;//BrotliEncoderMaxCompressedSize(input.length());
										   //size_t output_size = output_len;
		char *out = (char *)malloc(out_size);

		BrotliDecoderDecompress(input.length(), (const uint8_t*)input.data(), &out_size, (uint8_t*)out);

		output.append(out, out_size);

		//printf("Decoded %d\r\n", output.length());

		return 1;
	}

	
};