#include "framework/Singleton.h"
#include "framework/xLinux.h"
#include <string>
#include <vector>
#include <map>

class Db
{
	DECLARE_SINGLETON(Db)
public:
	Db();
	std::vector<std::map< std::string, std::string > > query(const char query[]);
	int insert(const char query[]);

	int Close();
	int Check();

private:
    pthread_mutex_t mutex;
};

#pragma once

#ifdef WIN32
#include "DB_win.h"
#endif

#ifdef LINUX
#include "DB_linux.h"
#endif

inline Db::Db()
{
    pthread_mutex_init(&mutex, NULL);

	std::string host = "185.43.6.164";
	int port = 3306;
	std::string user = "root";
	std::string pass = "";
	std::string database = "proxy";

#ifdef WIN32
	DB_win::Instance()->connect(host, user, pass, database, port);
#endif
	
#ifdef LINUX
	DB_linux::Instance()->connect(host, user, pass, database, port);
#endif
}

inline int Db::Check()
{
	char sql[1024];
	sprintf(sql, "INSERT INTO `memory_log` (`test`) VALUES ('%s');", "test");

	//printf("sql: %s\r\n", sql);
	int insert_id = Db::Instance()->insert(sql);
	if (insert_id <= 0)
	{
		printf("SQL ERROR %d\r\n", insert_id);
		exit(0);
		return 0;
	}
	printf("Insert ID: %d\r\n", insert_id);
	

	return 1;
}

inline int Db::insert(const char query[])
{
#ifdef WIN32
	return DB_win::Instance()->insert(query);
#endif

#ifdef LINUX
	return DB_linux::Instance()->insert(query);
#endif
}


inline std::vector<std::map< std::string, std::string > > Db::query(const char query[])
{
    
#ifdef WIN32
	return DB_win::Instance()->query(query);
#endif

#ifdef LINUX
	return DB_linux::Instance()->query(query);
#endif
}


inline int Db::Close()
{
#ifdef WIN32
	return DB_win::Instance()->close();
#endif

#ifdef LINUX
	return DB_linux::Instance()->close();
#endif

}


