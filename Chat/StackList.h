//
// Created by Jibril on 29.07.2020.
//
#pragma once
#include <string>
#include <vector>
#include "framework/xmutex.h"
#include "Object.h"

class StackList
{
public:
    //int inc;
    int stack_size;


    std::string name;

    int inc;
    int next_num;
    std::vector< PackData* > stack_vector;

    pthread_mutex_t mutex;

    StackList(std::string name)
    {
        inc = 0;
        stack_size = 0;
        next_num = 0;
        this->name = name;
        pthread_mutex_init(&mutex, NULL);
    }

    void add(PackData *data)
    {
        pthread_mutex_lock(&mutex);

        stack_vector.push_back(data);
        stack_size += data->len;
        this->inc++;

        pthread_mutex_unlock(&mutex);

        //printf("Stack(%s) size %d bytes %d count\r\n", name.c_str(), stack_size, sblist_getsize(stack_list));
    }

    PackData* get()
    {
        PackData* data = NULL;

        pthread_mutex_lock(&mutex);

        if (this->next_num < this->inc)
        {
            //printf("Stack(%s) bytest %d size %d getter %d\r\n", name.c_str(), stack_size, this->inc, this->next_num);

            data = stack_vector[this->next_num];// *((struct PackData**)sblist_get(stack_list, this->next_num));
            this->next_num++;
            stack_size -= data->len;
        }

        pthread_mutex_unlock(&mutex);

        return data;
    }

    void clearStack()
    {
        printf("Clear stack %d elem of %d\r\n", this->inc - next_num, this->inc);

        for(int i = next_num; i < inc; i++)
        {
            free(stack_vector[i]->buffer);
            delete stack_vector[i];
        }
        pthread_mutex_destroy(&mutex);
        //stack_vector.clear();

    }
};