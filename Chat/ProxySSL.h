#pragma once



#include "framework/FileTemplate.h"
#include "framework/Processor.h"



class ProxySSL {
public:

	static bool checkEndHostName(char symb) {
		char *regexCustomDomain = "1234567890-qwertyuiopasdfghjklzxcvbnm.";

		for (int k = 0; k < strlen(regexCustomDomain); k++) {
			//printf("finded %c %d = %d\r\n", regexCustomDomain[k], regexCustomDomain[k], symb);
			if (regexCustomDomain[k] == symb) {


				return true;
			}
		}
		return false;
	}


	static int getSslInfoNew(int socket, char(&host)[255]) {

		char hostReverce[255] = { 0 };
		int n = 0;
		int l = 0;
		int i = 0;


		char request[4096] = { 0 };

		//printf("start recv\r\n");
		int lenRecv = recv(socket, request, sizeof(request), 2);

		if (lenRecv <= 0) {
			printf("ERRRORRR socket %d\r\n", socket);
			return 0;
		}

		//printf("recv %d\r\n", lenRecv);

		//std::string reverseString;
		n = 0;
		for (i = 0; i < lenRecv; i++) {
			//printf("%c = %d\r\n", request[i], request[i]);
			if (request[i] == 0 && request[i + 1] == 23 && request[i + 2] == 0 && request[i + 3] == 0) {


				//-----------------
				for (int j = 1; j < 100; j++) {


					if (ProxySSL::checkEndHostName(request[i - j])) { // >= 45

																	   //printf("%c = %d res %d\r\n", request[i - j], request[i - j], SslHelper::checkEndHostName(request[i - j]));
						hostReverce[n] = request[i - j];

						n++;
					}
					else {

						int j = 0;
						for (int i = n - 1; i >= 0; i--) {
							//printf("%d %d %c\r\n", n - i, i, hostReverce[i]);
							host[j] = hostReverce[i];
							j++;

							//client->host[n - l - 1] = hostReverce[l];
						}
						host[j] = '\0';

						return 1;

					}
				}
				return 0;
			}
		}
		//printf("\r\n\r\n");
		return 0;
	}

	static void get_cn_and_alt_name(SERVICE_INFO *&pSvc, X509* certificate)
	{
		GENERAL_NAMES* subjectAltNames =
			(GENERAL_NAMES*)X509_get_ext_d2i(certificate, NID_subject_alt_name, NULL, NULL);

		//subjectAltNames = (GENERAL_NAMES*)X509_get_ext_d2i(certificate, NID_commonName, NULL, NULL);

		int i = 0, count = sk_GENERAL_NAME_num(subjectAltNames);

		for (i = 0; i < count; ++i)
		{
			GENERAL_NAME* generalName = sk_GENERAL_NAME_value(subjectAltNames, i);

			std::string name = std::string(reinterpret_cast<char*>(generalName->d.ia5->data));
			pSvc->altNames.push_back(name);
			//std::cout << "SA: '" << name << "'." << std::endl;

		}
		/*
		X509_NAME *subj = X509_get_subject_name(certificate);

		int idx = -1, success = 0;
		unsigned char *utf8 = NULL;
		do
		{
		if (!subj) break;

		idx = X509_NAME_get_index_by_NID(subj, NID_commonName, -1);
		if (!(idx > -1))  break;

		X509_NAME_ENTRY* entry = X509_NAME_get_entry(subj, idx);
		if (!entry) break;

		ASN1_STRING* data = X509_NAME_ENTRY_get_data(entry);
		if (!data) break;

		int length = ASN1_STRING_to_UTF8(&utf8, data);
		if (!utf8 || !(length > 0))  break;


		//pSvc->cnName.append(utf8, length);
		char cnName[255];
		sprintf(cnName, "%s", utf8);
		pSvc->cnName = cnName;
		success = 1;

		} while (0);
		*/

	}



	static int createSocketSsl(SERVICE_INFO *&pSvc) {


		//int connect = Socket::Connect(pSvc->sremote, pSvc->saddr);
		//if (connect > 0) {
			pSvc->ctx_remote = NULL;
			pSvc->ssl_remote = NULL;

			pSvc->ctx_remote = SSL_CTX_new(SSLv23_method());

			pSvc->ssl_remote = SSL_new(pSvc->ctx_remote);


			SSL_set_fd(pSvc->ssl_remote, pSvc->sremote);
			if (pSvc->hostname.length() > 0) {
				SSL_set_tlsext_host_name(pSvc->ssl_remote, pSvc->hostname.c_str());
			}



			int err = SSL_connect(pSvc->ssl_remote);
			if (err == 1) {

				X509 *cert = SSL_get_peer_certificate(pSvc->ssl_remote);
				printf("sert getted\r\n");

				get_cn_and_alt_name(pSvc, cert);

				if (cert) { X509_free(cert); }
				//Sleep(40000);

				return 1;
			}

			printf("SSl remote connect Error %d\r\n", err);


			if (pSvc->ctx) {
				SSL_CTX_free(pSvc->ctx_remote);
			}
			if (pSvc->ssl) {
				ProxySSL::ShutdownSSL(pSvc->ssl_remote);
			}


		//}

		return 0;
	}


	static int closeLocalSSL(SERVICE_INFO *&pSvc) {

		//SSL_shutdown(pSvc->ssl);
		if (pSvc->ctx) {
			SSL_CTX_free(pSvc->ctx);
		}
		if (pSvc->ssl) {
			SSL_free(pSvc->ssl);
		}


		ServerHelper::Close(pSvc->slocal);

		return 1;
	}

	static int closeRemoteSSL(SERVICE_INFO *&pSvc) {


		//SSL_shutdown(pSvc->ssl_remote);

		if (pSvc->ctx_remote) {
			SSL_CTX_free(pSvc->ctx_remote);
		}
		if (pSvc->ssl_remote) {
			SSL_free(pSvc->ssl_remote);
		}
		ServerHelper::Close(pSvc->sremote);

		return 1;
	}



	static void ShutdownSSL(SSL* ssl)
	{
		printf("shutdown ssl\r\n");
		if (ssl) {
			SSL_shutdown(ssl);
			SSL_free(ssl);
		}
	}


	static int writeSsl(SSL *ssl, const char *buf, int len) {

		int sent = 0;
		while (sent < len) {
			int m = SSL_write(ssl, buf + sent, len - sent);
			if (m < 0) return 1;
			sent += m;
		}
		return 1;
	}

	static int proxyRequestSsl(void* lpParameter) {
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;

		std::string req;

		while (TRUE) {

			char buffer[4096];
			int n = SSL_read(pSvcInfo->ssl, buffer, 4096);
			if (n <= 0) {
				break;
			}

			
			//req.append(buffer, n);

			//if ((req.find("GET ") == 0 || req.find("POST ") == 0) && req.find("\r\n\r\n") != std::string::npos) {

			//	ProxyHTTP::parseRequest(req, pSvcInfo);
			//	req = "";
			//}

			
			int len = SSL_write(pSvcInfo->ssl_remote, buffer, n);

		}


		if (!pSvcInfo->isClosed)
		{
			pSvcInfo->isClosed = TRUE;
			closeSsl(pSvcInfo->ssl, pSvcInfo->ssl_remote);
		}


		return 0;
	}

	static int proxyResponseSsl(void* lpParameter, connection *con) {
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;


		while (TRUE) {

			char buffer[4096];
			int n = SSL_read(pSvcInfo->ssl_remote, buffer, 4096);
			if (n <= 0) {
				//printf("Resp %d exit\r\n", n);
				break;
			}
			//if (!ProxyHTTP::chechHttp(pSvcInfo, buffer, n, true)) {
				int len = SSL_write(pSvcInfo->ssl, buffer, n);
			//}
		}


		if (!pSvcInfo->isClosed)
		{
			pSvcInfo->isClosed = TRUE;
			closeSsl(pSvcInfo->ssl_remote, pSvcInfo->ssl);
		}

		//SSL_shutdown(pSvcInfo->ssl_remote);
		//SSL_shutdown(pSvcInfo->ssl);


		//printf("close response\r\n");
		return 0;
	}

	static int closeSsl(SSL *first_ssl, SSL *second_ssl = NULL)
	{

		//LizzzMutex::Instance("socket")->lock();
		if (first_ssl == NULL)
		{
			printf("FIRST is NULL SSL\r\n");
		}

		//SSL_write(first_ssl, "", 0);
		//printf("step1\r\n");
		SSL_set_shutdown(first_ssl, SSL_SENT_SHUTDOWN);


		//printf("step2\r\n");

		//Socket::Close(first_fd);
		for (int i = 0; i < 4; i++) {
			int ret = SSL_shutdown(first_ssl);

			//printf("ret %d\r\n", ret);
			if (ret)
			{
				break;
			}

		}
		//printf("step3\r\n");

		if (second_ssl != NULL)
		{
			//printf("step4\r\n");
			SSL_write(second_ssl, "", 0); /* ����������� ���������� 0 ���� ����� �� ��������� ������ */

			int ret = SSL_shutdown(second_ssl);
			//printf("step5 %d\r\n", ret);

			/*
			SSL_set_shutdown(second_ssl, SSL_SENT_SHUTDOWN);

			Socket::Close(second_fd);
			printf("step4.5\r\n");
			for (int i = 0; i < 4; i++) {
			printf("step4.6\r\n");
			int ret = SSL_shutdown(second_ssl);
			printf("ret second %d\r\n", ret);
			if (ret)
			{
			break;
			}

			}
			*/
		}

		//LizzzMutex::Instance("socket")->unlock();
		//printf("step5\r\n");

		return 1;
	}

	static void sendError(int s, int code) {
		char buf[10] = { 5, code, 0, 1, 0,0,0,0, 0,0 };
		send(s, buf, 10, 0);
	}

	static int acceptSsl(SERVICE_INFO *pSvc) {
		//printf("acceptSsl!\r\n");
		//SERVICE_INFO *pSvc = (SERVICE_INFO*)lpParameter;

		/* if (genSert(pSvc) == 1) {

		}
		else {
		printf("Critical Error sert\r\n");
		return 0;
		}
		*/

		extern Mycrypto *mycrypto;

		pSvc->ctx = NULL;
		pSvc->ssl = NULL;

		while (true) {
			printf("while true! %s\r\n", pSvc->hostname.c_str());

			//#ifdef LINUX
			//		pthread_mutex_lock(&mutex);
			//#endif // LINUX

			HTTPS *certData = mycrypto->getCertData(pSvc);
			if (certData->serial <= 0) {
				printf("Critical Error sert\r\n");
				return 0;
			}

			printf("use sert! %d - %s\r\n", certData->serial, certData->hostname.c_str());

			pSvc->ctx = SSL_CTX_new(SSLv23_server_method());
			if (!pSvc->ctx) {
				printf("Error SSL_CTX_new\r\n");
				break;
			}



			//printf("CertLen %d KeyLen %d (%s)\r\n", Mycrypto::pem(pSvc->cert).length(), Mycrypto::key(pSvc->key).length(), pSvc->host);

			//pSvc->cert = mycrypto->httpsList[sertID].cert;
			//pSvc->key = mycrypto->httpsList[sertID].key;


			//printf("len key %s\r\n", key(pSvc->key));
			//int lsp = SSL_CTX_use_certificate(pSvc->ctx, mycrypto->httpsList[pSvc->sertId].cert);
			int lsp = SSL_CTX_use_certificate(pSvc->ctx, certData->cert);
			if (lsp <= 0) {
				printf("error ssl cert created\r\n");
				break;
			}

			//system("pause");


			//printf("Cert %s\r\n", Mycrypto::pem(pSvc->cert).c_str());
			//printf("use key!\r\n");

			//PEM_write_X509(stdout, pSvc->cert);
			//PEM_write_PrivateKey(stdout, pSvc->key, NULL, NULL, 0, 0, NULL);
			//int rds = SSL_CTX_use_PrivateKey(pSvc->ctx, mycrypto->httpsList[pSvc->sertId].key);
			//printf("Key status %d\r\n", rds);
			int rds = SSL_CTX_use_PrivateKey(pSvc->ctx, certData->key);
			if (rds <= 0) {
				printf("error ssl key created %s\r\n", pSvc->hostname.c_str());
				break;
			}



			pSvc->ssl = SSL_new(pSvc->ctx);
			SSL_set_fd(pSvc->ssl, pSvc->slocal);



			int err = SSL_accept(pSvc->ssl);

			printf("accept (%d) %s\r\n", err, pSvc->hostname.c_str());
			//#ifdef LINUX
			//		pthread_mutex_unlock(&mutex);
			//#endif // LINUX

			if (err == 1) {
				//printf("ssl init!\r\n");
				return TRUE;
			}
			else {

				printf("SSL Accept ERROR close connection %d %s\r\n", err, pSvc->hostname.c_str());
				//ServerHelper::Close(pSvc->slocal);
				//if (pSvc->ctx) {
				//	SSL_CTX_free(pSvc->ctx);
				//}
				//if (pSvc->ssl) {
				//	ProxySSL::ShutdownSSL(pSvc->ssl);
				//}
			}
			break;
		}


		SSL_CTX_free(pSvc->ctx);
		SSL_free(pSvc->ssl);


		return FALSE;
	}


	static int getHostNameBySsl(connection *con) {
		char host[255] = { 0 };
		if (ProxySSL::getSslInfoNew(con->socket, host))
		{
			printf("Host %s\r\n", host);
			if (host != NULL && strlen(host) > 3) {
				con->hostname = host;
				return 1;
			}


			con->hostname = "";
		}

		return 0;
	}


	static int resolveProxy(SERVICE_INFO *pSvc)
	{
		pSvc->isClosed = FALSE;

		Thread t1, t2;

		//printf("Start ssl thread\r\n");
		t1.Start((LPTHREAD_START_ROUTINE)proxyRequestSsl, pSvc);
		t2.Start((LPTHREAD_START_ROUTINE)proxyResponseSsl, pSvc);

		t1.WaitForEnd();
		t2.WaitForEnd();

		SSL_CTX_free(pSvc->ctx_remote);
		SSL_free(pSvc->ssl_remote);

		return 1;
	}

	static int init(SERVICE_INFO *pSvc) {
		printf("start ssl conenct\r\n");

		connection *con = pSvc->con;

		if (getHostNameBySsl(con))
		{
			con->setAttribute("hostname", con->hostname);
			pSvc->hostname = con->hostname;

			if (!createSocketSsl(pSvc)) {
				printf("Error create remote SSL connection %s\r\n", pSvc->hostname.c_str());
				ServerHelper::Close(pSvc->sremote);
				return FALSE;
			}

			if (acceptSsl(pSvc))
			{
				resolveProxy(pSvc);
				return FALSE;
			}

			ServerHelper::Close(pSvc->sremote);
			return FALSE;

		}

		ProxyHTTP::tunel(pSvc);
		return TRUE;

	}
	/*
	static bool chechHttpSsl(void* lpParameter, const char *buffer, int n) {
		SERVICE_INFO* pSvcInfo = (SERVICE_INFO*)lpParameter;
		if (n >= 5 && buffer[0] == 'H' && buffer[1] == 'T' &&buffer[2] == 'T' &&buffer[3] == 'P' &&buffer[4] == '/') {

			printf("HTML DETECTED Ssl\r\n");
			int htmlStart = 0;
			pSvcInfo->startBody = -1;
			char bufferTwo[4096];

			int isHeaderReaded = 0;
			pSvcInfo->html = "";

			pSvcInfo->html.append(buffer, n);
			if ((pSvcInfo->startBody = pSvcInfo->html.find("\r\n\r\n")) != std::string::npos) {
				pSvcInfo->startBody += 4;
				isHeaderReaded = 1;
			}

			printf("read header ok %d\r\n", isHeaderReaded);

			if (isHeaderReaded == 0) {

				while (true) {
					int n2 = SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096); // recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
					if (n2 <= 0)
					{
						printf("recv Error! GG %d\r\n", WSAGetLastError());
						break;
					}
					n += n2;

					pSvcInfo->html.append(bufferTwo, n2);
					if ((pSvcInfo->startBody = pSvcInfo->html.find("\r\n\r\n")) != std::string::npos) {
						pSvcInfo->startBody += 4;
						isHeaderReaded = 1;
						break;
					}

				}
			}

			Processor *proc = new Processor();
			proc->responce->header = pSvcInfo->html;
			int status = proc->parseResponceHeader();

			printf("Parse header %d\r\n", status);

			if (proc->responce->contentType.find("text/html") == std::string::npos) { // && proc->responce->contentType.find("application/x-") == std::string::npos) {
				printf("read more and sended not mitm %d\r\n", pSvcInfo->html.length());
				writeSsl(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length());

				delete proc;
				return true;
			}
			else {


				if (proc->responce->isChunk) {

					//pSvcInfo->startBody

					printf("Chunk content\r\n");
					int chunkLen = 0;
					int startChunkBlock = 0;
					int endChunkBlock = 0;
					std::string chunk = "";

					while (true) {
						int n = (endChunkBlock) ? endChunkBlock : pSvcInfo->startBody;

						//printf("start chunk block %d %d htmllen\r\n", n, pSvcInfo->html.length());

						if (n >= pSvcInfo->html.length()) {
							int len = SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
							if (len <= 0) {
								break;
							}
							pSvcInfo->html.append(bufferTwo, len);
							//printf("loaded +%d=%d\r\n", len, pSvcInfo->html.length());
						}

						chunk = "";

						while (true) {
							if (pSvcInfo->html[n] == 13 && pSvcInfo->html[n + 1] == 10) {
								chunkLen = strtol(chunk.c_str(), NULL, 16);
								startChunkBlock = n + 2;
								endChunkBlock = startChunkBlock + chunkLen;
								break;
							}
							chunk += (char)pSvcInfo->html[n];
							n++;

							if (n >= pSvcInfo->html.length()) {
								int len = SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096);// recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
								if (len <= 0) {
									break;
								}
								pSvcInfo->html.append(bufferTwo, len);
								//printf("loaded +%d=%d\r\n", len, pSvcInfo->html.length());
							}
						}

						//printf("Chunk(%d) %s %d (%d - %d)\r\n", chunk.length(), chunk.c_str(), chunkLen, startChunkBlock, endChunkBlock);

						if (chunkLen == 0) {
							break;
						}

						//if (endChunkBlock > pSvcInfo->html.length()) {
						//printf("%d > %d\r\n", endChunkBlock, pSvcInfo->html.length());
						while (endChunkBlock >= pSvcInfo->html.length()) {

							int len = SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
							if (len <= 0) {
								break;
							}

							pSvcInfo->html.append(bufferTwo, len);
							//printf("loaded2 +%d=%d\r\n", len, pSvcInfo->html.length());
						}

						//}



						endChunkBlock += 2;

					}

					
					ProxyHTTP::modify(pSvcInfo, proc);
					writeSsl(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length());
				}
				else if (proc->responce->contentLen >= 0) {
					printf("Length content\r\n");



					int lenBody = pSvcInfo->html.length() - pSvcInfo->startBody;
					//printf("len body %d\r\n", lenBody);
					//printf("diff %d == %d  nCout %d startBody %d\r\n", lenBody, proc->header.contentLen, nCount, startBody);
					if (lenBody >= proc->responce->contentLen) {
						//printf("gege");
						//return 1;
					}
					else {
						int contentLength2 = n;
						int n = 0;

						//int exit = 0;
						while (true) {
							n = SSL_read(pSvcInfo->ssl_remote, bufferTwo, 4096); //recv(pSvcInfo->sremote, bufferTwo, 4096, 0);
																				 //n = recv(pSvcInfo->sremote, buffer2, sizeof(buffer2), 0);
																				 //printf("qqqq %d\r\n", n);
							if (n <= 0) {
								break;
							}

							pSvcInfo->html.append(bufferTwo, n);
							contentLength2 += n;


							int lenSize = contentLength2 - pSvcInfo->startBody;

							//printf("exit %d >= %d\r\n", lenSize, proc->header.contentLen);
							if (lenSize >= proc->responce->contentLen) {
								break;
							}


						}
						//return 1;
					}

					ProxyHTTP::modify(pSvcInfo, proc);
					writeSsl(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length());

				}
				else if (proc->responce->codeInt == 304 || proc->responce->codeInt == 204) {
					printf("No body content\r\n");

					writeSsl(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length());

					delete proc;
					return TRUE;
					//SSL_write(pSvcInfo->ssl, html.data(), html.length());
				}
				else if (!proc->responce->isChunk && proc->responce->contentLen == -1) {
					printf("NO chunk no len %s\r\n", pSvcInfo->ip.c_str());

					writeSsl(pSvcInfo->ssl, pSvcInfo->html.data(), pSvcInfo->html.length());

					delete proc;
					return TRUE;
					
					//while (true) {
					//int n = recv(pSvcInfo->sremote, bufferTwo, 1024, 0);;
					//if (n <= 0) {
					//break;
					//}
					//pSvcInfo->html.append(buffer, n);
					//}
					
					//SSL_write(pSvcInfo->ssl, html.data(), html.length());
					//break;
				}


				//printf("Sended %s %d/%d\r\n", pSvcInfo->host, sended, pSvcInfo->html.length());
			}

			delete proc;
			return TRUE;
		}
		else {
			return FALSE;
		}

		
	}
	*/
	
};