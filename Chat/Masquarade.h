#include "framework/Singleton.h"
//#include "StackList.h"
#include "Object.h"
#include <vector>
#include <map>
#include "sblist.h"

static int pack_count = 0;

struct Pck
{
    char buffer[1024];
    int len;
};

class ClientObj
{
public:
    int client_socket;
    HANDLE ptr;
    volatile int done;
    int is_close;

    int inc;
    int next_num;
    std::vector< PackData* > stack_vector;
    pthread_mutex_t mutex;

    ClientObj()
    {
        done = 0;
        is_close = 0;
        inc = 0;
        next_num = 0;
        pthread_mutex_init(&mutex, NULL);
    }

    void add(char *buffer, int n)
    {
        PackData *pack = new PackData(buffer, n);
        pthread_mutex_lock(&mutex);
        stack_vector.push_back(pack);
        inc++;
        pthread_mutex_unlock(&mutex);

    }

    PackData* get()
    {

        PackData* data = NULL;
        pthread_mutex_lock(&mutex);
        if (this->next_num < this->inc)
        {
            data = stack_vector[this->next_num];
            this->next_num++;
        }
        pthread_mutex_unlock(&mutex);
        return data;
    }

    void clearStack()
    {
        printf("Clear stack %d elem of %d\r\n", this->inc - next_num, this->inc);

        pthread_mutex_lock(&mutex);
        for(int i = next_num; i < inc; i++)
        {
            PackData *data = stack_vector[i];

            free(data->buffer);
            delete data;
        }
        pthread_mutex_unlock(&mutex);
    }
};


class Massquarade
{
    DECLARE_SINGLETON(Massquarade)
public:
    Massquarade();
    pthread_mutex_t mutex;
    sblist *con_list;

    int collect();

    ClientObj* getClient(int socket);
    //ClientObj* createClient(int socket);
    //void close(ClientObj* req);
    void setClose(int socket);

    int send(int client_socket, char *buffer, int n);

    //int asyncSender(int client_socket, char* buffer, int n);
};

#pragma once

inline Massquarade::Massquarade()
{
    pthread_mutex_init(&mutex, NULL);
    con_list = sblist_new(sizeof (ClientObj*), 8);
}

inline void Massquarade::setClose(int socket)
{

    //pthread_mutex_lock(&mutex);
    ClientObj* req = getClient(socket);// client_list[socket];
    if(req) req->is_close = 1;
    //pthread_mutex_unlock(&mutex);
}

static void* thread_sender(void* lpParameter)
{
    ClientObj* req = (ClientObj*)lpParameter;

    printf("Send to client %d\r\n", req->client_socket);


    while (TRUE)
    {
        if (req->is_close == 1)
            break;

        PackData* data = req->get();

        if (data)
        {
            if (data->len == 7 && data->buffer[0] == '2' && data->buffer[1] == '5')
            {
                //req->is_close = 1;
                break;
            }



            int snd = send(req->client_socket, data->buffer, data->len, MSG_NOSIGNAL);

            free(data->buffer);
            delete data;

            if(snd <= 0)
            {
            //    req->is_close = 1;
                break;
            } //else {
            //    printf("send %d byest\r\n", snd);
            //}

        }
        else {

            //printf(".");
            utils::xsleep(100);
        }

    }

    shutdown(req->client_socket, 2);
    req->done = 1;

}

/*
inline void Massquarade::close(ClientObj* req)
{
    

} */

inline int Massquarade::collect()
{
    pthread_mutex_lock(&mutex);

    size_t i;
    for(i=0;i<sblist_getsize(con_list);) {
        ClientObj* req = *((ClientObj**)sblist_get(con_list, i));

        if(req->done) {
            joinThread(req->ptr);
            req->clearStack();
            sblist_delete(con_list, i);
            delete req;
        } else
            i++;
    }
    pthread_mutex_unlock(&mutex);

    return 1;
}

inline ClientObj* Massquarade::getClient(int socket)
{
    ClientObj *req = NULL;

    pthread_mutex_lock(&mutex);


    size_t i;
    for(i=0;i<sblist_getsize(con_list);) {
        ClientObj *tmp = *((ClientObj **) sblist_get(con_list, i));
        if(tmp->client_socket == socket)
        {
            req = tmp;
            break;
        } else
            i++;
    }

    pthread_mutex_unlock(&mutex);

    return req;
}



inline int Massquarade::send(int client_socket, char *buffer, int n)
{

    ClientObj* req = getClient(client_socket);

    if (!req)
    {
        collect();

        req = new ClientObj();
        req->client_socket = client_socket;

        pthread_mutex_lock(&mutex);
        sblist_add(con_list, &req);
        pthread_mutex_unlock(&mutex);

        req->ptr = createThread((LPTHREAD_START_ROUTINE)thread_sender, req);
        //if(pthread_create(&req->ptr, 0, thread_sender, req) != 0)
        //    printf("pthread_create failed. OOM?\n");


        printf("Total thread %d count Objects\r\n", sblist_getsize(con_list));

    }

    req->add(buffer, n);
   // if(req->is_close == 1)
    //{
    //    return 0;
    //}

    //if (n == 7 && buffer[0] == '2' && buffer[1] == '5')
    //{
    //    printf("Browser send close socket %d\r\n", req->client_socket);
    //    shutdown(req->client_socket, SHUT_RDWR);
    //    req->is_close = 1;
    //} else {
    //    req->add(buffer, n);
    //}



    return 0;
}
