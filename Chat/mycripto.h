#pragma once

#include "Object.h"


#ifdef LINUX
#include <ctime>
#else
#include <time.h>
#endif

class Mycrypto {
public:
	int serial;

	X509 *x509CaBuff = NULL;
	EVP_PKEY *prv_keyBuff = NULL;

	Mycrypto() {
		serial = std::time(0);
		printf("Set DATE SERIAL %d\r\n", serial);
		//X509 *x509CaBuff = X509_new();
	}

	~Mycrypto() {
		X509_free(x509CaBuff);
		EVP_PKEY_free(prv_keyBuff);

	}

	static int add_ext(X509 *cert, int nid, char *value)
	{
		X509_EXTENSION *ex;
		X509V3_CTX ctx;
		/* This sets the 'context' of the extensions. */
		/* No configuration database */
		X509V3_set_ctx_nodb(&ctx);
		/* Issuer and subject certs: both the target since it is self signed,
		* no request and no CRL
		*/
		X509V3_set_ctx(&ctx, cert, cert, NULL, NULL, 0);
		ex = X509V3_EXT_conf_nid(NULL, &ctx, nid, value);
		if (!ex)
			return 0;

		X509_add_ext(cert, ex, -1);
		X509_EXTENSION_free(ex);
		return 1;
	}

	int load_key(char *keyfile) {
		std::ifstream in1(keyfile);
		std::string root_ca_key_input((std::istreambuf_iterator<char>(in1)), std::istreambuf_iterator<char>());

		BIO *bio = BIO_new(BIO_s_mem());

		BIO_write(bio, root_ca_key_input.data(), root_ca_key_input.length());

		prv_keyBuff = PEM_read_bio_PrivateKey(bio, &prv_keyBuff, NULL, NULL);
		BIO_free(bio);
		return 1;
	}

	int load_cert(char *certfile) {
		std::ifstream in1(certfile);
		std::string root_ca_cert_input((std::istreambuf_iterator<char>(in1)), std::istreambuf_iterator<char>());

		BIO *bioCa = BIO_new(BIO_s_mem());
		BIO_write(bioCa, root_ca_cert_input.data(), root_ca_cert_input.length());

		x509CaBuff = PEM_read_bio_X509(bioCa, NULL, 0, NULL);
		BIO_free(bioCa);
		return 1;
	}

	int loadRootCa(char *rootCaFile, char *rootKeyFile) {

		std::cout << "Cert loaded: " << rootCaFile << "\r\n";
		std::cout << "Key loaded: " << rootKeyFile << "\r\n";
		//return 0;
		//x509CaBuff = ;
		if (!load_cert(rootCaFile)) {
			printf("Error load root ca cert\r\n");
		}
		//prv_keyBuff = load_key(rootKeyFile);
		if (!load_key(rootKeyFile)) {
			printf("Error load root ca key\r\n");
		}



		return 1;
	}



	/*
	analize threads
	*/

	/*
	int countThreads = 0;
	void initThread() {
		countThreads++;
	}
	void destroyThread() {
		countThreads--;
		pritnThreadInfo();
	}
	void pritnThreadInfo() {
		printf("Thread active %d\r\n", countThreads);
	}

	*/

	sf::safe_ptr< std::map< std::string, HTTPS* > > cert_list_safe;

	std::vector<HTTPS> httpsList;
	//std::map<std::string, HTTPS> httpsMap;

	typedef std::map<std::string, HTTPS> httpsMap;
	httpsMap fm;

	int coutHttps = 0;

	std::vector<char*> list;
	int block = 0;

	int incrumentSerial() {
		serial++;
		return serial;
	}


	HTTPS *getCertData(SERVICE_INFO *pSvc) {
		std::string hostname = pSvc->hostname.c_str();

		//LizzzMutex::Instance("cert")->lock();
		//HTTPS *https = ;
		

		if (!(*cert_list_safe)[hostname])
		{
			create_signed_cert_MVP(hostname, incrumentSerial(), pSvc->altNames, pSvc->cnName);
			//https = (*cert_list_safe)[hostname];
		}

		//LizzzMutex::Instance("cert")->unlock();

		return (*cert_list_safe)[hostname];
		//LizzzMutex::Instance("get_cert")->lock();
		/*
		HTTPS https = fm[hostname];
		if (https.serial == 0) {
			create_signed_cert_MVP(hostname, incrumentSerial(), pSvc->altNames, pSvc->cnName);
			https = fm[hostname];
		}

		//LizzzMutex::Instance("get_cert")->unlock();


		return https;
		*/
	}

	int create_signed_cert_MVP(std::string hostname, int serial, std::vector<std::string> altNames, std::string cnName) {
		//printf("Block %d\r\n", this->block);
		//if (this->block == 1) {
		//	//printf("Block %d\r\n", this->block);
		//	return -2;
		//}

		//this->block = 1;

		//printf("ready1\r\n");

		HTTPS *https = new HTTPS;
		https->serial = serial;
		https->hostname = hostname;
		//strcpy(https.hostname, host);
		//https.hostname = host;

		//SSL_load_error_strings();
		//SSLeay_add_ssl_algorithms();
		//printf("ready2\r\n");
		//seed PRNG
		//RAND_load_file("/dev/urandom", 128);
		//char rand_buff[16];
		//RAND_seed(rand_buff, 16);

		RSA *rsakey;
		/*while (RAND_status() != 1) {
		RAND_seed(&rsakey, 1024);
		}*/
		//printf("ready3\r\n");
		//X509 *req = 0;
		X509_NAME *subj = 0;
		//EVP_PKEY *pkey = 0;

		//stCur->hostname = "miracal.ru";

		//X509

		//pki_key_generate_rsa(2048, rsakey);
		rsakey = RSA_generate_key(2048, RSA_F4, NULL, NULL);
		//printf("ready4\r\n");
		//printf("check %s\r\n", https.hostname);
		// Create evp obj to hold our rsakey
		if (!(https->key = EVP_PKEY_new())) {
			printf("Could not create EVP object");
			return -1;
		}

		if (!(EVP_PKEY_assign_RSA(https->key, rsakey))) {
			printf("Could not assign RSA key to EVP object");
			return -1;
		}

		// create request object
		if (!(https->cert = X509_new())) {
			printf("Failed to create X509_REQ object");
			return -1;
		}
		//printf("ready5\r\n");

		X509_set_version(https->cert, 2);

		X509_set_pubkey(https->cert, https->key);

		subj = X509_get_subject_name(https->cert);

		//printf("ready6\r\n");
		if (!X509_gmtime_adj(X509_get_notBefore(https->cert), (long)-(60 * 60 * 24 * 365 * 5))) {
			printf("Error setting start date");
			return -1;
		}


		if (!X509_gmtime_adj(X509_get_notAfter(https->cert), (long)60 * 60 * 24 * 365 * 10)) {
			printf("Error setting end date");
			return -1;
		}

		//X509_REQ_set_pubkey(https.cert, https.key);
		//STACK_OF(X509_EXTENSION) *exts = NULL;
		//exts = sk_X509_EXTENSION_new_null();
		//add_ext(https.cert, NID_subject_alt_name, "email:steve@openssl.org");
		//X509_REQ_add_extensions

		//X509_REQ_add_extensions(x, exts);
		//sk_X509_EXTENSION_pop_free(exts, X509_EXTENSION_free);


		X509_NAME_add_entry_by_txt(subj, "O",
			MBSTRING_ASC, (unsigned char *)"Certificate", -1, -1, 0);


		//X509_NAME_add_entry_by_txt(subj, "CN",
		//	MBSTRING_ASC, (unsigned char *)https.hostname, -1, -1, 0);

		//printf("CN NAME: %s\r\n", cnName.c_str());
		X509_NAME_add_entry_by_txt(subj, "CN",
			MBSTRING_ASC, (unsigned char *)https->hostname.c_str(), -1, -1, 0);


		//X509_NAME_add_entry_by_txt(subj, "subjectAltName",
		//	MBSTRING_ASC, (unsigned char *)"*.aferon.com", -1, -1, 0);



		X509_NAME_add_entry_by_txt(subj, "OU",
			MBSTRING_ASC, (unsigned char *)"Domain Control Validated", -1, -1, 0);

		std::string buildSubjectAltNamesDns;

		//char Buffer[512];
		// Format the value
		//sprintf(Buffer, "DNS:%s, DNS:www.aferon.com", "aferon.com");

		//add_ext(https.cert, NID_subject_alt_name, Buffer);

		for (int i = 0; i < altNames.size(); i++) {
			buildSubjectAltNamesDns += "DNS:" + altNames[i] + ",";

			//printf("DNS:%s\r\n", altNames[i].c_str());
			//char Buffer[512];
			// Format the value
			//sprintf(Buffer, "DNS:%s", "af.com");

			//add_ext(https.cert, NID_subject_alt_name, Buffer);


		}

		buildSubjectAltNamesDns = buildSubjectAltNamesDns.substr(0, buildSubjectAltNamesDns.length() - 1);

		//printf("altNames: %s\r\n", buildSubjectAltNamesDns.c_str());
		add_ext(https->cert, NID_subject_alt_name, const_cast<char*>(buildSubjectAltNamesDns.c_str()));

		//X509_NAME_add_entry_by_txt(subj, "subjectAltName",
		//	MBSTRING_ASC, (unsigned char *)altNames[i].c_str(), -1, -1, 0);

		if (X509_set_subject_name(https->cert, subj) != 1) {
			printf("Error adding subject to request");
			return -1;
		}
		//printf("ready7\r\n");

		ASN1_INTEGER_set(X509_get_serialNumber(https->cert), https->serial);



		//printf("ready8\r\n");
		X509_set_issuer_name(https->cert, X509_get_subject_name(x509CaBuff));

		//printf("ready8.5\r\n");
		if (!(X509_sign(https->cert, prv_keyBuff, EVP_sha256()))) {
			printf("error sign");
			return -1;
		}

		
		(*cert_list_safe)[hostname] = https;



		return https->serial;// this->find(host);
							//return 1;
	}

	
	static int findToChar(char *string, char *find = "", int len = -1, int offset = 0) {

		int pos = -1;
		int stringLen = len;
		if (len == -1) {
			stringLen = strlen(string);
		}

		if (stringLen < strlen(find)) {

			return -1;
		}

		for (int i = offset; i < stringLen; i++) {
			//printf(",");
			int isFinded = -1;
			int ls = 0;

			for (int k = 0; k < strlen(find); k++) {
				//printf("+");
				if (string[i + k] == find[k]) {

					ls++;
				}
			}
			if (ls >= strlen(find)) {
				return i;
			}
		}
		return pos;
	}


	static int createMitmRootSA(char *outCertFile, char *outKeyFile) {


		RSA *rsakey = 0;

		X509 *req = 0;
		X509_NAME *subj = 0;

		EVP_PKEY *pkey = 0;

		int serial = std::time(0) * 10001;

		rsakey = RSA_generate_key(2048, RSA_F4, NULL, NULL);


		// Create evp obj to hold our rsakey
		if (!(pkey = EVP_PKEY_new()))
			printf("Could not create EVP object");

		if (!(EVP_PKEY_set1_RSA(pkey, rsakey)))
			printf("Could not assign RSA key to EVP object");

		// create request object
		if (!(req = X509_new()))
			printf("Failed to create X509_REQ object");

		X509_set_version(req, 2);

		X509_set_pubkey(req, pkey);

		subj = X509_get_subject_name(req);


		if (!X509_gmtime_adj(X509_get_notBefore(req), 0))
			printf("Error setting start date");

		if (!X509_gmtime_adj(X509_get_notAfter(req), (long)60 * 60 * 24 * 365 * 60))
			printf("Error setting end date");

		if (X509_set_subject_name(req, subj) != 1)
			printf("Error adding subject to request");

		ASN1_INTEGER_set(X509_get_serialNumber(req), serial);



		X509_NAME_add_entry_by_txt(subj, "O",
			MBSTRING_ASC, (unsigned char *)"Certificate", -1, -1, 0);
		X509_NAME_add_entry_by_txt(subj, "CN",
			MBSTRING_ASC, (unsigned char *)"California", -1, -1, 0);


		add_ext(req, NID_basic_constraints, "critical,CA:TRUE");
		add_ext(req, NID_key_usage, "critical, cRLSign, digitalSignature, keyCertSign");
		add_ext(req, NID_authority_key_identifier, "keyid:always, issuer:always");
		add_ext(req, NID_ext_key_usage, "critical, serverAuth, clientAuth, emailProtection, timeStamping, msEFS, msCTLSign");
		add_ext(req, NID_subject_key_identifier, "hash");

		X509_set_issuer_name(req, subj);


		if (!(X509_sign(req, pkey, EVP_sha256())))
			printf("Error signing request");
		else {
			printf("save public cert %s/%s\r\n", outCertFile, outKeyFile);
			//printf("%s\r\n", pem(req));
			Mycrypto::saveToFile(outCertFile, pem(req));
			Mycrypto::saveToFile(outKeyFile, key(pkey));
			//savePublicToFile(outCertFile, req);
			//savePrivateToFile(outKeyFile, pkey);
		}

		return 1;
	}

	static int saveToFile(std::string path, std::string data) {

		std::ofstream output(path.c_str(), std::ios_base::out | std::ios::binary);
		if (output.is_open()) {
			output.write(data.data(), data.length());
		}
		output.close();

		return 1;
	}

	int savePublicToFile(char *file, X509 *req) {
		FILE *fp;

		if (!(fp = fopen(file, "w"))) {
			printf("Error writing to public key file");
		}
		if (PEM_write_X509(fp, req) != 1)
			printf("Error while writing public key");
		fclose(fp);

		return 1;
	}

	static int findSymbolMirror(std::string &str, char split) {
		//std::cout << str;
		for (int i = str.length(); i >= 0; i--) {
			if (str[i] == split) {
				return i;
			}
		}
		return -1;
	}

	static std::string getExePath() {

#ifdef LINUX

		char result[PATH_MAX];
		ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
		std::string toRunable = std::string(result, (count > 0) ? count : 0);

		int pos = findSymbolMirror(toRunable, '/');
		//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
		if (pos != -1) {
			toRunable = toRunable.substr(0, pos);
		}

		return toRunable;
#else

		char ownPth[MAX_PATH];

		// Will contain exe path
		HMODULE hModule = GetModuleHandle(NULL);
		if (hModule == NULL)
		{
			printf("Executable path NULL\r\n");
			return "";

		}

		// When passing NULL to GetModuleHandle, it returns handle of exe itself
		GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));

		// Use above module handle to get the path using GetModuleFileName()
		std::string pathS = ownPth;


		int pos = findSymbolMirror(pathS, '\\');
		//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
		if (pos != -1) {
			pathS = pathS.substr(0, pos);
		}

		//printf("path %s\r\n", pathS.c_str());
		/*
		if (merge.length() > 0) {
		pathS += "\\";
		pathS.append(merge.c_str(), merge.length());
		}
		printf("Usage %s\r\n", pathS.c_str());
		*/
		return pathS;
#endif


	}

	int savePrivateToFile(char *file, EVP_PKEY *pkey) {
		FILE *fp;

		if (!(fp = fopen(file, "w"))) {
			printf("Error writing to private key file");
		}


		if (PEM_write_PrivateKey(fp, pkey, NULL, NULL, 0, 0, NULL) != 1)
			printf("Error while writing private key");

		fclose(fp);
		return 1;
	}

	static std::string pem(X509* x509)
	{
		BIO * bio_out = BIO_new(BIO_s_mem());
		PEM_write_bio_X509(bio_out, x509);
		BUF_MEM *bio_buf;
		BIO_get_mem_ptr(bio_out, &bio_buf);
		std::string pem = std::string(bio_buf->data, bio_buf->length);
		BIO_free(bio_out);
		return pem;
	}

	static std::string key(EVP_PKEY *pkey)
	{
		BIO * bio_out = BIO_new(BIO_s_mem());
		PEM_write_bio_PrivateKey(bio_out, pkey, NULL, NULL, 0, 0, NULL);
		BUF_MEM *bio_buf;
		BIO_get_mem_ptr(bio_out, &bio_buf);
		std::string pem = std::string(bio_buf->data, bio_buf->length);
		BIO_free(bio_out);
		return pem;
	}


};