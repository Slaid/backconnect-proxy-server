#include "framework/Singleton.h"

#ifdef LINUX
#include "sys/times.h"
#include "sys/vtimes.h"
#endif

class InfoProcSelf
{
	DECLARE_SINGLETON(InfoProcSelf)
public:
	InfoProcSelf();
	std::string getInfo();

private:
};

#pragma once

#ifdef LINUX
static int parseLine(char* line) {
	// This assumes that a digit will be found and the line ends in " Kb".
	int i = strlen(line);
	const char* p = line;
	while (*p <'0' || *p > '9') p++;
	line[i - 3] = '\0';
	i = atoi(p);
	return i;
}

static int getValue() { //Note: this value is in KB!
	FILE* file = fopen("/proc/self/status", "r");
	int result = -1;
	char line[128];

	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "VmSize:", 7) == 0) {
			result = parseLine(line);
			break;
		}
	}
	fclose(file);
	return result;
}

static clock_t lastCPU, lastSysCPU, lastUserCPU;
static int numProcessors;

static void init_detect_proc() {
	FILE* file;
	struct tms timeSample;
	char line[128];

	lastCPU = times(&timeSample);
	lastSysCPU = timeSample.tms_stime;
	lastUserCPU = timeSample.tms_utime;

	file = fopen("/proc/cpuinfo", "r");
	numProcessors = 0;
	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "processor", 9) == 0) numProcessors++;
	}
	fclose(file);
}

static  double getCurrentValue() {
	struct tms timeSample;
	clock_t now;
	double percent;

	now = times(&timeSample);
	if (now <= lastCPU || timeSample.tms_stime < lastSysCPU ||
		timeSample.tms_utime < lastUserCPU) {
		//Overflow detection. Just skip this value.
		percent = -1.0;
	}
	else {
		percent = (timeSample.tms_stime - lastSysCPU) +
			(timeSample.tms_utime - lastUserCPU);
		percent /= (now - lastCPU);
		percent /= numProcessors;
		percent *= 100;
	}
	lastCPU = now;
	lastSysCPU = timeSample.tms_stime;
	lastUserCPU = timeSample.tms_utime;

	return percent;
}
#endif

InfoProcSelf::InfoProcSelf()
{

}

inline std::string InfoProcSelf::getInfo()
{
	std::string result;

#ifdef LINUX

	init_detect_proc();


	result = "MEM: " + utils::xitoa(getValue() / 1024) + "Kb<br />\r\n";
	result += "NUM PROCESSORS USAGE: " + utils::xitoa(numProcessors) + "<br />\r\n";

	char buf[32];
	sprintf(buf, "%10.3f", getCurrentValue());
	result += "PERCENT PROC USAGE: " + std::string(buf) + "<br />\r\n";

	char buf2[32];
	sprintf(buf2, "%i", lastCPU);
	result += "CPU: Last " + std::string(buf2) + " ms<br />\r\n";

	sprintf(buf2, "%i", lastSysCPU);
	result += "CPU: Last SYS " + std::string(buf2) + " ms<br />\r\n";

	sprintf(buf2, "%i", lastUserCPU);
	result += "CPU: last User CPU " + std::string(buf2) + " ms<br />\r\n";
#endif

	return result;
}