#include "framework/Singleton.h"
#include "framework/xmutex.h"
#include "framework/FileTemplate.h"

class ErrorLog
{
	DECLARE_SINGLETON(ErrorLog)
public:
	ErrorLog();
	void add(std::string log);
	void save(std::string str);
	int saveToFile(std::string path, std::string data);
	std::string file_get_contents(std::string path);

private:
	 std::vector< std::string >  error_log;
};

#pragma once

inline ErrorLog::ErrorLog()
{

}

inline void ErrorLog::add(std::string str)
{
	LizzzMutex::Instance("error")->lock();
	error_log.push_back(str);
	LizzzMutex::Instance("error")->unlock();
}

inline int ErrorLog::saveToFile(std::string path, std::string data) {

    uint64_t ms = utils::timeSinceEpochMillisec() / 1000;

    data = utils::xitoa(ms) + ": " + data + "<br />\n";
	std::ofstream output(path.c_str(), std::ios_base::out | std::ios::binary | std::ios_base::app);
	if (output.is_open()) {
		printf("Writen %d bytes\r\n", data.length());
		output.write(data.data(), data.length());
	}
	else {
		printf("Error opened file %s\r\n", path.c_str());
		return 0;
	}
	output.close();

	return 1;
}

inline std::string ErrorLog::file_get_contents(std::string path) {
	std::string data = "";
	char* buffer;

	int length = 0;
	std::ifstream input(path.c_str(), std::ios::binary);

	char buf[1024];
	//int length = 0;
	if (input) {
		// get length of file:
		input.seekg(0, input.end);
		length = input.tellg();
		input.seekg(0, input.beg);


		while (true) {
			int part = sizeof(buf);
			if (length < part) {
				part = length;
			}
			input.read(buf, part);
			data.append(buf, part);

			length -= part;
			if (length == 0) {
				break;
			}
		}

		return data;
	}

	return "";
}


inline void ErrorLog::save(std::string str)
{
	printf("Save error_log.txt\r\n");
	LizzzMutex::Instance("error")->lock();
	saveToFile("error_log.txt", str);
	LizzzMutex::Instance("error")->unlock();
}