﻿#ifndef SOCKET_PROXY_H
#define SOCKET_PROXY_H

#include "framework/WS32_helper.h"
#include "framework/ConnectionManager.h"

#include "Object.h"
#include "mycripto.h"
Mycrypto *mycrypto = new Mycrypto;


#include "ProxyHTTP.h"
#include "ProxySSL.h"


#ifdef LINUX
typedef long LONG;
typedef unsigned int UINT;
typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef unsigned long DWORD_PTR;

#define MAKEWORD(a, b) ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b) ((LONG)(((WORD)(a)) | ((DWORD)((WORD)(b))) << 16))
#else
#endif

class MitmServer {
public:
	MitmServer(connection *con);
	int Client(connection *con);

	bool checkDomainName(char szName[100]);
	int requestResiver(connection *pSvc);

	int authController(connection *con);
	int reciveAuthHeader(connection *con);
};

static void sendError(int s, int code) {
	char buf[10] = { 5, code, 0, 1, 0,0,0,0, 0,0 };
	send(s, buf, 10, 0);
}

inline bool MitmServer::checkDomainName(char szName[100]) {
	for (int i = 0; i < strlen(szName); i++) {
		if (szName[i] == '.') {
			return true;
		}
	}
	printf("Error domain name, skipp! (%s)\r\n", szName);
	return false;

}
//std::string confidence = "qwe:qwe";

enum authmethod {
	AM_NO_AUTH = 0,
	AM_GSSAPI = 1,
	AM_USERNAME = 2,
	AM_INVALID = 0xFF
};

enum socksstate {
	SS_1_CONNECTED,
	SS_2_NEED_AUTH, /* skipped if NO_AUTH method supported */
	SS_3_AUTHED,
};

enum errorcode {
	EC_SUCCESS = 0,
	EC_GENERAL_FAILURE = 1,
	EC_NOT_ALLOWED = 2,
	EC_NET_UNREACHABLE = 3,
	EC_HOST_UNREACHABLE = 4,
	EC_CONN_REFUSED = 5,
	EC_TTL_EXPIRED = 6,
	EC_COMMAND_NOT_SUPPORTED = 7,
	EC_ADDRESSTYPE_NOT_SUPPORTED = 8,
};

static const char* auth_user = "qwe";
static const char* auth_pass = "qwe";

int check_auth_method(char *buf, int n, connection *con)
{
	
	//printf("Auth_3 %d\r\n", n);
	//for (int i = 0; i < n; i++)
	//{
	//	printf("%d", buf[i]);
	//}
	//printf("\r\n");
	
	
	

	if (buf[0] != 5)
	{
		printf("Invalid SOCKS5 proxy (%s)\r\n", buf);
		return AM_INVALID;
	}

	size_t idx = 1;
	if (idx >= n) return AM_INVALID;
	int n_methods = buf[idx];
	idx++;
	while (idx < n && n_methods > 0) {
		if (buf[idx] == AM_NO_AUTH) {
			if (!auth_user) return AM_NO_AUTH;
		}
		else if (buf[idx] == AM_USERNAME) {
			if (auth_user) return AM_USERNAME;
		}
		idx++;
		n_methods--;
	}
	return AM_INVALID;
}

static void send_auth_response(int fd, int version, int meth) {
	char buf[2];
	buf[0] = version;
	buf[1] = meth;
	send(fd, buf, 2, 0);
}

static void send_error(int fd, int ec) {
	/* position 4 contains ATYP, the address type, which is the same as used in the connect
	request. we're lazy and return always IPV4 address type in errors. */
	char buf[10] = { 5, ec, 0, 1 /*AT_IPV4*/, 0,0,0,0, 0,0 };
	send(fd, buf, 10, 0);
}

static enum errorcode check_credentials(char* buf, size_t n) {
	
	printf("Auth_credentals %d\r\n", n);
	for (int i = 0; i < n; i++)
	{
		printf("%c", buf[i]);
	}
	printf("\r\n");
	

	if (n < 5) return EC_GENERAL_FAILURE;
	if (buf[0] != 1) return EC_GENERAL_FAILURE;
	unsigned ulen, plen;
	ulen = buf[1];
	if (n < 2 + ulen + 2) return EC_GENERAL_FAILURE;
	plen = buf[2 + ulen];
	if (n < 2 + ulen + 1 + plen) return EC_GENERAL_FAILURE;
	char user[256], pass[256];
	memcpy(user, buf + 2, ulen);
	memcpy(pass, buf + 2 + ulen + 1, plen);
	user[ulen] = 0;
	pass[plen] = 0;

	printf("Auth: %s:%s\r\n", user, pass);

	if (!strcmp(user, auth_user) && !strcmp(pass, auth_pass)) return EC_SUCCESS;
	return EC_NOT_ALLOWED;
}



static int connect_socks_target(char *buf, size_t n, connection *con) {
	
	printf("Auth_resolve %d\r\n", n);
	for (int i = 0; i < n; i++)
	{
		printf("%c", buf[i]);
	}
	printf("\r\n");
	

	if (n < 5) return -EC_GENERAL_FAILURE;
	if (buf[0] != 5) return -EC_GENERAL_FAILURE;
	if (buf[1] != 1) return -EC_COMMAND_NOT_SUPPORTED; /* we support only CONNECT method */
	if (buf[2] != 0) return -EC_GENERAL_FAILURE; /* malformed packet */

	int af = AF_INET;
	size_t minlen = 4 + 4 + 2, l;
	char namebuf[256];
	//struct addrinfo* remote;

	switch (buf[3]) {
	case 4: /* ipv6 */
		af = AF_INET6;
		minlen = 4 + 2 + 16;
		/* fall through */
	case 1: /* ipv4 */
		if (n < minlen) return -EC_GENERAL_FAILURE;
		if (namebuf != inet_ntop(af, buf + 4, namebuf, sizeof namebuf))
			return -EC_GENERAL_FAILURE; /* malformed or too long addr */
		break;
	case 3: /* dns name */
		l = buf[4];
		minlen = 4 + 2 + l + 1;
		if (n < 4 + 2 + l + 1) return -EC_GENERAL_FAILURE;
		memcpy(namebuf, buf + 4 + 1, l);
		namebuf[l] = 0;
		break;
	default:
		return -EC_ADDRESSTYPE_NOT_SUPPORTED;
	}
	unsigned short port;
	port = (buf[minlen - 2] << 8) | buf[minlen - 1];

	/* there's no suitable errorcode in rfc1928 for dns lookup failure */

	port = (port == 80) ? 80 : 443;

	con->hostname = namebuf;
	printf("Resolve %s:%d\r\n", namebuf, port);

	addrinfo *remote = NULL;
	addrinfo hint = { 0 };

	int nRet = getaddrinfo(namebuf, NULL, &hint, &remote);
	if (nRet != 0)
	{
		return -EC_GENERAL_FAILURE;
	}

	sockaddr_in svr = { 0 };
	svr.sin_family = AF_INET;
	svr.sin_addr = ((sockaddr_in*)remote->ai_addr)->sin_addr;
	svr.sin_port = htons(port);
	con->saddr = svr;

	con->remote_ip = inet_ntoa(con->saddr.sin_addr);
	con->remote_port = ntohs(con->saddr.sin_port);

	//if (resolve(namebuf, port, &remote)) return -EC_GENERAL_FAILURE;

	printf("resolve %s:%d => %s:%d\r\n", namebuf, port, con->remote_ip.c_str(), con->remote_port);

	con->sremote = socket(AF_INET, SOCK_STREAM, 0);
	if (con->sremote == -1) {
	eval_errno:
		if (con->sremote != -1) ServerHelper::Close(con->sremote);
		freeaddrinfo(remote);
		switch (errno) {
		case ETIMEDOUT:
			return -EC_TTL_EXPIRED;
		case EPROTOTYPE:
		case EPROTONOSUPPORT:
		case EAFNOSUPPORT:
			return -EC_ADDRESSTYPE_NOT_SUPPORTED;
		case ECONNREFUSED:
			return -EC_CONN_REFUSED;
		case ENETDOWN:
		case ENETUNREACH:
			return -EC_NET_UNREACHABLE;
		case EHOSTUNREACH:
			return -EC_HOST_UNREACHABLE;
		case EBADF:
		default:
			perror("socket/connect");
			return -EC_GENERAL_FAILURE;
		}
	}

	//printf("Start connect\r\n");
	if (!ServerHelper::Connect(con->sremote, con->saddr))
		return FALSE;

	//if (connect(fd, remote->ai_addr, remote->ai_addrlen) == -1)
	//	return FALSE;

	freeaddrinfo(remote);

	return con->sremote;
}

inline int MitmServer::reciveAuthHeader(connection *con)
{
	int n;
	char buf[1024];
	con->state = SS_1_CONNECTED;
	int am;
	int ret;

	while ((n = recv(con->socket, buf, sizeof buf, 0)) > 0)
	{
		switch (con->state) {
			case SS_1_CONNECTED:
				am = check_auth_method(buf, n, con);
				if (am == AM_NO_AUTH) con->state = SS_3_AUTHED;
				else if (am == AM_USERNAME) con->state = SS_2_NEED_AUTH;
				send_auth_response(con->socket, 5, am);
				if (am == AM_INVALID) return FALSE;
				break;

			case SS_2_NEED_AUTH:
				ret = check_credentials(buf, n);
				send_auth_response(con->socket, 1, ret);
				if (ret != EC_SUCCESS)
					return FALSE;
				con->state = SS_3_AUTHED;
				break;

			
			case SS_3_AUTHED:
				ret = connect_socks_target(buf, n, con);
				printf("Socket remote %d\r\n", con->sremote);

				if (ret < 0) {
					send_error(con->socket, ret*-1);
					return FALSE;
				}
				
				send_error(con->socket, EC_SUCCESS);
				return TRUE;

		}


	}

	return FALSE;

	/*
	con->first_request = "";
	con->proxyType = 0; 
	char buffer[4096];


	recv(con->socket, buffer, 2, 2);



	if (buffer[0] == 5 && buffer[1] == 1) {
		
		printf("Socks auth Start\r\n");

		
		if (SocksAuth(con->socket, "qwe", "qwe", TRUE))
		{
			printf("Socks auth ok\r\n");
			//char sendBuff[] = { 5,  0 };
			//send(con->socket, sendBuff, 2, 0);
			//Sleep(1);

			int len = recv(con->socket, buffer, 4096, 0);

			printf("ConfigSite len %d\r\n", len);

			return FALSE;

			if (!this->GetRequest(con))
			{
				xprintf("Get proxy info Error\r\n");
				return FALSE;
			}

			//sendError(con->socket, 0);

			
		}
		else {
			printf("Error login or password\r\n");
		}

		con->proxyType = 1;
		return TRUE;
	}

	
	int len = recv(con->socket, buffer, sizeof(buffer), 0);
	if (len <= 0)
	{
		return FALSE;
	}

	con->first_request.append(buffer, len);

	if (con->first_request.find("CONNECT") == 0 || con->first_request.find("GET") == 0 || con->first_request.find("POST") == 0)
	{
		
		while (true)
		{
			if (con->first_request.find("\r\n\r\n") != std::string::npos)
			{
				break;
			}

			int len = recv(con->socket, buffer, sizeof(buffer), 0);
			if (len <= 0)
			{
				break;
			}
			con->first_request.append(buffer, len);
		}
		con->proxyType = 2;

		return TRUE;
	}

	return FALSE;
	*/
}

/* https proxy auth */
int MitmServer::authController(connection *con)
{
	/*
	if (reciveAuthHeader(con))
	{
		printf("connection remote ok\r\n");
		return TRUE;

	}
	*/
	return false;

	/*
	if (con->proxyType == 1)
	{
		printf("CONNECT SOCKS5/PROXY CLIENT\r\n");
		//if (!this->GetRequest(con))
		//{
		//	xprintf("Get proxy info Error\r\n");
		//	return FALSE;
		//}

		return TRUE;
	}

	if (con->proxyType == 2)
	{
		printf("CONNECT HTTPS/PROXY CLIENT\r\n");

		//printf("REQ: %s\r\n", con->first_request.c_str());

		Processor *proc = new Processor(con->first_request);
		proc->parseRequest();

		std::string basic = proc->getHeader("proxy-authorization");
		int pos = 0;
		if (basic.length() > 0 && (pos = basic.find("Basic ")) == 0)
		{
			//int pos = basic.find("Basic ");
			basic = basic.substr(6, basic.length() - 6);
			//printf("auth: %s\r\n", basic.c_str());
			basic = websocketpp::base64_decode(basic);
			//printf("auth: %s\r\n", basic.c_str());

			if (basic.find(confidence) == 0)
			{
				if (proc->host.length() == 0)
					return FALSE;


				con->saddr = ServerHelper::getIpByHost(proc->host, proc->port);

				printf("Auth ok %s:%d\r\n", proc->host.c_str(), proc->port);


				return TRUE;
			}
			
		}

		else {
			std::string html = "HTTP/1.0 407 Proxy Authentication Required\r\n"
				"Proxy-Authenticate: Basic realm=\"proxy\"\r\n"
				"Connection: close\r\n"
				"Content-type: text/html; charset=utf-8\r\n"
				"\r\n"
				"<html><head><title>407 Proxy Authentication Required</title></head>\r\n"
				"<body><h2>407 Proxy Authentication Required</h2><h3>Access to requested resource disallowed by administrator or you need valid username/password to use this resource</h3></body></html>\r\n";
			send(con->socket, html.data(), html.length(), 0);
		}
	}

	

	return FALSE;
	*/
}

/*
inline bool MitmServer::Auth(connection *con) {
	//return authController(con);
	return false;


	char buffer[1024];
	int len = recv(con->socket, buffer, 3, 0);
	if (len <= 0)
	{
		return FALSE;
	}

	//printf("%d-%d-%d\r\n", buffer[0], buffer[1], buffer[2]);

	//std::string data;
	//data.append(buffer, len);
	//std::cout << data.c_str();

	if (len == 3 && buffer[0] == 5 && buffer[1] == 1) {
		char sendBuff[2] = { 5, 0 };
		send(con->socket, sendBuff, 2, 0);

		//xprintf("deb4\r\n");
		if (!this->GetRequest(con))
		{
			xprintf("Get proxy info Error\r\n");
			return 0;
		}

		return true;
	}
	else {
		char sendBuff[2] = { 5,1 };
		send(con->socket, sendBuff, 2, 0);

		for (int i = 0; i < len; i++) {
			printf("%d", buffer[i]);
		}
		printf(" Auth Error\r\n");
		return false;
	}
}
*/







/*
inline int MitmServer::connectWithRemote(connection *con) {



	// init service info
	//SERVICE_INFO *pSvc = initServiceInfo(con);

	con->sremote = socket(AF_INET, SOCK_STREAM, 0);

	xprintf("Connect %s => %s:%d\r\n", con->socket_ip.c_str(), con->remote_ip.c_str(), con->remote_port);

	//printf("debug %s\r\n", inet_ntoa(con->saddr.sin_addr));

	if (!Connect(con->sremote, con->saddr))
	{
		ServerHelper::Close(con->sremote);
		xprintf("not connect\r\n");
		sendError(con->socket, 1);
		return FALSE;
	}


	xprintf("success\r\n");

	//sendError(con->socket, 0); //socks5

	if(con->remote_port == 443)
	{
		std::string html = "HTTP/1.1 200 Connection established\r\n\r\n"; //https proxy
		int len = send(con->socket, html.data(), html.length(), 0);
		printf("Send %d\r\n", len);
	}
	else {
		int len = send(con->sremote, con->first_request.data(), con->first_request.length(), 0);
		printf("Send %d\r\n", len);
	}

	SERVICE_INFO *pSvc = new SERVICE_INFO;
	pSvc->slocal = con->socket;
	pSvc->sremote = con->sremote;
	pSvc->con = con;
	pSvc->keep_alive_cnt = 0;


	//xprintf("pSvc init\r\n");

	if (con->remote_port == 443)
	{
		if (getHostNameBySsl(con))
		{
			con->setAttribute("hostname", con->hostname);
			pSvc->hostname = con->hostname;

			if (!ProxySSL::createSocketSsl(pSvc)) {
				ServerHelper::Close(pSvc->sremote);
				delete pSvc;
				return FALSE;
			}
				
			acceptSsl(pSvc);

			ServerHelper::Close(pSvc->sremote);
			ConnectionManager::Instance("mitm")->closeConnection(con);
			delete pSvc;
			return TRUE;

		}
	}

	ProxyHTTP::tunel(pSvc);

	delete pSvc;
	return TRUE;

}

*/


int MitmServer::requestResiver(connection *con)
{

	SERVICE_INFO *pSvc = new SERVICE_INFO;
	pSvc->slocal = con->socket;
	pSvc->con = con;
	pSvc->keep_alive_cnt = 0;
	pSvc->sremote = con->sremote;

	int result = 0;

	if (con->remote_port == 80)
	{

		ProxyHTTP::tunel(pSvc);
		result = TRUE;
	}

	if (con->remote_port == 443)
	{
		//std::string html = "HTTP/1.1 200 Connection established\r\n\r\n"; //https proxy
		//int len = send(con->socket, html.data(), html.length(), 0);

		result = ProxySSL::init(pSvc);
	}

	delete pSvc;
	return result;

	/*

	std::string authRequest = con->first_request;
	Processor *proc = new Processor(authRequest);
	proc->parseRequest();

	if (con->remote_port == 443)
	{
		std::string html = "HTTP/1.1 200 Connection established\r\n\r\n"; //https proxy
		int len = send(con->socket, html.data(), html.length(), 0);
		printf("Send %d\r\n", len);
	}

	if (con->remote_port == 80)
	{

	}
	return 1;
	*/
}

static int checkAllow(int socket)
{
	/* check aviability server */
	char pingReq[2] = { 0 };
	int len = recv(socket, pingReq, 2, 2);
	if (len == 2)
	{
		printf("Ping %d-%d\r\n", pingReq[0], pingReq[1]);
	}
	if (pingReq[0] == 9 && pingReq[1] == 9) {
		len = recv(socket, pingReq, 2, 0);
		send(socket, pingReq, len, 0);
		xprintf("Check Init Dll\r\n");
		return 1;
	}

	return 0;
}

static int webInterface(connection *con)
{
	//return 0;
	
	//printf("start dump %d\r\n", socket);
	//
	/* server stat */
	char buff[4096];
	int len = recv(con->socket, buff, 4096, 2);

	if (len <= 0)
	{
		return 1;
	}

	std::string req = "";

	
	req.append(buff, len);

	if (req.find("GET /favicon.ico") == 0)
	{
		len = recv(con->socket, buff, 4096, 0);

		con->setAttribute("WebGui", "GET /favicon.ico");

		std::string body = "";
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		send(con->socket, responce.data(), responce.length(), 0);
		return 1;
	}


	if (req.find("GET /dump") == 0)
	{
		

		len = recv(con->socket, buff, 4096, 0);

		con->setAttribute("WebGui", req.substr(0, req.find("\r\n")));

		std::string body = "Server listener at: 8888 port\r\n";
		
		
		//LizzzMutex::Instance("setAttribute")->lock();
		std::vector<std::string> list = ConnectionManager::Instance("mitm")->dumpConnectionList();
		//LizzzMutex::Instance("setAttribute")->unlock();

		
		xprintf("dump size %d len %d\r\n", list.size(), len);

		body += "Size: " + utils::xitoa(list.size(), 10) + "\r\n";

		for (int i = 0; i < list.size(); i++)
		{
			//if (!list[i])
			//	continue;
			
			
			//body += "Socket: " + utils::xitoa(list[i]->socket, 10) + "\r\n";
			//body += "Ip: " + list[i]->socket_ip + "\r\n";
			//body += "Status: " + utils::xitoa(list[i]->status, 10) + "\r\n";

			
			//body += "Remote: " + list[i]->remote_ip + ":" + utils::xitoa(list[i]->remote_port, 10) + "\r\n";
			//body += "Hostname: " + list[i]->hostname + "\r\n";
			//
			//if(list[i].length() > 0)
			
			body += "<div class='item'>" + list[i] + "</div>";

			
			body += "\r\n";
		}
		
		
		//xprintf("end dump size %d\r\n", list.size());

		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		//len = recv(con->socket, buff, 2, 0);
		send(con->socket, responce.data(), responce.length(), 0);
		

		return 1;
	}

	if (req.find("GET /") == 0)
	{
		len = recv(con->socket, buff, 4096, 0);

		con->setAttribute("WebGui", "GET /");

		Sleep(10);

		std::string body = "index <a href='/dump'>dump</a>";
		std::string responce = "HTTP/1.1 200 OK\r\nContent-length: " + utils::xitoa(body.length(), 10) + "\r\nConnection: Close\r\nContent-Type: text/html\r\n\r\n" + body;
		send(con->socket, responce.data(), responce.length(), 0);
		return 1;
	}

	
	
	return 0;
}

int MitmServer::Client(connection *con)
{
	//xprintf("deb1\r\n");
	//con->setAttribute("status", "0");
	//con->status = 0;
	if (checkAllow(con->socket))
		return 0;
	//xprintf("deb2\r\n");
	if (webInterface(con))
		return 0;

	
	//xprintf("deb3\r\n");
	if (!reciveAuthHeader(con))
	{
		return 0;
	}
	
	//xprintf("deb4.4\r\n");
	//con->remote_ip = inet_ntoa(con->saddr.sin_addr);
	//con->remote_port = ntohs(con->saddr.sin_port);

	//printf("Connect to %s:%d\r\n", con->remote_ip.c_str(), con->remote_port);

	con->setAttribute("remote", con->remote_ip + ":" + utils::xitoa(con->remote_port, 10));

	//xprintf("deb5\r\n");
	if (con->remote_port == 443 || con->remote_port == 80) {} else
	{
		xprintf("Deny port %s:%d\r\n", con->remote_ip.c_str(), con->remote_port);
		return 0;
	}

	//xprintf("deb6\r\n");
	//if (!requestResiver(con))
	//{
		//xprintf("unaviabe remote server %s:%d\r\n", con->remote_ip.c_str(), con->remote_port);
		
	//	return 0;
	//}

	return requestResiver(con);
}

inline MitmServer::MitmServer(connection *con) {

	if(!this->Client(con))
	{
		ConnectionManager::Instance("mitm")->closeConnection(con);
	}
}

#endif SOCKET_PROXY_H