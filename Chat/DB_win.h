#ifdef WIN32
#include "framework/Singleton.h"
#pragma comment(lib, "libmysql.lib")
#include "mysql.h"

class DB_win
{
	DECLARE_SINGLETON(DB_win);
public:
	DB_win();

	int connect(std::string host, std::string user, std::string pass, std::string database, int port = 3306);
	std::vector<std::map<std::string, std::string> > query(const char query[]);
	int insert(const char query[]);
	int close();

	MYSQL mysql;
};

#pragma once
#include "framework/xLinux.h"

inline DB_win::DB_win()
{

}

inline int DB_win::connect(std::string host, std::string user, std::string pass, std::string database, int port)
{
	mysql_init(&mysql);
	mysql_real_connect(&mysql, host.c_str(), user.c_str(), pass.c_str(), database.c_str(), 3306, NULL, 0);
	printf("Mysql connect success (%s:%d) %s\r\n", host.c_str(), port, database.c_str());

	return 1;
}

inline int DB_win::insert(const char query[])
{
	int result = 0;

	if (mysql_query(&mysql, query) > 0) // ������. ���� ������ ���, �� ���������� ������
	{
		// ���� ���� ������, ...
		printf("%s", mysql_error(&mysql));  // ... ������ ��
		return result; // � �������� ������
	}
	
	std::vector<std::map<std::string, std::string> > data = DB_win::query("SELECT LAST_INSERT_ID() AS id");

	if(data.size() > 0)
	{
		std::map<std::string, std::string>::iterator it = data[0].begin();
		for (int i = 0; it != data[0].end(); it++, i++) {  // выводим их
			result = utils::ft_atoi(it->second.c_str());
			break;
			//std::cout << i << ") Ключ " << it->first << ", значение " << it->second << std::endl;
		}
	}

	return result;
}

inline int DB_win::close()
{
	if (&this->mysql)
	{
		mysql_close(&this->mysql);
		return 1;
	}
	return 0;
}

inline std::vector<std::map<std::string, std::string> > DB_win::query(const char query[])
{
	std::vector<std::map<std::string, std::string>> result;

	if (mysql_query(&mysql, query) > 0) // ������. ���� ������ ���, �� ���������� ������
	{

		// ���� ���� ������, ...
		printf("%s", mysql_error(&mysql));  // ... ������ ��
		return result; // � �������� ������
	}


	MYSQL_RES *res; // ���������� �������������� �������
	MYSQL_FIELD *field;

	res = mysql_store_result(&mysql); // ����� ���������,
	int num_fields = mysql_num_fields(res); // ���������� �����
	int num_rows = mysql_num_rows(res); // � ���������� �����.

										//printf("nums %d = %d\r\n", num_fields, num_rows);

	std::vector<std::string> field_name;

	for (int i = 0; i < num_fields; i++) // ������� �������� �����
	{
		field = mysql_fetch_field_direct(res, i); // ��������� �������� �������� ����
		field_name.push_back(field->name);
		//printf("| %s |", field->name);
	}

	//printf("\n");



	MYSQL_ROW row; // ������ ����� ������� ������

	for (int i = 0; i < num_rows; i++) // ����� �������
	{
		std::map<std::string, std::string> line;

		row = mysql_fetch_row(res); // �������� ������

		for (int l = 0; l < num_fields; l++)
		{
			if (row[l])
			{
				line.insert(std::pair<std::string, std::string>(field_name[l], row[l]));
			}
			else {
				line.insert(std::pair<std::string, std::string>(field_name[l], ""));
			}


			//line[field_name[l]] = row[l];

			//printf("| %s |", row[l]); // ������� ����

		}
		result.push_back(line);


		//printf("\n");
	}

	//printf("Count records = %d\r\n", num_rows); // ����� ���������� � ���������� �������
	mysql_free_result(res); // ������� ����������

	return result;
}

#endif