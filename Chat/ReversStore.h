#include "framework/Singleton.h"
#include "Geo.h"

class Revers
{
public:
	int id;
	std::string ip;
	std::string uid;
	int port;
	int group_id;
	int max_clients;
	geo_t* geo;
	int is_removed;
	int is_online;

    sblist *con_list;
    pthread_mutex_t mutex;

    Revers()
    {
        pthread_mutex_init(&mutex, NULL);
        con_list = sblist_new(sizeof (connection*), 8);
    }

	int addConnection(connection* con)
    {
        pthread_mutex_lock(&mutex);
        sblist_add(con_list, &con);
        printf("ReversID: %d connected %s(%d), sockets_count: %d\r\n", this->id, con->key, con->socket, sblist_getsize(con_list));
        pthread_mutex_unlock(&mutex);
		return 1;
    }

    int con_delete(int socket)
    {
        pthread_mutex_lock(&mutex);

        connection *tmp = NULL;
        size_t i;
        for(i=0;i<sblist_getsize(con_list);) {
            tmp = *((struct connection **) sblist_get(con_list, i));
            if(tmp->socket == socket)
            {
                sblist_delete(con_list, i);
            } else
                i++;
        }

        if(sblist_getsize(con_list) == 0) this->is_online = 0;

        pthread_mutex_unlock(&mutex);
        return 1;
    }

    connection *con_get_last()
    {
        if(sblist_getsize(con_list) == 0) return 0;

        connection *con = NULL;

        pthread_mutex_lock(&mutex);
        con = *((struct connection **) sblist_get(con_list, sblist_getsize(con_list) - 1));
        pthread_mutex_unlock(&mutex);

        return con;
    }
};



class ReversStore
{
	DECLARE_SINGLETON(ReversStore)
public:
	ReversStore();
	void reload();
	int add(Revers* user);

    Revers* getById(int id);
	Revers* get(std::string uid, std::string ip, int port);
    Revers* getOnlineByFilter(int *index, int group_id, int curr_id);
    std::string sendCommand(int id, int command);

    std::string print();


private:
	std::string table_name = "revers";
	std::map< std::string, Revers* > revers_list;
	std::vector< Revers* > revers_ids;

	//std::map< int, std::vector< int > > revers_user_pull;

    pthread_mutex_t mutex;
};

#pragma once

inline ReversStore::ReversStore()
{
    pthread_mutex_init(&mutex, NULL);
    reload();
}

inline void ReversStore::reload()
{
    /*
    std::map< std::string, Revers* >::iterator it;
    for (it = revers_list.begin(); it != revers_list.end(); it++) {
        revers_list.erase(it->first);
        delete it->second;
    }
    */
    pthread_mutex_lock(&mutex);

    std::string sql = "SELECT * FROM `revers`";
    std::vector<std::map<std::string, std::string> > rows = Db::Instance()->query(sql.c_str());
    printf("ReversServer reload from MYSQL: rows %d\r\n", rows.size());
    for(int i = 0; i < rows.size(); i++)
    {
        std::string uid = rows[i]["uid"];
        Revers *revers = revers_list[uid];

        if(!revers)
        {
            revers = new Revers();
            revers->id = atoi(rows[i]["id"].c_str());
            revers->ip = rows[i]["ip"];
            revers->port = atoi(rows[i]["port"].c_str());
            revers->uid = rows[i]["uid"];
            revers->group_id = atoi(rows[i]["group_id"].c_str());
            revers->max_clients = atoi(rows[i]["max_clients"].c_str());
            revers->is_online = 0;

            revers_list[rows[i]["uid"]] = revers;
            revers_ids.push_back(revers);
        } else {
            revers->ip = rows[i]["ip"];
            revers->port = atoi(rows[i]["port"].c_str());
            revers->uid = rows[i]["uid"];
            revers->group_id = atoi(rows[i]["group_id"].c_str());
            revers->max_clients = atoi(rows[i]["max_clients"].c_str());
        }
    }

    pthread_mutex_unlock(&mutex);
}

inline int ReversStore::add(Revers* revers)
{

	if (!revers_list[revers->uid])
	{
        revers->geo = GeoIp::Instance()->geo(revers->ip);

		revers_list[revers->uid] = revers;


		std::string sql = "INSERT INTO `revers` (`ip`, `port`, `uid`, `group_id`, `max_clients`, `is_online`, `geo_code`, `city`, `country`, `latitude`, `longitude`)";
		sql += " VALUES ('" + revers->ip + "', " + utils::xitoa(revers->port) + ", '" + revers->uid + "', " + utils::xitoa(revers->group_id) + ", " + utils::xitoa(revers->max_clients) + ", '1', '" + revers->geo->code + "', '" + revers->geo->city + "', '" + revers->geo->country + "', '" + revers->geo->latitude + "', '" + revers->geo->longitude + "');";
		//printf("sql %s\r\n", sql.c_str());
		int insert_id = Db::Instance()->insert(sql.c_str());

        if(insert_id)
        {
            revers->id = insert_id;
        }

	}

	return 1;
}

inline Revers* ReversStore::getById(int id)
{
    Revers* revers = NULL;
    pthread_mutex_lock(&mutex);

    for(int i = 0; i < revers_ids.size(); i++)
    {
        if(revers_ids[i]->id == id)
        {
            revers = revers_ids[i];
            break;
        }
    }

    pthread_mutex_unlock(&mutex);
    return revers;
}

inline std::string ReversStore::sendCommand(int id, int command)
{
    Revers* revers = getById(id);
    if(revers)
    {
        connection *con = revers->con_get_last();
        if(!con)
        {
            return "revers not online";
        }

        char buffer[128];
        sprintf(buffer, "restart");
        int n = strlen(buffer);
        pack(1, buffer, n);
        int snd = send_to_tunel(con->socket, buffer, n);

        return "send command to socket: " + utils::xitoa(con->socket);
    }
    return "revers not found";
}

inline Revers* ReversStore::get(std::string uid, std::string ip, int port)
{
	Revers* revers = NULL;
    pthread_mutex_lock(&mutex);

	revers = revers_list[uid];
	if (!revers)
	{
		revers = new Revers();
		revers->ip = ip;
		revers->port = port;
		revers->uid = uid;
		revers->group_id = 2;
		revers->max_clients = 500;
		ReversStore::Instance()->add(revers);
		revers_list[uid] = revers;
        revers_ids.push_back(revers);
	}

    revers->is_online = 1;

    pthread_mutex_unlock(&mutex);
	return revers;
}

inline Revers* ReversStore::getOnlineByFilter(int *index, int group_id, int curr_id)
{
    Revers* revers = NULL;
    pthread_mutex_lock(&mutex);

    std::vector< Revers* > filtered_revers;
    for(int i = 0; i < revers_ids.size(); i++)
    {
        if(revers_ids[i]->is_online == 1 && revers_ids[i]->group_id == group_id)
        {
            //printf("i: %d\r\n", revers_ids[i]->id);
            filtered_revers.push_back(revers_ids[i]);
        }
    }

    printf("Revers pull by filter group_id: %d\r\n", filtered_revers.size());

    if(filtered_revers.size() == 1)
    {
        revers = filtered_revers[0];

        
        if(curr_id != 0 && revers->id == curr_id)
        {
            revers = NULL;
        }
    }

    if(filtered_revers.size() > 1)
    {
        (*index)++;
        if(*index >= filtered_revers.size())
        {
            *index = 0;
        }

        revers = filtered_revers[*index];

        if(revers->id == curr_id)
        {
            (*index)++;
            if(*index >= filtered_revers.size())
            {
                *index = 0;
            }

            revers = filtered_revers[*index];
        }

    }

    pthread_mutex_unlock(&mutex);

    //exit(0);
    return revers;
}



inline std::string ReversStore::print()
{
    std::string result = "<h4>Revers INFO</h4>";

    pthread_mutex_lock(&mutex);

    int n = 0;

    for(int i = 0; i < revers_ids.size(); i++)
    {
        Revers *revers = revers_ids[i];

        result += utils::xitoa(n, 10) + ") id: "+utils::xitoa(revers->id) + " ip: " + revers->ip + " Sockets_open:" + utils::xitoa(sblist_getsize(revers->con_list), 10) + " is_online: " + utils::xitoa(revers->is_online) + " group_id: " + utils::xitoa(revers->group_id) + " <a target='_blank' href='/proxy/command?id="+utils::xitoa(revers->id)+"&action=restart'>Restart</a>";
        result += "<br />";
        n++;

    }

    result += "<br />------------<br />";
    pthread_mutex_unlock(&mutex);

    return result;
}