#include "framework/Singleton.h"
#include "ManagerPorts.h"

#pragma once

inline UsersStore::UsersStore() {
    // run once singletone
    pthread_mutex_init(&mutex, NULL);
    

    

    /*
    User *user = new User;
    std::vector< User* > test;
    
    test.push_back(user);


    //test.erase(test.begin() + 1);

    int len = test.size();
    for(int i=0; i<test.size();i++)
    {
        User *tmp = test[i];
        
        printf("Test %d => %d\r\n", *user, *tmp);
    }

    exit(0);
    
    */
    reload();


    //addUser(1, "qwe", "qwe", 2, 1001);
    //addUser(2, "qwe", "qwe", 2, 2500);

    //addUser(3, "", "", 2, 1040);

    /*
    addUser(new User(1, "qwe", "qwe", 2), 1001);
    addUser(new User(2, "qwe", "qwe", 2), 1002);
    addUser(new User(3, "asd", "asd", 2), 1001);
    addUser(new User(4, "asd", "asd", 2), 1002);
    addUser(new User(5, "zxc", "zxc", 2), 1003);
    addUser(new User(6, "zxc", "zxc", 2), 1003);
    addUser(new User(7, "rty", "rty", 2), 1001);
    */
}

inline User* UsersStore::portHasAuth(int port)
{
    User *user = NULL;
    pthread_mutex_lock(&mutex);

    std::vector< User* > users_pull = revers_user_pull[port];

    size_t i;
    for(i=0;i<users_pull.size();i++) {
        User *tmp = users_pull[i];

        if (tmp->login.length() == 0 && tmp->password.length() == 0)
        {
            user = tmp;
            break;
        }
    }

    //printf("Port %d\r\n", port);
    //user = port_no_auth[port];
    pthread_mutex_unlock(&mutex);
    //printf("exit");
	return user;
}

inline int UsersStore::add(User *user)
{
    if(user->port <= 0) return 0;

    pthread_mutex_lock(&mutex);

    users.push_back(user);
    revers_user_pull[user->port].push_back(user);

    //if (user->login.length() == 0 && user->password.length() == 0)
    //{
    //    port_no_auth[user->port] = user;
    //}

    pthread_mutex_unlock(&mutex);
    ManagerPorts::Instance()->addPort(user->port, user->id);
    return 1;
}

inline int UsersStore::removeAll()
{
    pthread_mutex_lock(&mutex);
    for(int i = 0; i < users.size(); i++)
    {
        delete users[i];
    } 
    users.clear();
    revers_user_pull.clear();

    pthread_mutex_unlock(&mutex);
    return 1;
}

inline User* UsersStore::get(int id)
{

    User *result = 0;
    pthread_mutex_lock(&mutex);

    size_t i;
    for(i=0;i<users.size();i++) {
        
        if(users[i]->id == id)
        {
            result = users[i];
            break;
        }
    }
    pthread_mutex_unlock(&mutex);
	
	//printf("Check code: %d\r\n", result);
	return result;
}

inline int UsersStore::remove(int id)
{
    User *user = 0;

    pthread_mutex_lock(&mutex);
    size_t i;
    for(i=0;i<users.size();i++) {
        User *tmp = users[i];
        if(tmp->id == id)
        {
            ManagerPorts::Instance()->removePort(users[i]->port, users[i]->id);
            user = tmp;
            users.erase(users.begin() + i);
            break;
        }
    }
    
    if(user)
    {
        std::vector< User* > users_pull = revers_user_pull[user->port];

        size_t i;
        for(i=0;i<users_pull.size();i++) {
            User *tmp = users_pull[i];
            if(tmp->id == id)
            {
                users_pull.erase(users_pull.begin() + i);
                break;
            }
        }

        printf("removed user %d\r\n", user->id);
        delete user;
        return 1;
    }

    pthread_mutex_unlock(&mutex);
    
    return 0;
}

inline User* UsersStore::getUser(int revers_port, std::string login, std::string password)
{

    User *result = NULL;
    
    pthread_mutex_lock(&mutex);
	std::vector< User* > users = revers_user_pull[revers_port];
    pthread_mutex_unlock(&mutex);

	for (int i = 0; i < users.size(); i++)
	{
		User *user = users[i];
		//printf("user %s == %s\r\n", user->login.c_str(), login.c_str());
		if(login.compare(user->login) == 0)
		{
			//printf("Success user %s == %s\r\n", user->login.c_str(), login.c_str());
			result = user;
			break;
		}
	}
	printf("Check login %s on revers %d User_addr #%d users on port %d\r\n", login.c_str(), revers_port, result, users.size());
    
	
	//printf("Check code: %d\r\n", result);
	return result;
}

inline void UsersStore::reload()
{

    removeAll();
    

    std::string sql = "SELECT * FROM `revers_users`";
    std::vector<std::map<std::string, std::string> > rows = Db::Instance()->query(sql.c_str());
    printf("Users reload from MYSQL: rows %d\r\n", rows.size());

    for(int i = 0; i < rows.size(); i++)
    {

        User *user = new User;
        user->id = atoi(rows[i]["id"].c_str());
        user->login = rows[i]["login"];
        user->password = rows[i]["password"];
        user->group_id = atoi(rows[i]["group_id"].c_str());
        user->type = atoi(rows[i]["type"].c_str());
        user->expire = atoi(rows[i]["expire"].c_str());
        user->revers_id = atoi(rows[i]["revers_id"].c_str());
        user->revers_index = atoi(rows[i]["revers_index"].c_str());
        user->port = atoi(rows[i]["port"].c_str());

        this->add(user);

        //user_ids.push_back(user);
        //user_list[user->id] = user;
        //revers_user_pull[port].push_back(user->id);

        

        printf("loaded user %d %s %s %d %d\r\n", user->id, user->login.c_str(), user->password.c_str(), user->group_id, user->port);

    }


}

inline std::string UsersStore::updateUser(int id)
{
    std::string json;

    User* userSql = User::model()->_getById(id);
    User *user = this->get(id);

    if(userSql)
    {
        if(!user)
        {
            printf("set revers\r\n");

            json = "{ success: 'set revers'}";
            user = userSql;

            user->setNextRevers();

            this->add(user);

        } else {
            user->_clone(userSql);
            printf("Clone revers_id %d => %d\r\n", user->revers_id, userSql->revers_id);
            //exit(0);

            json = "{ success: 'user updated data from sql'}";
            delete userSql;
        }

        //printf("Model find login: %s\r\n", user->login.c_str());
        //exit(0);
    } else {
        printf("delete user %d\r\n", id);

        json = "{ success: 'delete user'}";
        this->remove(id);
    }

    if(!user) 
    {
        //json = "{ error: 'user not found v2' }";
        return json;
    }

    Revers *revers = ReversStore::Instance()->getById(user->revers_id);

    if(!revers)
    {
        json = "{ success: true, id: " + utils::xitoa(id) +", ip: false, id: "+utils::xitoa(user->revers_id)+" }";
        return json;
    }

    json = "{ success: true, id: " + utils::xitoa(id) +", ip: '"+revers->ip+"', id: "+utils::xitoa(user->revers_id)+" }";
    return json;
}



inline int UsersStore::disconnectUserByReversId(int revers_id)
{
    pthread_mutex_lock(&mutex);
    for(int i = 0; i < users.size(); i++) {
        User *user = users[i];
        if(user->revers_id == revers_id)
        {
            
            user->closeAllConnection();

            /*
            for(int j=0; j < sblist_getsize(user->con_list); j++) {
                connection* con = *((struct connection **) sblist_get(user->con_list, j));
                printf("Revers Shutdown close client %d\r\n", con->socket);
                shutdown(con->socket, SHUT_RDWR);
            }
            */
        }
    }
    pthread_mutex_unlock(&mutex);

    return 1;
}


inline std::string UsersStore::print()
{
    std::string result = "<h4>Users info</h4>";

    pthread_mutex_lock(&mutex);

    int n = 0;

    result += "User count: "+utils::xitoa(users.size())+"<br />";
    for(int i = 0; i < users.size(); i++)
    {
        User *user = users[i];

        result += utils::xitoa(n, 10) + ") id: " + utils::xitoa(user->id) + " ip: " + user->revers_ip + " sockets:" + utils::xitoa(sblist_getsize(user->con_list), 10) + " revers_id: " + utils::xitoa(user->revers_id) + " port: " + utils::xitoa(user->port) + "<br />";

        if(sblist_getsize(user->con_list) > 0)
        {
            result += ConnectionManager::Instance()->print(user->id);
        }


        n++;

    }

    result += "<br />------------<br />";
    pthread_mutex_unlock(&mutex);

    return result;
}
