// Chat.cpp : Defines the entry point for the console application.
//


#include "framework/preIncl.h"

#include "framework/WS32_helper.h"
#include "framework/n18.h"
#include "framework/FileTemplate.h"
#include "framework/xPath.h"
#include "framework/xThread.h"
#include "framework/xLinux.h"

#include "InfoProcSelf.h"
#include "sblist.h"
#include "ErrorController.h"
#include "packer.h"
#include "Db.h"
#include "Geo.h"
#include "Object.h"
#include "ConnectionManager.h"
#include "framework/model.h"
#include "ReversStore.h"


class User: public Model
{
    MODEL(User);
public:
    int id;
    std::string login;
    std::string password;
    int group_id;
    int revers_id;
    int revers_index;
    int type;
    int expire;
    int port;

    std::string revers_ip;

    sblist *con_list;
    pthread_mutex_t mutex;

    User()
    {
        _set("id", &this->id);
        _set("login", &this->login);
        _set("password", &this->password);
        _set("group_id", &this->group_id);
        _set("revers_id", &this->revers_id);
        _set("revers_index", &this->revers_index);
        _set("type", &this->type);
        _set("expire", &this->expire);
        _set("port", &this->port);
        _setTableName("revers_users");
   
        this->revers_ip = "";

        pthread_mutex_init(&mutex, NULL);
        con_list = sblist_new(sizeof (connection*), 8);
        if(!con_list)
        {
            printf("Error create con_list\r\n"); exit(0);
        }
    }

    int setNextRevers()
    {
        if(this->type != 1 && this->revers_id != 0) return 0;

        int index = this->revers_index;

        int revers_id = this->revers_id;
        int revers_index = this->revers_index;

        Revers *revers = ReversStore::Instance()->getOnlineByFilter(&index, this->group_id, revers_id);

        if(revers)
        {

            this->revers_id = revers->id;
            this->revers_index = index;

            //user->save();
            this->_save();

            closeAllConnection();
            return this->revers_id;
            //return "Result: Ok Id: " + utils::xitoa(revers_id) + " => "+ utils::xitoa(user->revers_id) + " Index: " + utils::xitoa(revers_index) + " => "  + utils::xitoa(user->revers_index);
        } else {
            return revers_id * -1;
            //return "Result: Not found proxy online: revers_id: " + utils::xitoa(revers_id);
        }

    }

    int closeAllConnection()
    {
        pthread_mutex_lock(&mutex);
        printf("Close user pull %d\r\n", sblist_getsize(con_list));
        for(int j=0; j < sblist_getsize(con_list); j++) {
            connection* con = *((struct connection **) sblist_get(con_list, j));
            printf("[close_all] Close %d\r\n", con->socket);
            //con->close_s();
            shutdown(con->socket, SHUT_RDWR);
            //closesocket(con->socket);
        }

        pthread_mutex_unlock(&mutex);

        return 1;
    }

    int addConnection(connection* con)
    {
        pthread_mutex_lock(&mutex);
        sblist_add(con_list, &con);
        printf("UserID: %d connected %s(%d), sockets_count: %d\r\n", this->id, con->key, con->socket, sblist_getsize(con_list));
        pthread_mutex_unlock(&mutex);
		return 1;
    }

    int con_delete(int socket)
    {
        pthread_mutex_lock(&mutex);

        connection *tmp = NULL;
        size_t i;
        for(i=0;i<sblist_getsize(con_list);) {
            tmp = *((struct connection **) sblist_get(con_list, i));
            if(tmp->socket == socket)
            {
                sblist_delete(con_list, i);
            } else
                i++;
        }
        printf("Delete con %d pull size %d\r\n", socket, sblist_getsize(con_list));
        pthread_mutex_unlock(&mutex);
        return 1;
    }

    connection *con_get_last()
    {
        if(sblist_getsize(con_list) == 0) return 0;

        connection *con = NULL;

        pthread_mutex_lock(&mutex);
        con = *((struct connection **) sblist_get(con_list, sblist_getsize(con_list) - 1));
        pthread_mutex_unlock(&mutex);

        return con;
    }
};

class UsersStore
{
    DECLARE_SINGLETON(UsersStore)
public:
    UsersStore();
    void reload();
    
    User* getUser(int revers_port, std::string login, std::string password);
    User* portHasAuth(int port);

    int disconnectUserByReversId(int revers_id);


    std::string updateUser(int id);

    int removeAll();
    int add(User *user);
    User* get(int id);
    int remove(int id);
    
    std::string print();

private:
    std::vector< User* > users;
    std::map< int, std::vector< User* > > revers_user_pull;
    //std::map< int, User* > port_no_auth;

    pthread_mutex_t mutex;
};

#include "UsersStore.h"


void initalize()
{
	GeoIp::Instance();
	TrafficAnalize::Instance(); //start logging traffic
	Db::Instance()->Check(); //connection to sql server
	ReversStore::Instance(); //upload revers from sql
	UsersStore::Instance(); //upload users from sql

}


static void* thred_test_instance(void *data) {
    User *user = (User*)data;

    //printf("%d inside %d kb thread\r\n", user->id, getValue() / 1024);

    while(true) Sleep(3000);
    return 0;
}

void thread_test()
{
    for(int i = 0; i < 32000; i++)
    {
        User *user = new User();
        user->id = i;
        HANDLE pt = createThread((LPTHREAD_START_ROUTINE)thred_test_instance, (void*)user);
        printf("Created %d thread\r\n", i);
    }
    
    while(true) Sleep(3000);
    exit(0);
}


void loop_creator()
{
    ManagerPorts::Instance()->looper();
}

void start()
{

	initalize();
	loop_creator();


	xprintf("service is started\r\n");
	while (true)
	{
		utils::xsleep(3000);
	}
}


#ifdef WIN32
int _tmain(int argc, _TCHAR* argv[])
{
	
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		printf("WSAStartup function failed with error: %d\n", iResult);
		system("pause");
		return 1;
	}


	start();

	return 0;
}
#else

void posix_death_signal(int signum)
{
    ErrorLog::Instance()->save("Server Error handler\r\nPlease Restart\r\n");
    printf("Server Error handler\r\nPlease Restart\r\n");
    signal(signum, SIG_DFL);
    exit(42);
}

int main(int argc, char* argv[])
{
    printf("Linux platform\r\n");

    signal(SIGSEGV, posix_death_signal);
    start();
}

#endif
