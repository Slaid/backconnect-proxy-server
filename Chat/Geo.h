#include "framework/Singleton.h"

#ifdef WIN32

#else
#include "geo/GeoLite2PP.hpp"
#endif

class geo_t
{
public:
	std::string city;
	std::string country;
	std::string latitude;
	std::string longitude;
	std::string code;
};

class GeoIp
{
	DECLARE_SINGLETON(GeoIp)
public:
	GeoIp();
	geo_t* geo(std::string ip);

#ifndef WIN32
	GeoLite2PP::DB* geo_db;
#endif
};

#pragma once

inline GeoIp::GeoIp() {

#ifndef WIN32
	std::string database_filename = "Chat/geo/databases/GeoLite2-City.mmdb";
	this->geo_db = new GeoLite2PP::DB(database_filename);
#endif

	std::string ip = "213.33.214.182";
	geo_t *geo = this->geo(ip);

	printf("[geo_test] IP %s Code %s City %s Country %s Latitude %s:%s\r\n", ip.c_str(), geo->code.c_str(), geo->city.c_str(), geo->country.c_str(), geo->latitude.c_str(), geo->longitude.c_str());
}

inline geo_t* GeoIp::geo(std::string ip)
{
	geo_t* result = new geo_t;

#ifndef WIN32
	result->city = this->geo_db->get_field(ip, "en", GeoLite2PP::VCStr{ "city", "names" });
	result->country = this->geo_db->get_field(ip, "en", GeoLite2PP::VCStr{ "country", "names" });
	result->latitude = this->geo_db->get_field(ip, "", GeoLite2PP::VCStr{ "location" , "latitude" });
	result->longitude = this->geo_db->get_field(ip, "", GeoLite2PP::VCStr{ "location" , "longitude" });
	result->code = this->geo_db->get_field(ip, "", GeoLite2PP::VCStr{ "country", "iso_code" });
#endif


	//result = this->geo_db->lookup( ip );
	return result;
}

/*
static void geo_test()
{
std::string database_filename = "Chat/geo/databases/GeoLite2-City.mmdb";
GeoLite2PP::DB db(database_filename);
std::string json = db.lookup("213.33.214.182");
std::cout << json << std::endl;
//exit(0);
}

*/