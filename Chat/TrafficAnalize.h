#include "framework/Singleton.h"
#include "framework/xThread.h"
#include "framework/xLinux.h"
#include "framework/xmutex.h"
#include "Object.h"
#include "Db.h"
#include "DB_helper.h"

class TrafficPart
{
public:
    int upload;
    int download;
    std::string ip;
    int port;
    std::string key;
};

struct target
{
    char line[1024];
};

class TrafficAnalize
{
	DECLARE_SINGLETON(TrafficAnalize)
public:
	TrafficAnalize();
	void run();
	void setBenchmark(int bytes);
	int calculator();

    void grabberUri(connection* con, char* buffer, int n);
    void grabberResponse(connection* con, char* buffer, int n);

	void appendTrafficIP(connection* con);
	void logTarget(connection* con, char* buffer, int len);
	void saveInfoAbout(connection *con);

	std::string getTarget();
	void saveTrafficToSql();

	int total;
	int totalPerSecond;

	std::vector< char* > target_log;

    sblist *targets;

	int requests_cnt;
	int requests_per_second;
	int requests_cnt_total;

	//std::map< std::string, TrafficPart* > traffic_log;


    sblist *traffic_log;

    pthread_mutex_t mutex;

	/*
    TrafficAnalize()
    {
        requests_cnt = 0;
    }
    */
};

#pragma once


static void targetsThreadCounter()
{
	while (TRUE)
	{
		TrafficAnalize::Instance()->calculator();
		utils::xsleep(1000);
	}
}


static void saveResolution()
{
	while (TRUE)
	{

		utils::xsleep(1000 * 15);
        TrafficAnalize::Instance()->saveTrafficToSql();
	}
}

inline TrafficAnalize::TrafficAnalize()
{
    requests_cnt = 0;
    requests_cnt_total = 0;
    requests_per_second = 0;

    pthread_mutex_init(&mutex, NULL);

    targets = sblist_new(sizeof (target*), 8);
    traffic_log = sblist_new(sizeof (TrafficPart*), 8);

	Thread t1;
	t1.Start((LPTHREAD_START_ROUTINE)saveResolution, (void*)0);

	Thread t2;
	t2.Start((LPTHREAD_START_ROUTINE)targetsThreadCounter, (void*)0);
}

inline int TrafficAnalize::calculator()
{
	requests_per_second = requests_cnt;
	requests_cnt = 0;

	return 0;
}

struct sum_part
{
    int long long upload;
    int long long download;
    int port;
    std::string ip;
};

inline void TrafficAnalize::saveTrafficToSql()
{

    int time = utils::timeSinceEpochMillisec() / 1000 / 300; //resolution 1h
	printf("Save traffic to sql time %d rows %d\r\n", time, sblist_getsize(traffic_log));

	std::map< std::string, sum_part* > to_insert;

    pthread_mutex_lock(&mutex);
    for(int i = 0; i < sblist_getsize(traffic_log); i++)
    {
        TrafficPart *part = *((TrafficPart **) sblist_get(traffic_log, i));

        sum_part *part_in = to_insert[part->key];
        if(!part_in)
        {
            part_in = new sum_part;
            part_in->download = 0;
            part_in->upload = 0;
            part_in->port = part->port;
            part_in->ip = part->ip;

            to_insert[part->key] = part_in;
        }

        part_in->download += part->download;
        part_in->upload += part->upload;

        delete part;
    }

    sblist_free_items(traffic_log);
    pthread_mutex_unlock(&mutex);

    printf("Rows to save %d\r\n", to_insert.size());


    std::map< std::string, sum_part* >::iterator it;
    for (it = to_insert.begin(); it != to_insert.end(); ++it)
    {
        sum_part *part_in = it->second;
        //printf("%d = %d\r\n", part_in->upload, part_in->download);
        if(part_in && (part_in->upload > 50 || part_in->download > 50))
        {
            std::string sql = "INSERT INTO `user_traffic` (`ip`, `port`, `traffic`, `traffic_up`, `timestamp_hours`)";
            sql += " VALUES('" + part_in->ip + "', " + utils::xitoa(part_in->port) + ", " + utils::xitoa(part_in->upload) + ", " + utils::xitoa(part_in->download) + ", "+utils::xitoa(time)+")";
            sql += " ON DUPLICATE KEY UPDATE `ip` = '" + part_in->ip + "', `port` = " + utils::xitoa(part_in->port) + ", `traffic` = `traffic` + " + utils::xitoa(part_in->upload) + ", `traffic_up` = `traffic_up` + " + utils::xitoa(part_in->download) + ", `timestamp_hours` = "+utils::xitoa(time)+";";
            //printf("Sql: %s\r\n", sql.c_str());

            delete part_in;

            //printf("Sql %s\r\n", sql.c_str());
            Db::Instance()->query(sql.c_str());
        }

    }
    to_insert.clear();
    //exit(0);

}



inline void TrafficAnalize::setBenchmark(int bytes)
{
	this->total += bytes;
	printf("Total %d\r\n", this->total);
}

inline void TrafficAnalize::appendTrafficIP(connection *con)
{
	if(con->countTrafficRequest > 50 && con->countTrafficResponse > 50)
	{
		//printf("SQL append %d/%d bytes\r\n", con->countTrafficRequest, con->countTrafficResponse, con->socket_ip);

        TrafficPart *traffic_part = new TrafficPart;
        traffic_part->upload = con->countTrafficRequest;
        traffic_part->download = con->countTrafficResponse;
        traffic_part->ip = con->socket_ip;
        traffic_part->port = con->socket_port;
        traffic_part->key = con->key;

        pthread_mutex_lock(&mutex);
        sblist_add(traffic_log, &traffic_part);
        pthread_mutex_unlock(&mutex);

	}

}



inline void TrafficAnalize::logTarget(connection* con, char *buf, int n)
{
	std::string error;
	char namebuf[256];
	unsigned short port = 1;

	while (1)
	{
		if (n < 5)
		{
			error = "Error socket protocol min 5 bytes\r\n";
			break;
		}
		if (buf[0] != 5) 
		{
			error = "Error socket protocol non 5 version\r\n";
			break;
		}
		if (buf[1] != 1) 
		{
			error = "We support only CONNECT method\r\n";
			break;
		}

		if (buf[2] != 0) 
		{
			error = "Malformed packet\r\n";
			break;
		}

		int af = AF_INET;
		size_t minlen = 4 + 4 + 2, l;
		
		//struct addrinfo* remote;

		switch (buf[3]) {
		case 4: /* ipv6 */
			af = AF_INET6;
			minlen = 4 + 2 + 16;
			/* fall through */
		case 1: /* ipv4 */
			if (n < minlen) 
			{
				error = "Ip not min length for target\r\n";
				break;
			}
			if (namebuf != inet_ntop(af, buf + 4, namebuf, sizeof namebuf))
			{
				error = "Malformed or too long addr\r\n";
				break;
			}
			break;
		case 3: /* dns name */
			l = buf[4];
			minlen = 4 + 2 + l + 1;
			if (n < 4 + 2 + l + 1) 
			{
				error = "Dns name erro small length\r\n";
				break;
			}
			memcpy(namebuf, buf + 4 + 1, l);
			namebuf[l] = 0;
			break;
		default:
			error = "Target not supported\r\n";
			break;
		}
		
		con->remote_port = port = (int)buf[minlen - 2] * 256 + (unsigned char)buf[minlen - 1];
		break;
	}
	
	//std::string line;

    //char *buff_str = (char*)malloc(256);

    struct target *targ = (struct target*) malloc(sizeof (struct target));
    //targ->line =

    int timestamp = utils::timeSinceEpochMillisec() / 1000;

	if (error.empty())  
	{
	    sprintf(con->hostname, "%s", namebuf);
	    //con->hostname = namebuf;

	    sprintf(targ->line, "%d: %s => %s:%d\r\n", timestamp, con->key, namebuf, port);

		//line = std::string(con->key) + " => " + std::string(namebuf) + ":" + utils::xitoa(port) + "\r\n";
	}
	else {
	    //con->error(error.c_str());
        sprintf(targ->line, "%d: %s => Err: %s\r\n", timestamp, con->key, error.c_str());
		//line = std::string(con->key) + " => Err: error\r\n";
	}



	//line = utils::xitoa(ms) + ": " + line;

	LizzzMutex::Instance("analize")->lock();



	if (requests_cnt_total > 2100000000)
	{
		requests_cnt_total = 0;
	}
    requests_cnt++;
    requests_cnt_total++;

    sblist_add(targets, &targ);

	if(sblist_getsize(targets) > 40)
    {
        target *tmp = *((struct target **) sblist_get(targets, 0));
        free(tmp);
        sblist_delete(targets, 0);
    }


	LizzzMutex::Instance("analize")->unlock();


}

inline std::string TrafficAnalize::getTarget()
{
	std::string result;
	LizzzMutex::Instance("analize")->lock();

	target *tmp = NULL;
	size_t i;
    for(i=0;i<sblist_getsize(targets);i++) {
        tmp = *((struct target **) sblist_get(targets, i));

        result += std::string(tmp->line) + "<br />\r\n";
    }

    /*
	for (int i = 0; i < target_log.size(); i++)
	{
		result += std::string(target_log[i]) + "<br />\r\n";

	}*/
	LizzzMutex::Instance("analize")->unlock();
	return result;
}

static bool checkEndHostName(char symb) {
    char *regexCustomDomain = "1234567890-qwertyuiopasdfghjklzxcvbnm.";

    for (int k = 0; k < strlen(regexCustomDomain); k++) {
        //printf("finded %c %d = %d\r\n", regexCustomDomain[k], regexCustomDomain[k], symb);
        if (regexCustomDomain[k] == symb) {
            return true;
        }
    }
    return false;
}

static int getSslInfoNew(char* request, int lenRecv, char(&host)[255]) {

    char hostReverce[255] = { 0 };
    int n = 0;
    int i = 0;


    printf("recv %d\r\n", lenRecv);

    //std::string reverseString;
    n = 0;
    for (i = 0; i < lenRecv; i++) {
        //printf("%c = %d\r\n", request[i], request[i]);
        if (request[i] == 0 && request[i + 1] == 23 && request[i + 2] == 0 && request[i + 3] == 0) {


            //-----------------
            for (int j = 1; j < 100; j++) {


                if (checkEndHostName(request[i - j])) { // >= 45

                    //printf("%c = %d res %d\r\n", request[i - j], request[i - j], SslHelper::checkEndHostName(request[i - j]));
                    hostReverce[n] = request[i - j];

                    n++;
                }
                else {

                    int j = 0;
                    for (int i = n - 1; i >= 0; i--) {
                        //printf("%d %d %c\r\n", n - i, i, hostReverce[i]);
                        host[j] = hostReverce[i];
                        j++;

                        //client->host[n - l - 1] = hostReverce[l];
                    }
                    host[j] = '\0';

                    return 1;

                }
            }
            return 0;
        }
    }
    //printf("\r\n\r\n");
    return 0;
}

inline void TrafficAnalize::grabberUri(connection* con, char* buffer, int n) {
    //printf("Buff %d %s\r\n", n, buffer);
    std::string req = "";
    std::string line = "";
    req.append(buffer, n);
    int pos = req.find("\r\n");
    if (pos != std::string::npos) {
        line = req.substr(0, pos);
    }
    //con->uri = line.c_str();

    //printf("parse ssl %d %d\r\n", n, con->socket_port);
    if (con->remote_port == 443)
    {
        /*
        char host[255];
        getSslInfoNew(buffer, n, host);
        if(strlen(host) > 0)
        {
            con->resolve = host;
        }
         */
        //con->resolve = "ssl site";
    } else {
        int pos_host = req.find("Host: ");
        if(pos_host == std::string::npos)
        {
            pos_host = req.find("host: ");
        }
        if(pos_host == std::string::npos)
        {
            //con->resolve = "";
        } else {
            req = req.substr(pos_host + 6, req.length() - pos_host);

            int pos_host_end = req.find("\r\n");

            std::string host = req.substr(0, pos_host_end);
            //con->resolve = host;
        }

    }
}

inline void TrafficAnalize::grabberResponse(connection* con, char* buffer, int n)
{
    //printf("Buff %d %s\r\n", n, buffer);
    /*
    std::string req = "";
    std::string line = "";
    req.append(buffer,n);
    int pos = req.find("\r\n");
    if(pos != std::string::npos)
    {
        line = req.substr(0, pos);
    }
    con->response = line;
     */
}



inline void TrafficAnalize::saveInfoAbout(connection *con)
{
    appendTrafficIP(con);

    return; //skip save log
    
    if(con->is_bot) return ;

    if(con->socket_port == 5111) return ;

    con->time_end = utils::timeSinceEpochMillisec();
    //if(!con->user) return ;

    int user_id = 0;//con->user->id;
    //int user_id = con->user->id;
    //if(con->progress_code < 0)
    //    con->progress_code = con->progress_code * -1 + 1000;

    //std::string errorString = "";

    /*
    if(con->errors.size() > 0)
    {
        for(int i=0; i < con->errors.size(); i++)
        {
            errorString += con->errors[i] + "|";
        }
        errorString = errorString.substr(0, errorString.length() - 1);
    }
     */



    std::string sql = "INSERT INTO `traffic_log` (`user_id`, `revers_id`, `hostname`, `resolve`, `ip`, `port`, `latency`, `latency_revers`, `latency_client`, `byte_transfer`,`byte_transfer_request`, `time_start`, `time_end`, `progress_code`, `error_code`, `uri`, `response`, `error`)";
    sql += " VALUES (";
    sql += utils::xitoa(user_id) + ", ";
    sql += utils::xitoa(con->revers_id) + ", '";
    sql += std::string(con->hostname) + "', '";
    sql += "resolve', '";
    sql += std::string(con->socket_ip) + "', ";
    sql += utils::xitoa(con->remote_port) + ", ";
    sql += utils::xitoa(con->latency) + ", ";
    sql += utils::xitoa(con->latency_revers) + ", ";
    sql += utils::xitoa(con->latency_client) + ", ";
    sql += utils::xitoa(con->countTrafficResponse)+", ";
    sql += utils::xitoa(con->countTrafficRequest)+", ";
    sql += utils::xitoa(con->time_start / 1000)+", ";
    sql += utils::xitoa(con->time_end / 1000)+", ";
    sql += utils::xitoa(con->progress_code)+", ";
    sql += utils::xitoa(con->reciver_error_code)+", '";
    sql += "uri', '";
    sql += "response', '";
    sql += "error');";

    //LizzzMutex::Instance("insert_traffic")->lock();
    Db::Instance()->query(sql.c_str());
    //LizzzMutex::Instance("insert_traffic")->unlock();
}