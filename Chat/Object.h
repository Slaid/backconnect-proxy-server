#pragma once

struct PSK
{
    char* buffer;
    int len;
};

class StackList
{
public:
    int inc;
    int next_num;
    std::vector< PSK* > stack_vector;
    pthread_mutex_t mutex;

    StackList()
    {
        next_num = 0;
        inc = 0;
        pthread_mutex_init(&mutex, NULL);
    }

    void add(char *buffer, int n)
    {
        pthread_mutex_lock(&mutex);
        struct PSK* psk = (struct PSK*)malloc(sizeof(struct PSK));

        psk->buffer = (char*)malloc(sizeof(char) * n);
        memcpy(psk->buffer, buffer, n);
        psk->len = n;

        
        stack_vector.push_back(psk);
        inc++;
        pthread_mutex_unlock(&mutex);

    }

    struct PSK* get()
    {
        struct PSK* psk = NULL;
        pthread_mutex_lock(&this->mutex);
        if (this->next_num < this->inc)
        {
            psk = stack_vector[this->next_num];
            this->next_num++;
        }
        pthread_mutex_unlock(&this->mutex);
        return psk;

    }

    void clearStack()
    {
        //printf("Clear stack %d elem of %d\r\n", this->inc - next_num, this->inc);


        pthread_mutex_lock(&mutex);
        for(int i = next_num; i < inc; i++)
        {
            struct PSK* psk = stack_vector[i];

            free(psk->buffer);
            free(psk);
        }
        pthread_mutex_unlock(&mutex);
    }
};
/*
class StackList
{
public:
    sblist* stack_psk;
    int next_num;
    pthread_mutex_t mutex;

    StackList()
    {
        this->next_num = 0;
        this->stack_psk = sblist_new(sizeof(PSK*), 8);
        pthread_mutex_init(&this->mutex, NULL);
    }

    void add(char* buffer, int n)
    {
        struct PSK* psk = (struct PSK*)malloc(sizeof(struct PSK));

        psk->buffer = (char*)malloc(sizeof(char) * n);
        memcpy(psk->buffer, buffer, n);
        psk->len = n;

        pthread_mutex_lock(&this->mutex);
        sblist_add(this->stack_psk, &psk);
        pthread_mutex_unlock(&this->mutex);

    }

    struct PSK* get()
    {

        struct PSK* psk = NULL;
        pthread_mutex_lock(&this->mutex);
        if (this->next_num < sblist_getsize(this->stack_psk))
        {
            psk = *((struct PSK**)sblist_get(this->stack_psk, this->next_num));
            this->next_num++;
        }
        pthread_mutex_unlock(&this->mutex);
        return psk;

    }

    void clearStack()
    {
        pthread_mutex_lock(&this->mutex);
        if (this->next_num < sblist_getsize(this->stack_psk))
        {

            //printf("Clear stack %d elem of %d\r\n", sblist_getsize(stack_psk) - next_num, sblist_getsize(stack_psk));
            for (int i = this->next_num; i < sblist_getsize(this->stack_psk); i++)
            {
                struct PSK* psk = *((struct PSK**)sblist_get(this->stack_psk, i));
                free(psk->buffer);
                free(psk);

            }

            sblist_free_items(this->stack_psk);

        }
        pthread_mutex_unlock(&this->mutex);

    }
};
*/

class connection
{

public:
	int index;

	SOCKET socket;

	char *key;
    char *socket_ip;
    char *error;
    char *first_request;
    char *login;
    char *info;
    char *hostname;

    //std::string socket_ip;
	int socket_port;

    //connection *rem;
    int revers_id;

	//std::string key;

    StackList *stack;

    //int is_pack;
	//int is_auth;
	int isClosed;
	int isTrans;

	int status;
	int state; //for socketAuth state
	int is_bot;
	//int max_threads;
    unsigned long long int countTrafficRequest;
    unsigned long long int countTrafficResponse;



	//log request_field
    uint64_t time_start;
    uint64_t time_end;

	int byte_transfer;
    int byte_transfer_request;

    int latency;
    int latency_revers;
    int latency_client;

    int is_init;

    int progress_code;
    int user_id;

    //std::string hostname;
    int remote_port;

    //char uri[1024];
    //std::string uri;
    //std::string response;
    //std::string resolve;

    //std::vector<std::string> errors;

    volatile int last_active;

    int reciver_error_code;

    int is_transfer;
    int stage;

    //HANDLE ptr;

    struct sockaddr_in addr;
    
    int port;
    HANDLE ptr;
    int is_close;
    volatile int done;

    connection()
    {
        this->socket = 0;
        this->socket_ip = (char*)malloc(21*sizeof(char));
        this->key = (char*)malloc(128*sizeof(char));
        this->error = (char*)malloc(512*sizeof(char));
        this->first_request = (char*)malloc(1040*sizeof(char));
        this->login = (char*)malloc(128*sizeof(char));
        this->info = (char*)malloc(128*sizeof(char));
        this->hostname = (char*)malloc(256*sizeof(char));
        this->user_id = 0;
        this->revers_id = 0;
        this->stage = 0;
        this->is_close = 0;
        this->countTrafficRequest = 0;
        this->countTrafficResponse = 0;
        this->time_start = utils::timeSinceEpochMillisec();
        this->last_active = this->time_start / 1000;
        this->latency = 0;
        this->latency_revers = 0;
        this->latency_client = 0;
        this->progress_code = 0;
        this->reciver_error_code = 0;
        this->is_transfer = 0;
        this->done = 0;
        this->stack = new StackList();
        this->ptr = 0;
        this->port = 0;

        memset(this->socket_ip, 0, 21);
        memset(this->key, 0, 128);
        memset(this->error, 0, 512);
        memset(this->first_request, 0, 1040);
        memset(this->login, 0, 128);
        memset(this->hostname, 0, 256);
        memset(this->info, 0, 128);
    }

    
    void close_s()
    {
        if (this->is_close == 0)
        {
            this->is_close = 1;
            shutdown(this->socket, 2);
            close(this->socket);
        }
    }

    void clear()
    {
        this->stack->clearStack();
        delete this->stack;
        free(this->socket_ip);
        free(this->key);
        free(this->error);
        free(this->first_request);
        free(this->login);
        free(this->info);
        free(this->hostname);
        //free(con);
    }
};

void init_connection(connection *con)
{
    

}

void free_connection(connection *con)
{
    


    /*

    memset(con->socket_ip, 0, sizeof con->socket_ip);
    memset(con->key, 0, sizeof con->key);
    memset(con->error, 0, sizeof con->error);

    memset(con->first_request, 0, sizeof con->first_request);
    memset(con->login, 0, sizeof con->login);
    memset(con->hostname, 0, sizeof con->hostname);
     */
}

struct ServerObject
{

    int fd;
    int port;
    int count_users;
    int is_created;
};

class PackData
{
public:
    int len;
    char* buffer;
    //char buffer[1040];
    //int socket_client;


    PackData(char* buf, int len)
    {
        this->buffer = (char*)malloc(len + 1);
        memcpy(this->buffer, buf, len);
        this->len = len;
    }


};
