﻿#ifdef LINUX
#include "framework/Singleton.h"

//sql 8.0
//#include <boost/scoped_ptr.hpp>
//#include "SQLAPI.h"
//#include <mysql/jdbc.h>

//sql 5.7
#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

class DB_linux
{
	DECLARE_SINGLETON(DB_linux);
public:
	int connect(std::string host, std::string user, std::string pass, std::string database, int port = 3306);
	std::vector<std::map<std::string, std::string> > query(const char query[]);
	int insert(const char query[]);

	int close();

private:
	sql::Driver* driver;
	sql::Connection* con;
};

#pragma once


inline int DB_linux::connect(std::string host, std::string user, std::string pass, std::string database, int port)
{
	driver = get_driver_instance();
    con = driver->connect(host, user, pass);
    /* Connect to the MySQL test database */
    con->setSchema(database);

	return 1;
}

inline int DB_linux::close()
{
	if(con)
	{
		con->close();
		delete con;
		return 1;
	}
	return 0;
}

#ifdef DEBIAN


//for mysql connector 8.0.* /debian
inline std::vector<std::map<std::string, std::string> > DB_linux::query(const char query[])
{

	//printf("Sql: %s\r\n", query);

	std::vector<std::map<std::string, std::string>> result;


	if (mysql_query(&mysql, query) > 0) // ������. ���� ������ ���, �� ���������� ������
	{

		// ���� ���� ������, ...
		printf("Err: %s", mysql_error(&mysql));  // ... ������ ��
		return result; // � �������� ������
	}


	MYSQL_FIELD* field;

	res = mysql_store_result(&mysql); // ����� ���������,
	int num_fields = mysql_num_fields(res); // ���������� �����
	int num_rows = mysql_num_rows(res); // � ���������� �����.

										//printf("nums %d = %d\r\n", num_fields, num_rows);

	std::vector<std::string> field_name;

	for (int i = 0; i < num_fields; i++) // ������� �������� �����
	{
		field = mysql_fetch_field_direct(res, i); // ��������� �������� �������� ����
		field_name.push_back(field->name);
		//printf("| %s |", field->name);
	}

	//printf("\n");



	MYSQL_ROW row; // ������ ����� ������� ������

	for (int i = 0; i < num_rows; i++) // ����� �������
	{
		std::map<std::string, std::string> line;

		row = mysql_fetch_row(res); // �������� ������

		for (int l = 0; l < num_fields; l++)
		{
			if (row[l])
			{
				line.insert(std::pair<std::string, std::string>(field_name[l], row[l]));
			}
			else {
				line.insert(std::pair<std::string, std::string>(field_name[l], ""));
			}


			//line[field_name[l]] = row[l];

			//printf("| %s |", row[l]); // ������� ����

		}
		result.push_back(line);


		//printf("\n");
	}

	printf("Count records = %d\r\n", num_rows);
	mysql_free_result(res);


	return result;

}
#endif


#ifdef LINUX

inline int DB_linux::insert(const char query[])
{

	

	int result = 0;

	try {

		sql::Statement* stmt;
		stmt = con->createStatement();
		stmt->execute(query);
		delete stmt;

	}
	catch (sql::SQLException& e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line "
			<< __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}

	std::vector<std::map<std::string, std::string> > data = DB_linux::query("SELECT LAST_INSERT_ID() AS id");

	if(data.size() > 0)
	{
		std::map<std::string, std::string>::iterator it = data[0].begin();
		for (int i = 0; it != data[0].end(); it++, i++) {  // выводим их
			result = utils::ft_atoi(it->second.c_str());
			break;
			//std::cout << i << ") Ключ " << it->first << ", значение " << it->second << std::endl;
		}
	}
	

	//printf("Size %d %s\r\n", data.size(), data[0]["id"].c_str());
	//result = utils::ft_atoi(data[0]["id"].c_str());

	return result;
}


//mysql connector 5.7 yum install mysql-connector-odbc
inline std::vector<std::map<std::string, std::string> > DB_linux::query(const char query[])
{
	std::vector< std::map< std::string, std::string > > result;

	//printf("Sql: %s\r\n", query);

	try {

		sql::Statement* stmt;
		sql::ResultSet* res;
		sql::ResultSetMetaData* meta;



		//printf("QuerySql request\r\n");
		stmt = con->createStatement();
		res = stmt->executeQuery(query);

		meta = res->getMetaData();
		int num_fields = meta->getColumnCount();
		int num_rows = res->rowsCount();

		std::string field;
		std::vector<std::string> field_name;
		for (int i = 0; i < num_fields; i++) // ������� �������� �����
		{
			field = meta->getColumnName(i + 1);
			field_name.push_back(field);
			//printf("| %s |", field.c_str());
		}

		while (res->next()) {
			std::map<std::string, std::string> line;
			for (int l = 0; l < num_fields; l++)
			{
				std::string key = field_name[l];
				std::string val = res->getString(l + 1);
				//printf("sql: %s => %s\r\n", key.c_str(), val.c_str());
				line.insert(std::pair<std::string, std::string>(key, val));
			}
			result.push_back(line);

			//std::string val = res->getString(1);
			//std::cout << "\t... MySQL replies: ";

			//std::cout << val.c_str() << std::endl;
			//std::cout << "\t... MySQL says it again: ";

			//std::cout << res->getString(1) << std::endl;
		}

		//printf("sql=>row_ok\r\n");
	
		// con->close();
		//delete meta;
		//delete res;
		//delete stmt;
		// delete con;
		//printf("sql=>exit\r\n");
	}
	catch (sql::SQLException& e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line "
			<< __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << query << std::endl;
	}
	//printf("Sql finish\r\n");


	return result;
}

#endif
#endif