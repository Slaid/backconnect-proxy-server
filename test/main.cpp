
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <chrono>
#include <pthread.h>
#include <unistd.h>
#include "Singleton.h"

struct lmutex_t
{
    int id;
    int lock;
};

class lmutex_store
{
    DECLARE_SINGLETON(lmutex_store)
public:
    std::vector< lmutex_t* > list;
};

class lmutex
{
public:
    static int create()
    {
        struct lmutex_t *lm = (struct lmutex_t*)malloc(sizeof(lmutex_t));
        lm->id = lmutex_store::Instance()->list.size() + 1;
        lm->lock = 0;
        lmutex_store::Instance()->list.push_back(lm);
        return lm->id;
    }

    static int lock(int id)
    {
        struct lmutex_t *lm = lmutex_store::Instance()->list[id - 1];
        while(1)
        {
            if(!lm->lock)
            {
                break;
            }
            
            usleep(1);

            //printf(".");
        }
        lm->lock = 1;
        return 1;
    }

    static int unlock(int id)
    {
        struct lmutex_t *lm = lmutex_store::Instance()->list[id - 1];
        lm->lock = 0;
        return 1;
    }
};

int main(int argc, char *argv)
{
    int id = lmutex::create();
    printf("Id mutex %d\r\n", id);

    lmutex::lock(id);
    printf("lock\r\n");
    lmutex::unlock(id);

    printf("End\r\n");

    return 0;
}