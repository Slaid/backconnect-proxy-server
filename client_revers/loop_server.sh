#/bin/bash
cd "$(dirname "$0")"

ulimit -n 999999
echo 10000 > /proc/sys/kernel/threads-maxax
echo 10000 > /sys/fs/cgroup/pids/user.slice/user-0.slice/pids.max
result=1
while [ $result -ne 0 ]; do
    echo "`date +%d-%m-%Y_%H:%M:%S` Start loop_server<br />" >> error_log.txt
    ./revers
    result=$?
    echo "`date +%d-%m-%Y_%H:%M:%S` Server Error ${result}<br />" >> error_log.txt
    sleep 1
done