#/bin/bash
cd "$(dirname "$0")"

./stop_server.sh

make clean
make

nohup ./loop_server.sh >/dev/null 2>&1 &