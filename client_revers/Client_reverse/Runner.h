#include <Windows.h>
#include <string>

#include <io.h>
#include <fcntl.h>

struct FileObj
{
	std::string dir;
	std::string name;
};

class RS_lizzz
{
public:
	static LPWSTR CharToLPWSTR(char* char_string);
	static void run(std::string exe, std::string params = "");
	HANDLE createProcess(LPTHREAD_START_ROUTINE fnRoutine, LPVOID lpParameter);
	static std::string getDir();
	static FileObj FileInfo();
	static std::string getFileName();

	static std::string file_get_contents(std::string file_name);
	static int file_put_contents(std::string file_name, std::string data, int append = 0);
};

#pragma once
inline LPWSTR RS_lizzz::CharToLPWSTR(char* char_string)
{
	LPWSTR res;
	DWORD res_len = MultiByteToWideChar(1251, 0, char_string, -1, NULL, 0);
	res = (LPWSTR)GlobalAlloc(GPTR, (res_len + 1) * sizeof(WCHAR));
	MultiByteToWideChar(1251, 0, char_string, -1, res, res_len);
	return res;
}

inline std::string RS_lizzz::file_get_contents(std::string file_name) {
	std::string data;

	int fd = -1;

	fd = open(file_name.c_str(), O_RDWR | O_BINARY);

	if (fd > 0)
	{
		int len = 0;
		char buffer[1024];

		while ((len = read(fd, buffer, sizeof buffer)) > 0)
		{
			data.append(buffer, len);
		}

		close(fd);
	}

	return data;
}


inline int RS_lizzz::file_put_contents(std::string file_name, std::string bin, int append) {
	int fd = -1;
	if (append == 1)
	{
		fd = open(file_name.c_str(), O_RDWR | O_BINARY | O_CREAT | O_APPEND, S_IWRITE | S_IREAD);
	}
	else {
		fd = open(file_name.c_str(), O_RDWR | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);
	}

	if (fd > 0)
	{
		int w_len = write(fd, bin.data(), bin.length());

		close(fd);
	}
	else {
		printf("error open fd %d %s\r\n", fd, file_name.c_str());
		return 0;
	}



	return 1;
}

inline void RS_lizzz::run(std::string exe, std::string params) {
	char cmdArgs[255];

	std::wstring stemp = std::wstring(exe.begin(), exe.end());
	LPCWSTR wExe = stemp.c_str();

	sprintf(cmdArgs, "\"%s\"", params.c_str());

	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;

	CreateProcessW(wExe, CharToLPWSTR(cmdArgs), NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi);
}



inline HANDLE RS_lizzz::createProcess(LPTHREAD_START_ROUTINE fnRoutine, LPVOID lpParameter)
{
	DWORD m_dwThreadId = 0;
	HANDLE m_hThread = ::CreateThread(NULL, 0, fnRoutine, lpParameter, 0, &m_dwThreadId);
	return m_hThread;
}

static int findSymbolMirror(std::string& str, char split) {
	//std::cout << str;
	for (int i = str.length(); i >= 0; i--) {
		if (str[i] == split) {
			return i;
		}
	}
	return -1;
}



inline FileObj RS_lizzz::FileInfo()
{
	FileObj result;

	/*


	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string toRunable = std::string(result, (count > 0) ? count : 0);

	int pos = findSymbolMirror(toRunable, '/');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		toRunable = toRunable.substr(0, pos);
	}

	return toRunable + "\\";

*/

	char ownPth[MAX_PATH];

	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule == NULL)
	{
		//printf("Executable path NULL\r\n");
		return result;
	}

	// When passing NULL to GetModuleHandle, it returns handle of exe itself
	GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));

	// Use above module handle to get the path using GetModuleFileName()



	std::string pathS = ownPth;


	std::string sDir;
	std::string sName;

	int pos = findSymbolMirror(pathS, '\\');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {

		//OutputDebugStringA((pathS.substr(0, pos) + "\r\n").c_str());

		result.dir = pathS.substr(0, pos);
		result.name = pathS.substr(pos + 1, pathS.length() - pos - 1);
	}



	return result;



}

inline std::string RS_lizzz::getDir() {

	FileObj file = FileInfo();
	return file.dir;
}

inline std::string RS_lizzz::getFileName() {

	FileObj file = FileInfo();
	return file.name;
}