#pragma once



static int send_to_tunel(int socket, char* buffer, int len)
{
    int snd = -1;
    LizzzMutex::Instance("sender")->lock();
    snd = send(socket, buffer, len, MSG_NOSIGNAL);
    LizzzMutex::Instance("sender")->unlock();
    return snd;
}

static int pack(int socket_client, char buffer[], int& n)
{


    //std::string res;

    //printf("res %s\r\n", buffer + 5);

    char new_buff[1024 + 15] = { 0 };

    char header[15];
    sprintf(header, "ex%06d%06d", socket_client, n);


    memcpy(new_buff + 14, buffer, n);
    memcpy(new_buff, header, 14);

    n = n + 14;

    memcpy(buffer, new_buff, n);


    //char bf2[1024];
    //memcpy(bf2, buffer + 5 + 14, 11);
    //printf("nav2 %s\r\n", bf2);

    return 1;
}



static int read_data(int sock, char* buf, int size) {
    int bytes_read = 0, len = 0;
    while (bytes_read < size && ((len = recv(sock, buf + bytes_read, size - bytes_read, 0)) > 0)) {
        bytes_read += len;
    }
    if (len == 0 || len < 0) return -1;
    return bytes_read;
}



static int recv_from_tunel(int socket, int* client_socket, char buffer[])
{
    char buffer_navi[15];
    int nav_len = read_data(socket, buffer_navi, 14);
    if (nav_len <= 0)
    {
        printf("[PACKER] Tunel recv %d SOCKET_ERROR %d\r\n", nav_len, errno);
        return -1;
    }

    if (nav_len != 14)
    {
        printf("[PACKER] Tunel error nav_header len %d\r\n", nav_len);
        return -1;
    }

    int pack_body_len = 0;
    if (buffer_navi[0] == 'e' && buffer_navi[1] == 'x')
    {
        char socket_string[7] = { 0 };
        char pack_len[7] = { 0 };

        memcpy(socket_string, (buffer_navi)+2, 6);
        socket_string[6] = 0;

        memcpy(pack_len, (buffer_navi)+6 + 2, 6);
        pack_len[6] = 0;

        //printf("socket %s len %s = %d/%d\r\n", socket_string, pack_len, *len, atoi(pack_len));
        pack_body_len = atoi(pack_len);
        int ex_socket = atoi(socket_string);

        *client_socket = ex_socket;
    }
    else {
        printf("[PACKER] Tunel broken pack not ex len %d %s\r\n", nav_len, buffer_navi);
        return 0;
    }

    if (*client_socket <= 0 || pack_body_len <= 0 || pack_body_len > 1024)
    {
        printf("[Packer] broken pack\r\n");
        return 0;
    }


    int n = read_data(socket, buffer, pack_body_len);
    if (n <= 0) {
        printf("[PACKER] Error tunel recv 0\r\n");
        return -1;
    }

    return n;
}

