#pragma once
#include "Singleton.h"
#include <chrono>
#include <vector>

struct lmutex_model 
{
	int id;
	int status;
};

class lmutexStore
{
	DECLARE_SINGLETON(lmutexStore);
public:
	std::vector< lmutex_model* > lmutex_list;

	int create()
	{
		struct lmutex_model* ml = (struct lmutex_model*)malloc(sizeof lmutex_model);

		int id = lmutex_list.size() + 1;
		ml->status = 0;
		lmutex_list.push_back(ml);
		return id;
	}
};

class lmutex
{

public:
	lmutex()
	{
		integrator();
	}

	static int create()
	{
		return lmutexStore::Instance()->create();
	}

	static void lock(int index)
	{

	}

	int integrator()
	{
		while(1)
		{
			std::chrono::milliseconds(1);
			printf(".");
		}

		return 1;
	}
};