#include "Singleton.h"


class Service_app
{
    DECLARE_SINGLETON(Service_app)
public:
    Service_app();
    int InstallService();

    const char* service_name = "surf_proxy.exe";
    const char* service_path = "C:\\run.exe";
};

#pragma once
inline int Service_app::InstallService() {
    SC_HANDLE hSCManager = OpenSCManagerA(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    if (!hSCManager) {
        printf("Error: Can't open Service Control Manager");
        return -1;
    }

    SC_HANDLE hService = CreateServiceA(
        hSCManager,
        this->service_name,
        this->service_name,
        SERVICE_ALL_ACCESS,
        SERVICE_WIN32_OWN_PROCESS,
        SERVICE_DEMAND_START,
        SERVICE_ERROR_NORMAL,
        service_path,
        NULL, NULL, NULL, NULL, NULL
    );

    if (!hService) {
        int err = GetLastError();
        switch (err) {
        case ERROR_ACCESS_DENIED:
            printf("Error: ERROR_ACCESS_DENIED");
            break;
        case ERROR_CIRCULAR_DEPENDENCY:
            printf("Error: ERROR_CIRCULAR_DEPENDENCY");
            break;
        case ERROR_DUPLICATE_SERVICE_NAME:
            printf("Error: ERROR_DUPLICATE_SERVICE_NAME");
            break;
        case ERROR_INVALID_HANDLE:
            printf("Error: ERROR_INVALID_HANDLE");
            break;
        case ERROR_INVALID_NAME:
            printf("Error: ERROR_INVALID_NAME");
            break;
        case ERROR_INVALID_PARAMETER:
            printf("Error: ERROR_INVALID_PARAMETER");
            break;
        case ERROR_INVALID_SERVICE_ACCOUNT:
            printf("Error: ERROR_INVALID_SERVICE_ACCOUNT");
            break;
        case ERROR_SERVICE_EXISTS:
            printf("Error: ERROR_SERVICE_EXISTS");
            break;
        default:
            printf("Error: Undefined");
        }
        CloseServiceHandle(hSCManager);
        return -1;
    }
    CloseServiceHandle(hService);

    CloseServiceHandle(hSCManager);
    printf("Success install service!");
    return 0;
}

inline Service_app::Service_app()
{
    //InstallService();
}
