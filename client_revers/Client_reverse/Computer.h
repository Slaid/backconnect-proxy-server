#pragma once
#include <string>

#ifdef WIN32
#include <Windows.h>
#include "psapi.h"
#endif

class Computer
{
public:
	static std::string memInfo()
	{
		std::string result;
#ifdef WIN32
		MEMORYSTATUSEX statex;

		statex.dwLength = sizeof(statex);

		GlobalMemoryStatusEx(&statex);

#define DIV 1048576
#define WIDTH 7

		result = _itoa((statex.ullTotalPhys / DIV) - (statex.ullAvailPhys / DIV)) + "/" + _itoa(statex.ullTotalPhys / DIV) + " MB";
#endif
		return result;
	}

	static std::string currMemInfo()
	{
		std::string result;

#ifdef WIN32
		HANDLE hProcess = GetCurrentProcess();
		int processID = GetCurrentProcessId();
		PROCESS_MEMORY_COUNTERS pmc;

		//printf("\nProcess ID: %u\n", processID);

		// Print information about the memory usage of the process.
		hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);
		if (NULL == hProcess)
			return "";

		if (GetProcessMemoryInfo(hProcess, (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc)))
		{
			//printf("\tWorkingSetSize: 0x%08X - %u\n", pmc.WorkingSetSize, pmc.WorkingSetSize / 1024);
			//printf("\tQuotaPeakPagedPoolUsage: 0x%08X - %u\n", pmc.QuotaPeakPagedPoolUsage, pmc.QuotaPeakPagedPoolUsage / 1024);
			//printf("\tQuotaPagedPoolUsage: 0x%08X - %u\n", pmc.QuotaPagedPoolUsage, pmc.QuotaPagedPoolUsage / 1024); pmc.QuotaPeakNonPagedPoolUsage, pmc.QuotaPeakNonPagedPoolUsage / 1024);
			//printf("\tQuotaNonPagedPoolUsage:0x%08X-%u\n", pmc.QuotaNonPagedPoolUsage, pmc.QuotaNonPagedPoolUsage / 1024);
			//printf("\tPagefileUsage: 0x%08X - %u\n", pmc.PagefileUsage, pmc.PagefileUsage / 1024);
			//printf("\tPeakPagefileUsage: 0x%08X - %u\n", pmc.PeakPagefileUsage, pmc.PeakPagefileUsage / 1024);
			result += "usage: " + _itoa(pmc.PagefileUsage / 1024) + " kb";
			//printf("\tcb: 0x%08X - %u\n", pmc.cb, pmc.cb / 1024);
		}

		// wait until process is dead
		// WaitForSingleObject( hProcess , INFINITE );
		// GetProcessMemoryInfo( hProcess, (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc) );
		///fprintf(stdout, " PeakWorkingSetSize : %d\n", pmc.PeakWorkingSetSize);
		//fprintf(stdout, " PrivateUsage (Bytes): %d\n", pmc.PrivateUsage);
		//fprintf(stdout, " PrivateUsage (KB) : %f\n", (float)pmc.PrivateUsage / 1024.0);
		//printf("Mem %d\r\n", history);
		CloseHandle(hProcess);
#endif

		return result;
	}
};