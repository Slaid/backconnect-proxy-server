#include <string>
#include <time.h>
#include <fstream>

namespace DNS
{
    bool InitDns();
    bool GetDns(const char* argv, in_addr* addr);
    std::string getIp(std::string hostname);

    int getIpWithPach(unsigned char* buf, int n, in_addr* addr);
};

static std::string g_dns = "8.8.8.8";

std::string DNS::getIp(std::string hostname)
{
    in_addr addr;

    DNS::GetDns(hostname.c_str(), &addr);

    return inet_ntoa(addr);
}

bool DNS::InitDns()
{
    std::fstream file;
    file.open("/etc/resolv.conf", std::ios::in);

    bool ret = false;

    if (file.is_open())
    {
        char* buf = (char*)malloc(5000);

        do {
            const char* flag = "nameserver";

            file.read(buf, 5000);

            std::string strOptions = buf;

            int pos = strOptions.find(flag);

            std::string ip = strOptions.substr(pos, strOptions.length() - pos).c_str();

            if (pos == -1)
                break;

            pos = ip.find("\n");
            int len = strlen(flag) + 1;

            if (pos == -1)
            {
                ip = ip.substr(len, ip.length() - len);
            }
            else
            {
                ip = ip.substr(len, pos - len);
            }

            g_dns = ip;

            //infoLog("DNS Server Address : %s", ip.c_str());

            ret = true;

        } while (false);

        if (buf)
            free(buf);

        file.close();
    }

    return ret;
}



bool DNS::GetDns(const char* argv, in_addr* addr)
{
    struct sockaddr_in sa;
    int result = lizzz_inet_pton(AF_INET, argv, &(sa.sin_addr));
    if (result != 0)
    {
        *addr = sa.sin_addr;
        //printf("result %d %s\r\n", result, argv);
        return true;
    }

    time_t ident;
    int fd;
    int rc;
    int serveraddrlent;
    const char* q;
    unsigned char* p;
    unsigned char* countp;
    unsigned char reqBuf[512] = { 0 };
    unsigned char rplBuf[512] = { 0 };
    sockaddr_in serveraddr;


    bool ret = false;

    do
    {
        //udp
        fd = socket(AF_INET, SOCK_DGRAM, 0);

        //set timeout
#ifdef LINUX
        timeval tv_out;
        tv_out.tv_sec = 5;
        tv_out.tv_usec = 0;
        setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_out, sizeof(timeval));

#else
        int outtime = 5000;
        setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&outtime, sizeof(int));

#endif

        if (fd == -1)
        {
            /*	perror("error create udp socket");*/
            break;
        }

        time(&ident);
        //copy
        p = reqBuf;
        //Transaction ID
        *(p++) = ident;
        *(p++) = ident >> 8;
        //Header section
        //flag word = 0x0100
        *(p++) = 0x01;
        *(p++) = 0x00;
        //Questions = 0x0001
        //just one query
        *(p++) = 0x00;
        *(p++) = 0x01;
        //Answer RRs = 0x0000
        //no answers in this message
        *(p++) = 0x00;
        *(p++) = 0x00;
        //Authority RRs = 0x0000
        *(p++) = 0x00;
        *(p++) = 0x00;
        //Additional RRs = 0x0000
        *(p++) = 0x00;
        *(p++) = 0x00;
        //Query section
        countp = p;
        *(p++) = 0;
        for (q = argv; *q != 0; q++)
        {
            if (*q != '.')
            {
                (*countp)++;
                *(p++) = *q;
            }
            else if (*countp != 0)
            {
                countp = p;
                *(p++) = 0;
            }
        }
        if (*countp != 0)
            *(p++) = 0;

        //Type=1(A):host address
        *(p++) = 0;
        *(p++) = 1;
        //Class=1(IN):internet
        *(p++) = 0;
        *(p++) = 1;

        //	 printf("\nRequest:\n");
        //	 printmessage(reqBuf);

        //fill
        memset((void*)&serveraddr, 0, sizeof(serveraddr));
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_port = htons(53);
        serveraddr.sin_addr.s_addr = inet_addr(g_dns.c_str());

        //send to DNS Serv
        if (sendto(fd, (char*)reqBuf, p - reqBuf, MSG_NOSIGNAL, (sockaddr*)&serveraddr, sizeof(serveraddr)) < 0)
        {
            /*		perror("error sending request");*/
            break;
        }

        //recev the reply
        memset((void*)&serveraddr, 0, sizeof(serveraddr));
        serveraddrlent = sizeof(serveraddr);

        rc = recvfrom(fd, (char*)rplBuf, sizeof(rplBuf), MSG_NOSIGNAL, (sockaddr*)&serveraddr, &serveraddrlent);
        if (rc < 0)
        {
            /*		perror("error receiving request\n");*/
            break;
        }


        //	printf("\nReply: %d %s\n", rc, rplBuf);
        ret = getIpWithPach(rplBuf, rc, addr);

    } while (false);

    closesocket(fd);

    return ret;
}

int DNS::getIpWithPach(unsigned char* buf, int n, in_addr* addr)
{
    unsigned char ip[5];
    //print out results
    int cnt = 0;
    for (int i = n; i != 0; i--)
    {
        //printf("%c = %d\r\n", buf[i], buf[i]);
        ip[3 - cnt] = buf[i - 1];
        //printf("%d ", ip[cnt]);
        cnt++;
        if (cnt == 4)
            break;
    }

    memcpy(&addr->s_addr, ip, 4);


    return true;
}

