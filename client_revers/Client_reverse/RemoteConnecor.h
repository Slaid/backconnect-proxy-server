﻿#include "Singleton.h"
#include "sblist.h"
#include "xLinux.h"
//#include <io.h>
//#include <fcntl.h>

union sockaddr_union {
	struct sockaddr_in  v4;
	struct sockaddr_in6 v6;
};

#define SOCKADDR_UNION_AF(PTR) (PTR)->v4.sin_family

#define SOCKADDR_UNION_LENGTH(PTR) ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET  ) ? sizeof((PTR)->v4) : ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET6 ) ? sizeof((PTR)->v6) : 0 ) )

#define SOCKADDR_UNION_ADDRESS(PTR) ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET  ) ? (void*) &(PTR)->v4.sin_addr  : ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET6 ) ? (void*) &(PTR)->v6.sin6_addr : (void*) 0 ) )

#define SOCKADDR_UNION_PORT(PTR) ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET  ) ? (PTR)->v4.sin_port  : ( \
	( SOCKADDR_UNION_AF(PTR) == AF_INET6 ) ? (PTR)->v6.sin6_port : 0 ) )

class PackData
{
public:
	int len;
	char* buffer;
	//int socket_client;


	PackData(char* buf, int len)
	{
		this->buffer = (char*)malloc(sizeof(char) * (len + 1));
		memcpy(this->buffer, buf, len);
		this->len = len;
	}
};

struct PSK
{
	char* buffer;
	int len;
};

class StackList
{
public:
	sblist* stack_psk;
	int next_num;

	StackList()
	{
		next_num = 0;
		stack_psk = sblist_new(sizeof(PSK*), 8);
	}

	void add(char* buffer, int n)
	{
		struct PSK* psk = (struct PSK*)malloc(sizeof(struct PSK));

		psk->buffer = (char*)malloc(sizeof(char) * n);
		memcpy(psk->buffer, buffer, n);
		psk->len = n;

		LizzzMutex::Instance("get")->lock();
		sblist_add(stack_psk, &psk);
		LizzzMutex::Instance("get")->unlock();

	}

	struct PSK* get()
	{

		struct PSK* psk = NULL;
		LizzzMutex::Instance("get")->lock();
		if (this->next_num < sblist_getsize(stack_psk))
		{
			psk = *((struct PSK**)sblist_get(stack_psk, this->next_num));
			this->next_num++;
		}
		LizzzMutex::Instance("get")->unlock();
		return psk;

	}

	void clearStack()
	{
		if (this->next_num < sblist_getsize(stack_psk))
		{
			LizzzMutex::Instance("get")->lock();
			printf("Clear stack %d elem of %d\r\n", sblist_getsize(stack_psk) - next_num, sblist_getsize(stack_psk));
			for (int i = next_num; i < sblist_getsize(stack_psk); i++)
			{
				struct PSK* psk = *((struct PSK**)sblist_get(stack_psk, i));
				free(psk->buffer);
				free(psk);

			}

			sblist_free_items(stack_psk);
			LizzzMutex::Instance("get")->unlock();
		}


	}
};

struct Request
{
	int id;
	int client_socket;

	sockaddr_in saddr;
	std::string remote_ip;
	int remote_port;
	SOCKET sremote;

	int tunel;
	int is_close;
	int is_close_reciver;
	int last_active;

	HANDLE ptr;
	HANDLE ptr2;

	volatile int done;

	StackList* stack;
};

class RemoteConnecor
{
	DECLARE_SINGLETON(RemoteConnecor)
public:
	RemoteConnecor();


	void ttlCloseConnection();

	void clear_all();


	//Request* createClient(int socket);
	Request* getClient(int socket);

	void die(Request* req);

	void closeCon(Request* req);

	sblist* con_list;
	int collect();


	std::vector< PackData* > sender_stack;

	int tunel_socket;


	void requestHandler(int client_socket, char* buffer, int n);

	int sleep_controller();



private:



};

#pragma once

inline void RemoteConnecor::ttlCloseConnection()
{
	int timestump = utils::timeSinceEpochMillisec() / 1000;

	LizzzMutex::Instance("mutex")->lock();

	int i;
	for (i = 0; i < sblist_getsize(con_list); i++)
	{
		Request* req = *((Request**)sblist_get(con_list, i));
		if (req && req->last_active + 30 < timestump)
		{
			req->is_close = 1;
			closesocket(req->sremote);
		}
	}

	printf("Connection clean %d pull size\r\n", i);
	LizzzMutex::Instance("mutex")->unlock();

}

static void clearInstance()
{
	while (1)
	{
		//RemoteConnecor::Instance()->ttlCloseConnection();
		Sleep(5 * 1000);
	}

}

static void sleep_controller_thread()
{
	while (1)
	{
		RemoteConnecor::Instance()->sleep_controller();
		Sleep(1000);
	}
}

static union sockaddr_union bind_addr;

inline RemoteConnecor::RemoteConnecor()
{
	bind_addr.v4.sin_family = AF_UNSPEC;

	con_list = sblist_new(sizeof(Request*), 8);

	createThread((LPTHREAD_START_ROUTINE)clearInstance, (void*)0);
	createThread((LPTHREAD_START_ROUTINE)sleep_controller_thread, (void*)0);

}

enum errorcode {
	EC_SUCCESS = 0,
	EC_GENERAL_FAILURE = 1,
	EC_NOT_ALLOWED = 2,
	EC_NET_UNREACHABLE = 3,
	EC_HOST_UNREACHABLE = 4,
	EC_CONN_REFUSED = 5,
	EC_TTL_EXPIRED = 6,
	EC_COMMAND_NOT_SUPPORTED = 7,
	EC_ADDRESSTYPE_NOT_SUPPORTED = 8,
};

static int count_con = 0;



int bindtoip(int fd, union sockaddr_union* bindaddr) {
	socklen_t sz = SOCKADDR_UNION_LENGTH(bindaddr);
	if (sz)
		return bind(fd, (struct sockaddr*)bindaddr, sz);
	return 0;
}

int resolve(const char* host, unsigned short port, struct addrinfo** addr) {
	struct addrinfo hints;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	char port_buf[8];
	snprintf(port_buf, sizeof port_buf, "%u", port);
	return getaddrinfo(host, port_buf, &hints, addr);
}

static int createSocket()
{
	int fd = 0;
	while (true)
	{
		fd = socket(AF_INET, SOCK_STREAM, 0);
		if (fd > 0)
			break;
	}
	return fd;
}

static int xCon(std::string ip, int port, int sremote)
{
	//printf("connect\r\n");
	addrinfo* remote = NULL;
	addrinfo hint = { 0 };
	std::string ipnew;

	int nRet = getaddrinfo(ip.c_str(), NULL, &hint, &remote);

	//printf("%d\r\n", nRet);
	if (nRet == 0)
		ipnew = inet_ntoa(((sockaddr_in*)remote->ai_addr)->sin_addr);


	freeaddrinfo(remote);


	if (ipnew.empty())
	{
		return 0;
	}

	//int socket_fd = -1;
	// Creating socket file descriptor 
	//socket_fd = ::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);


	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(ipnew.c_str());
	clientService.sin_port = htons(port);



	/*
	struct linger lg;
	lg.l_onoff = 1;
	lg.l_linger = 10;

	setsockopt(sremote, SOL_SOCKET, SO_LINGER, (char*)&lg, sizeof(lg));
	setsockopt(sremote, SOL_SOCKET, SO_OOBINLINE, NULL, 0);
	*/

	/*
	int opt = TRUE;
	if (setsockopt(sremote, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(int)) < 0)
		printf("setsockopt(SO_REUSEADDR) failed");

	if (SOCKADDR_UNION_AF(&bind_addr) != AF_UNSPEC && bindtoip(sremote, &bind_addr) == -1)
	{
		printf("Error bind\r\n");
	}

	struct linger lg = { 1,10 };
	setsockopt(sremote, SOL_SOCKET, SO_LINGER, (char*)&lg, sizeof(lg));
	*/


	//unsigned long ul = 1;
	//ioctlsocket(sremote, FIONBIO, &ul);
//int sremote = socket(AF_INET, SOCK_STREAM, 0);

//printf("Sremote %d\r\n", sremote);

	int iResult = connect(sremote, (sockaddr*)&clientService, sizeof(clientService));

	if (iResult == SOCKET_ERROR) {
		printf("Error connect %s:%d SOCKET %d - %d\n", ipnew.c_str(), port, iResult, sremote);
		closesocket(sremote);
		return 0;
	}

	printf("Socket connected is ok SOCKET %d\r\n", sremote);

	if (sremote > 1000000) exit(1);

	return sremote;
	/*
	fd_set fdsc;
	FD_ZERO(&fdsc);
	FD_SET(sremote, &fdsc);

	struct timeval timeout = { 60 * 15, 0 };

	int res = select(sremote, &fdsc, 0, 0, &timeout);
	*/

	//printf("Select %d errno %d\r\n", res, errno);


	//closesocket(socket_fd);
	//return 0;


}

static int connect_socks_target(char* buf, size_t n, int sremote) {

	//printf("Auth_resolve %d\r\n", n);
	//for (int i = 0; i < n; i++)
	//{
	//	printf("%c", buf[i]);
	//}
	//printf("\r\n");



	if (n < 5) return -EC_GENERAL_FAILURE;
	if (buf[0] != 5) return -EC_GENERAL_FAILURE;
	if (buf[1] != 1) return -EC_COMMAND_NOT_SUPPORTED; /* we support only CONNECT method */
	if (buf[2] != 0) return -EC_GENERAL_FAILURE; /* malformed packet */



	int af = AF_INET;
	size_t minlen = 4 + 4 + 2, l;
	char namebuf[256];
	//struct addrinfo* remote;

	switch (buf[3]) {
	case 4: /* ipv6 */
		af = AF_INET6;
		minlen = 4 + 2 + 16;
		/* fall through */
	case 1: /* ipv4 */
		if (n < minlen) return -EC_GENERAL_FAILURE;
		if (namebuf != inet_ntop(af, buf + 4, namebuf, sizeof namebuf))
			return -EC_GENERAL_FAILURE; /* malformed or too long addr */
		break;
	case 3: /* dns name */
		l = buf[4];
		minlen = 4 + 2 + l + 1;
		if (n < 4 + 2 + l + 1) return -EC_GENERAL_FAILURE;
		memcpy(namebuf, buf + 4 + 1, l);
		namebuf[l] = 0;
		break;
	default:
		return -EC_ADDRESSTYPE_NOT_SUPPORTED;
	}
	unsigned short port;
	port = (int)buf[minlen - 2] * 256 + (unsigned char)buf[minlen - 1];

	/*
	addrinfo* remote = NULL;
	addrinfo hint = { 0 };

	int nRet = getaddrinfo(namebuf, NULL, &hint, &remote);
	if (nRet != 0)
	{
		return -EC_GENERAL_FAILURE;
	}

	std::string ip = inet_ntoa(((sockaddr_in*)remote->ai_addr)->sin_addr);

	freeaddrinfo(remote);

	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(ip.c_str()); ;
	clientService.sin_port = htons(port);

	int socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int iResult = connect(socket_fd, (SOCKADDR*)&clientService, sizeof(clientService));
	if (iResult == SOCKET_ERROR) {
		printf("connect function failed with error: %ld %s:%d\n", WSAGetLastError(), ip.c_str(), port);
		iResult = closesocket(socket_fd);
		if (iResult == SOCKET_ERROR)
			printf("closesocket function failed with error: %ld\n", WSAGetLastError());
		//WSACleanup();

		return -1;
	}
	*/


	count_con++;
	//printf("Success %d) %s:%d SOCKET %d\r\n", count_con, ip.c_str(), port, socket_fd);
	//LizzzMutex::Instance("qwe")->lock();
	int fd = xCon(namebuf, port, sremote);


	//LizzzMutex::Instance("qwe")->unlock();
	return fd;


	//printf("connect_socks_target %s:%d\r\n", namebuf, port);
	/* there's no suitable errorcode in rfc1928 for dns lookup failure */

	//printf("Resolve %s:%d (%d:%d)\r\n", namebuf, port, buf[minlen - 2], buf[minlen - 1]);
	/*
	static union sockaddr_union bind_addr;// = { .v4.sin_family = AF_UNSPEC };
	bind_addr.v4.sin_family = AF_UNSPEC;


	addrinfo* remote = NULL;
	addrinfo hint = { 0 };

	int nRet = getaddrinfo(namebuf, NULL, &hint, &remote);
	if (nRet != 0)
	{
		return -EC_GENERAL_FAILURE;
	}

	sockaddr_in svr = { 0 };
	svr.sin_family = AF_INET;
	svr.sin_addr = ((sockaddr_in*)remote->ai_addr)->sin_addr;
	svr.sin_port = htons(port);

	//req->saddr = svr;

	std::string remote_ip = inet_ntoa(svr.sin_addr);
	int remote_port = ntohs(svr.sin_port);
	int sremote = 0;

	//if (resolve(namebuf, port, &remote)) return -EC_GENERAL_FAILURE;


	sremote = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//sremote = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);
	if (sremote == -1) {
		printf("Error create WSASocket\r\n");
		return -EC_GENERAL_FAILURE;
	}

	if (SOCKADDR_UNION_AF(&bind_addr) != AF_UNSPEC && bindtoip(sremote, &bind_addr) == -1)
	{
		printf("Error bind\r\n");
	}
	*/
	/*
	//printf("Created %d\r\n", sremote);
	struct hostent* hp;
	hp = gethostbyname("127.0.0.1");


	struct sockaddr_in sinc;
	sinc.sin_family = AF_INET;
	//sinc.sin_addr.s_addr = *(unsigned long*)hp->h_addr;
	//sinc.sin_addr.s_addr = htonl(INADDR_ANY);
	//sinc.sin_port = htons(606);

	int opt = TRUE;
	if (setsockopt(sremote, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(int)) < 0)
		printf("setsockopt(SO_REUSEADDR) failed");

	if (bind(sremote, (struct sockaddr*)&sinc, sizeof(struct sockaddr_in)) < 0) {
		printf("bind\r\n");
	}


	struct linger lg;
	lg.l_onoff = 1;
	lg.l_linger = 10;

	setsockopt(sremote, SOL_SOCKET, SO_LINGER, (char*)&lg, sizeof(lg));
	setsockopt(sremote, SOL_SOCKET, SO_OOBINLINE, NULL, 0);
	*/


	//unsigned long ul = 1;
	//ioctlsocket(sremote, FIONBIO, &ul);
	/*
	if (connect(sremote, remote->ai_addr, remote->ai_addrlen) == -1)
	{
		printf("Error connect\r\n");
		freeaddrinfo(remote);
		return FALSE;
	}
	*/

	//printf("Connect to %s:%d\r\n", req->remote_ip.c_str(), req->remote_port);
	//printf("Start connect\r\n");
	/*
	if (!ServerHelper::Connect(sremote, svr))
	{
		printf("Error %d) data len %d\r\n", -1, n);
		//printf("FAIL %s:%d socket %d\r\n", req->remote_ip.c_str(), req->remote_port, req->sremote);
		freeaddrinfo(remote);
		return FALSE;
	}


	count_con++;

	//if (connect(fd, remote->ai_addr, remote->ai_addrlen) == -1)
	//	return FALSE;

	freeaddrinfo(remote);

	printf("Success %d) %s:%d SOCKET %d count(%d)\r\n", sremote, remote_ip.c_str(), remote_port, sremote, count_con);
	*/
	//printf("SUCESS %s:%d socket %d\r\n", req->remote_ip.c_str(), req->remote_port, req->sremote);

	return 0;
}




inline void RemoteConnecor::clear_all()
{
	LizzzMutex::Instance("mutex")->lock();
	int i;
	for (i = 0; i < sblist_getsize(con_list); i++)
	{
		Request* req = *((Request**)sblist_get(con_list, i));
		RemoteConnecor::Instance()->die(req);
	}

	printf("All closed %d pull size\r\n", i);
	LizzzMutex::Instance("mutex")->unlock();
}

static int send_close_to_tunel(int client_socket)
{
	/*
	send pack close socket
	*/
	//printf("Send close to => tunel\r\n");
	char buf[7 + 15];
	memcpy(buf, "2525625", 7);
	int n = 7;
	pack(client_socket, buf, n);

	send_to_tunel(RemoteConnecor::Instance()->tunel_socket, buf, n);
	//RemoteConnecor::Instance()->stackSenderAdd(buf, n);

	return 1;
}




/*
std::mutex sender_tunel;

inline void RemoteConnecor::stackSenderAdd(char* buffer, int len)
{
	PackData* data = new PackData(buffer, len);

	//sender_tunel.lock();
	//RemoteConnecor::Instance()->response_stack->add(data);
	//sender_tunel.unlock();
}

*/



static int sleep_total = 0;

inline int RemoteConnecor::sleep_controller()
{
	if (sleep_total >= 1000)
	{
		sleep_total -= 1000;
	}

	return 0;
}

static int timeDelay()
{
	sleep_total += 1000;

	printf("Total Sleep %d\r\n", sleep_total);
	if (sleep_total >= 1000 * 1000)
	{
		printf("I'm sleep\r\n");
		Sleep(10000);
		sleep_total = 0;
	}


	return 0;
}

inline Request* RemoteConnecor::getClient(int socket)
{
	Request* req = NULL;

	LizzzMutex::Instance("mutex")->lock();


	size_t i;
	for (i = 0; i < sblist_getsize(con_list);) {
		Request* tmp = *((Request**)sblist_get(con_list, i));
		if (tmp->client_socket == socket)
		{
			req = tmp;
			break;
		}
		else
			i++;
	}

	LizzzMutex::Instance("mutex")->unlock();

	return req;
}



inline void RemoteConnecor::die(Request* req)
{

	if (req->is_close == 0)
	{
		req->is_close = 1;
		shutdown(req->sremote, 2);
		closesocket(req->sremote);
	}

	/*

	//pthread_mutex_lock(&mutex);
	Request* req = getClient(socket);// client_list[socket];
	if (req)
	{
		shutdown(req->sremote, 0);
		req->is_close = 1;
	}

	*/
	//pthread_mutex_unlock(&mutex);
}



static int reciver_remote(void* lpParameter)
{
	Request* req = (Request*)lpParameter;

	int tunel = RemoteConnecor::Instance()->tunel_socket;
	int client_socket = req->client_socket;
	int n;
	int len;

	while (TRUE) {
		if (req->is_close == 1)
		{
			break;
		}

		char buffer[1024 + 15];
		n = recv(req->sremote, buffer, 1024, 0);
		if (n <= 0) {
			req->is_close = 1;
			send_close_to_tunel(req->client_socket);
			break;
		}

		//printf("Download %d\r\n", n);

		//req->last_active = utils::timeSinceEpochMillisec() / 1000;
		//req->update();

		pack(req->client_socket, buffer, n);

		send_to_tunel(tunel, buffer, n);

		//RemoteConnecor::Instance()->stackSenderAdd(buffer, n);
	}



	RemoteConnecor::Instance()->die(req);


	//printf("Resiver close %d\r\n", client_socket);

	return 1;
}



static void thread_sender(Request* req)
{
	//printf("Create reciver\r\n");
	req->ptr2 = createThread((LPTHREAD_START_ROUTINE)reciver_remote, (void*)req);

	//WSABUF DataBuf;
	//DWORD SendBytes;
	//WSAOVERLAPPED SendOverlapped;

	while (TRUE)
	{
		if (req->is_close == 1)
		{
			break;
		}


		struct PSK* data = req->stack->get();


		if (data && data->len && data->buffer)
		{

			//DataBuf.len = data->len;
			//DataBuf.buf = data->buffer;

			//int snd = write(req->sremote, data->buffer, data->len);
			int snd = send(req->sremote, data->buffer, data->len, MSG_NOSIGNAL);
			//int snd = WSASend(req->sremote, &DataBuf, 1, &SendBytes, 0, 0 , NULL);

			//printf("Upload %d bytes\r\n", SendBytes);

			free(data->buffer);
			free(data);

			if (snd <= 0)
			{
				req->is_close = 1;
			}
		}
		else {

			//printf(".");
			utils::xsleep(100);
		}

	}

#ifdef WIN32
	::WaitForSingleObject(req->ptr2, INFINITE);
#else
	pthread_join(req->ptr, 0);
#endif
	//pthread_join(req->ptr2, 0);

}

static int handler(void* lpParameter)
{
	Request* req = (Request*)lpParameter;
	int client_socket = req->client_socket;
	int tunel = RemoteConnecor::Instance()->tunel_socket;

	struct PSK* data = req->stack->get();

	req->sremote = connect_socks_target(data->buffer, data->len, req->sremote);
	//printf("Sremote %d\r\n", req->sremote);
	if (req->sremote <= 0 || req->is_close == 1)
	{

		char buf[10 + 14] = { 5, 1, 0, 1 , 0, 0, 0, 0, 0, 0 };
		int ln = 10;
		pack(client_socket, buf, ln);
		send_to_tunel(tunel, buf, ln);

	}
	else {

		char buf[10 + 14] = { 5, 0, 0, 1 , 0, 0, 0, 0, 0, 0 };
		int ln = 10;
		pack(client_socket, buf, ln);
		send_to_tunel(tunel, buf, ln);


		thread_sender(req);

	}



	free(data->buffer);
	free(data);
	//free(data->buffer);
	//delete data;

	//char buff[1024];
	//while (recv(req->sremote, buff, sizeof buff, 0) > 0) {
	//	printf("Wait closed\r\n");
	//}

	RemoteConnecor::Instance()->closeCon(req);


	return 0;
}

inline void RemoteConnecor::closeCon(Request* req)
{
	shutdown(req->sremote, 2);
	closesocket(req->sremote);
	req->stack->clearStack();
	req->done = 1;

	/*
	return;

	Request* req = NULL;

	LizzzMutex::Instance("mutex")->lock();

	size_t i;
	for (i = 0; i < sblist_getsize(con_list);) {
		Request* tmp = *((Request**)sblist_get(con_list, i));

		if (tmp->client_socket == client_socket) {
			req = tmp;
			sblist_delete(con_list, i);
			break;
		}
		else
			i++;
	}


	if (req)
	{
		printf("Thread closed %d pull_size %d\r\n", req->sremote, sblist_getsize(RemoteConnecor::Instance()->con_list));

		req->clearStack();

		if (req->is_close == 0)
		{
			req->is_close = 1;

			shutdown(req->sremote, 2);
			closesocket(req->sremote);
		}
		//WSACleanup();

		delete req;
	}

	LizzzMutex::Instance("mutex")->unlock();
	 */
}


inline int RemoteConnecor::collect()
{
	LizzzMutex::Instance("mutex")->lock();

	size_t i;
	for (i = 0; i < sblist_getsize(con_list);) {
		struct Request* req = *((struct Request**)sblist_get(con_list, i));

		if (req->done) {
			printf("Thread closed %d pull_size %d\r\n", req->sremote, sblist_getsize(con_list));

#ifdef WIN32
			::WaitForSingleObject(req->ptr, INFINITE);
#else
			pthread_join(req->ptr, 0);
#endif


			//pthread_join(req->ptr, 0);
			//pthread_join(req->ptr2, 0);
			//if(req->ptr)
			//    joinThread(req->ptr);

			//if(req->ptr2)
			//    joinThread(req->ptr2);


			sblist_delete(con_list, i);
			delete req->stack;
			free(req);

		}
		else
			i++;
	}
	//printf("Closed pull_size %d\r\n", i);

	LizzzMutex::Instance("mutex")->unlock();
	//foo();

	return 1;
}

inline void RemoteConnecor::requestHandler(int client_socket, char* buffer, int n)
{
	Request* req = getClient(client_socket);

	if (!req)
	{

		if (n == 7 && buffer[0] == '2' && buffer[1] == '5')
		{
			printf("socket is closed\r\n");
			return;
		}

		if (buffer[0] != 5 && buffer[1] != 1)
		{
			printf("Error navi header\r\n");
			send_close_to_tunel(client_socket);
			return;
		}

		collect();

		struct Request* req = (struct Request*)malloc(sizeof(struct Request));


		req->is_close = 0;
		req->done = 0;
		req->client_socket = client_socket;


		req->stack = new StackList();
		req->stack->add(buffer, n);

		LizzzMutex::Instance("mutex")->lock();
		sblist_add(con_list, &req);
		LizzzMutex::Instance("mutex")->unlock();

		req->sremote = createSocket();

		DWORD m_dwThreadId;
		req->ptr = createThread((LPTHREAD_START_ROUTINE)handler, (void*)req); //::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)handler, req, 0, &m_dwThreadId);




		//printf("Total thread %d count Objects last socket %d\r\n", sblist_getsize(con_list), req->client_socket);

	}
	else {

		if (n == 7 && buffer[0] == '2' && buffer[1] == '5')
		{
			if (buffer[2] == '9')
			{
				exit(1);
			}
			RemoteConnecor::Instance()->die(req);
		}
		else {
			req->stack->add(buffer, n);
		}



	}





	/*
	PackData* data = new PackData(buffer, n);
	//data->socket_client = client_socket;

	Request* req = connection_list[client_socket];
	if (!req)
	{
		req = new Request();
		req->client_socket = client_socket;
		req->tunel = this->tunel_socket;
		req->is_close = 0;
		req->step = 1;

		req->stack_list = new StackList("Upload " + utils::xitoa(req->client_socket));
		req->stack_list->add(data);

		connection_list[req->client_socket] = req;

		DWORD m_dwThreadId = 0;
		req->th = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)handler, req, 0, &m_dwThreadId);

		total_thread++;
		printf("Total thread %d count Objects %d\r\n", total_thread, connection_list.size());

		timeDelay(); //����������� �� �������� ����������� � �������
	}
	else {
		if (n == 7 && buffer[0] == '2' && buffer[1] == '5')
		{

			printf("Browser send close socket %d\r\n", req->client_socket);
			//close(req->sremote);
			req->is_close = 1;
			return;
		}

		req->stack_list->add(data);
	}

	*/
}

