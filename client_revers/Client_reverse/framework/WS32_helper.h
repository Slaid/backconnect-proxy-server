#ifndef WS32HELPER_H
#define WS32HELPER_H

//#include "preIncl.h"
#include "Singleton.h"
#include <string>
#include <map>
#include <sstream>

#include "xLinux.h"
//#include "n18.h"
#include "Processor.h"

typedef struct
{
	std::string header;
	int contentLen;
	std::string body;
	int startBody;
	std::string host;
	std::string port;
	std::string uri;
	int isPost;
	std::string protocol;
	std::string postData;
	
	std::string method;
} HeaderSubject;

class ServerHelper {
	DECLARE_SINGLETON(ServerHelper)



public:
	ServerHelper();

	int readHeader(int s);
	int getUri(std::string header, std::string &result, int &isPost);
	int parseHeader();
	static int parseUrl(std::string url, std::string &hostname, int &port, std::string &uri);
	void runCallback(int len, int max);
	ServerHelper* progress(void(*f)(int&, int&));
	std::string loadUrlInst(std::string url, bool res_header = 0);
	static void Close(int s);
	static bool Connect(int s, sockaddr_in& r_addr);
	int getPost();

	static sockaddr_in getIpByHost(std::string host, int port);

	void addChunk(std::string &data);

	HeaderSubject *subj;

	int socket;

	void(*callback)(int &i, int &j);

	typedef std::map<std::string, std::string> headerList;
	headerList header_list;

private:
	int procCeil = 0;
};



inline ServerHelper::ServerHelper() {

#ifdef WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

	this->subj = new HeaderSubject;
}

inline int ServerHelper::readHeader(int s) {
	this->socket = s;

	this->subj->header = "";
	char buffer[1024];

	while (true) {
		int nCount = recv(socket, buffer, 1024, 0);

		if (nCount <= 0) {
			break;
		}

		//xprintf("REQUEST %s\r\n", buffer);
		this->subj->header.append(buffer, nCount);
		if (this->subj->header.find("\r\n\r\n") != std::string::npos) {
			return this->parseHeader();
		}


	}
	return 0;
}

static in_addr GetName(const char* name)
{


	in_addr ret = { 0 };

	addrinfo *info = NULL;
	addrinfo hint = { 0 };

	int nRet = getaddrinfo(name, NULL, &hint, &info);

	if (nRet == 0)
	{
		ret = ((sockaddr_in*)info->ai_addr)->sin_addr;
	}


	return ret;
}

inline sockaddr_in ServerHelper::getIpByHost(std::string host, int port)
{
	sockaddr_in svr = { 0 };
	svr.sin_family = AF_INET;
	svr.sin_addr = GetName(host.c_str());
	svr.sin_port = htons(port);
	
	return svr;
}


inline int ServerHelper::parseHeader() {

	std::string line;

	std::stringstream fd;
	fd << this->subj->header;

	int numLine = 0;
	while (getline(fd, line))
	{
		if (line.length() == 0)
			break;

		if (numLine == 0)
		{
			std::vector<std::string> arr = utils::explode(line, " ");
			this->subj->uri = arr[1];
			this->subj->method = arr[0];
		}
		else
		{
			utils::ft_tolower(line);

			int posDel = line.find(":");
			if (posDel != 0 && posDel != std::string::npos)
			{
				
				std::string key = line.substr(0, posDel);
				std::string val = line.substr(posDel + 1, line.length() - posDel - 2);
				utils::trim(key);
				utils::trim(val);

				this->header_list[key] = val;
				//printf("line '%s' - '%s'\r\n", key.c_str(), val.c_str());
			}

			
		}
		
		numLine++;
	}

	return 1;

}

inline int ServerHelper::parseUrl(std::string url, std::string &hostname, int &port, std::string &uri) {
	int pos;
	if (url.find("https://") == 0) {
		return 0;
	}

	if (url.find("http://") == std::string::npos) {
		return 0;
	}

	url = url.substr(7, url.length() - 7);

	pos = url.find("/");
	if (pos == std::string::npos) {
		hostname = url;
		url = "/";
		return 1;
	}

	hostname = url.substr(0, pos);
	uri = url.substr(pos, url.length() - pos);

	pos = hostname.find(":");
	if (pos != std::string::npos) {
		port = atoi(hostname.substr(pos + 1, hostname.length() - pos - 1).c_str());
		hostname = hostname.substr(0, pos);
	}
	else {
		port = 80;
	}

	return 1;
}


inline ServerHelper* ServerHelper::progress(void(*f)(int&, int&))
{
	this->callback = f;
	return this;
}

inline void ServerHelper::runCallback(int len, int max)
{
	if (max == 0) return (void)0;

	int proc = 100 * len / max;

	if (proc > this->procCeil)
	{
		this->procCeil = proc;

		//xprintf("progress %d\r\n", len);
		if (this->callback)
		{

			this->callback(len, max);
		}
	}


}



inline void ServerHelper::addChunk(std::string &body)
{

	std::string result = utils::xitoa(body.length(), 16) + "\r\n";
	if (body.length() > 0)
	{
		result += body + "\r\n";
	}
}

inline std::string ServerHelper::loadUrlInst(std::string url, bool res_header) {


	printf("url: %s\r\n", url.c_str());

	//runCallback(0, 0);

	std::string data = "";

	std::string hostname, uri;

	int port;
	parseUrl(url, hostname, port, uri);
	printf("Parse url: %s:%d %s\r\n", hostname.c_str(), port, uri.c_str());
	//return 1;

	struct addrinfo* result = NULL,
		* ptr = NULL,
		hints;

	memset(&hints, 0, sizeof(hints));
	//ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	char buf[11];
	sprintf(buf, "%i", port);


	int res = getaddrinfo(hostname.c_str(), buf, &hints, &result);

	if (res != 0)
	{
		printf("getaddrinfo\r\n");
		return 0;
	}

	ptr = result;

	int ConnectSocket = ::socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

	if (ConnectSocket == INVALID_SOCKET)
	{
		printf("freeaddrinfo\r\n");
		freeaddrinfo(result);
		return 0;
	}

	// Connect to server
	res = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);


	if (res == SOCKET_ERROR)
	{
		printf("SOCKET_ERROR\r\n");
		Close(ConnectSocket);
		ConnectSocket = INVALID_SOCKET;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		printf("INVALID_SOCKET\r\n");
		return 0;
	}

	std::string header = "GET ";
	header += uri;
	header += " HTTP/1.1\r\n";
	header += "Connection: close\r\n";
	header += "Host: ";
	header += hostname;
	if (port != 80) {
		header += ":";

		char buf[11];
		sprintf(buf, "%i", port);
		header += buf;
	}
	header += "\r\n";
	header += "User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) ";
	header += "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36\r\n";
	header += "\r\n";

	Processor* proc = new Processor();


	int step = 1; int stepN = 0;
	int contentLen = -1;
	int sended = send(ConnectSocket, header.data(), header.length(), 0);
	while (true) {
		char buffer[4096];
		int len = recv(ConnectSocket, buffer, sizeof(buffer), 0);
		if (len <= 0) {
			break;
		}

		stepN += len;
		data.append(buffer, len);

		if (contentLen == -1 && data.find("\r\n\r\n")) {
			proc->parseResponse(data);
			contentLen = proc->contentLength;

		}


		//xprintf("data %d\r\n", contentLen);
		//runCallback(stepN, contentLen);
	}
	proc->parseResponse(data);

	if (contentLen > 0) {

		printf("Len %d = %d\r\n", contentLen, proc->body.length());
		if (contentLen != proc->body.length()) {
			printf("Error update\r\n");
			return 0;
		}
	}


	proc->removeChunk();



	if (res_header) {
		data = proc->header;
		data += "\r\n";
		data += proc->body;

	}
	else {
		data = proc->body;
	}
	delete proc;


	return data;
}

inline bool ServerHelper::Connect(int s, sockaddr_in& r_addr)
{
	s = connect(s, (struct sockaddr *)&r_addr, sizeof(sockaddr));
	return s == 0;
}

inline void ServerHelper::Close(int s)
{
#ifdef LINUX
	close(s);
	//shutdown(s, SHUT_RDWR);
#else
	struct linger sl;
	sl.l_onoff = 1;		/* non-zero value enables linger option in kernel */
	sl.l_linger = 0;	/* timeout interval in seconds */
	setsockopt(s, SOL_SOCKET, SO_LINGER, (char*)&sl, sizeof(sl));

	int res = shutdown(s, 0);
	int res2 = closesocket(s);
	if (res2 == -1)
	{
		printf("WSA %d\r\n", WSAGetLastError());
	}

	//close(s);

	printf("Closed %d-%d-%d\r\n", res, res2, s);
#endif
}

inline int ServerHelper::getPost()
{
	//printf("method %s\r\n", this->subj->method.c_str());
	if (this->subj->method.find("POST") == 0)
	{
		int maxPostSize = atoi(this->header_list["content-length"].c_str());
		int headerSize = this->subj->header.find("\r\n\r\n") + 4;

		int postSize = this->subj->header.length() - headerSize;

		//printf("Post len %d of %d\r\n", maxPostSize, postSize);

		this->subj->postData = this->subj->header.substr(headerSize, postSize);

		char buffer[4096] = { 0 };
		while (postSize < maxPostSize) {
			int nCount = recv(socket, buffer, 1024 * 4, 0);

			if (nCount <= 0) {
				break;
			}

			this->subj->postData.append(buffer, nCount);

		}

		//printf("Data %s", this->subj->postData.c_str());
		return 1;
	}

	return 0;
}

#endif WS32HELPER_H