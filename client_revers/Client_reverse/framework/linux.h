#pragma once
#include <string>
#include <vector>

class utils
{
public:
	static int xsleep(int ms)
	{
#ifdef WIN32
		Sleep(ms);
#else
		usleep(ms * 1000);
#endif
		return 0;
	}

	struct s_proto {
		int protocol;
		std::string hostname;
		int port;
		std::string uri;
	};

	static s_proto parseUrl_latest(std::string url) {


		s_proto proto;
		proto.protocol = 0;
		proto.port = 80;
		proto.hostname = "localhost";

		//auto t = explode("privet ++hhas ++askjd++", "++");

		//exit(1);

		std::vector<std::string> v = explode(url, "/");

		//myprintf("explode ok %d\r\n", v.size());
		if (v.size() < 2)
		{
			//myprintf("Error count args %d\r\n", v.size());
			return proto;
		}

		//myprintf("Error count args %d\r\n", v.size());

		//myprintf("for explode %s %s \r\n", v[0].c_str(), v[2].c_str());

		if (v[2].length() == 0)
		{
			return proto;
		}
		std::vector<std::string> s = explode(v[2], ":");

		//myprintf("%s %s\r\n", s[0].c_str(), s[1].c_str());

		if (s.size() == 0)
		{
			proto.hostname = v[2];
		}
		else
		{
			proto.hostname = s[0];
			proto.port = atoi(s[1].c_str());
		}
		//myprintf("Error count args\r\n");
		if (v[0].find("https") == 0)
		{
			proto.protocol = 1;
		}

		v = std::vector<std::string>(v.begin() + 3, v.end());

		std::string uri = "/" + implode(v, "/");




		//proto.port = v[2];
		//"http://sf.aferon.com:8080/asdas/asd/asd/asd/"
		return proto;
	}

	static std::string implode(std::vector<std::string> arr, char *delim)
	{
		std::string result;
		for (int i = 0; i < arr.size(); i++)
		{
			result += arr[i] + delim;
		}

		result = result.substr(0, result.length() - strlen(delim));

		return result;
	}

	static std::vector<std::string> explode(std::string str, char *delim)
	{

		str += delim;

		std::vector<std::string> result;

		while (true)
		{


			int pos = str.find(delim);
			if (pos == std::string::npos)
			{
				break;
			}

			if (str.length() == 0)
			{
				break;
			}

			//myprintf("pos %d\r\n", pos);

			std::string part = str.substr(0, pos);

			//myprintf("part %s\r\n", part.c_str());
			result.push_back(part);

			pos = pos + strlen(delim);
			str = str.substr(pos, str.length() - pos);
			//myprintf("res %s\r\n", str.c_str());

			//break;
		}


		return result;
	}
};