#ifndef CFG_H
#define CFG_H

#include <string>
#include <vector>
#include <map>
#include <fstream>

#ifdef WIN32
#include <windows.h>
#else
#define PATH_MAX 260
#endif

#include <random>

#include "n18.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
using namespace rapidjson;



class Config {
public:
	static Config* Instance();
	Config();

	std::string fileGetContents(std::string path);
	static int findSymbolMirror(std::string &str, char split);
	static int addAutorun(std::string name, std::string pathToExe);
	static std::string getLinkToExe();
	static std::string getExePath();
	static int saveToFile(std::string path, std::string data);

	int reload();
	Config* load(std::string path);
	int setVal(std::string key, std::string val);
	int saveJson(std::string path = ".");

	static void gen_random(char *s, const int len);
	int genUid(std::string rel);

	std::string get(std::string key);

private:

	std::string localPath;
	Document DOM;
};


static Config *inst_config;

inline Config *Config::Instance()
{
	if (!inst_config)
	{
		inst_config = new Config;
	}
	return inst_config;
}


inline Config::Config()
{

}

struct cfgItem {
	std::string key;
	std::string val;
};


std::vector<cfgItem> cfgData;

inline std::string Config::fileGetContents(std::string path) {
	std::string data = "";
	char *buffer;

	int length = 0;
	std::ifstream input(path.c_str(), std::ios::binary);

	char buf[1024];
	//int length = 0;
	if (input) {
		// get length of file:
		input.seekg(0, input.end);
		length = input.tellg();
		input.seekg(0, input.beg);


		while (true) {
			int part = sizeof(buf);
			if (length < part) {
				part = length;
			}
			input.read(buf, part);
			data.append(buf, part);

			length -= part;
			if (length == 0) {
				break;
			}
		}

		return data;
	}

	return "";
}

inline int Config::findSymbolMirror(std::string &str, char split) {
	//std::cout << str;
	for (int i = str.length(); i >= 0; i--) {
		if (str[i] == split) {
			return i;
		}
	}
	return -1;
}

inline int Config::addAutorun(std::string name, std::string pathToExe) {

	char tmp[1024];
	sprintf(tmp, "reg add \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\" /v %s /t reg_sz /d \"%s\" /f", name.c_str(), pathToExe.c_str());
	system(tmp);

	return 1;
}

inline std::string Config::getLinkToExe() {

#ifdef LINUX

	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string toRunable = std::string(result, (count > 0) ? count : 0);

	int pos = findSymbolMirror(toRunable, '/');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		toRunable = toRunable.substr(0, pos);
	}

	return toRunable;
#else

	char ownPth[MAX_PATH];

	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule == NULL)
	{
		printf("Executable path NULL\r\n");
		return "";

	}

	// When passing NULL to GetModuleHandle, it returns handle of exe itself
	GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));

	// Use above module handle to get the path using GetModuleFileName()
	std::string pathS = ownPth;

	return pathS;
#endif
}

inline std::string Config::getExePath() {

#ifdef LINUX

	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string toRunable = std::string(result, (count > 0) ? count : 0);

	int pos = findSymbolMirror(toRunable, '/');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		toRunable = toRunable.substr(0, pos);
	}

	return toRunable + "\\";
#else

	char ownPth[MAX_PATH];

	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule == NULL)
	{
		printf("Executable path NULL\r\n");
		return "";

	}

	// When passing NULL to GetModuleHandle, it returns handle of exe itself
	GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));

	// Use above module handle to get the path using GetModuleFileName()
	std::string pathS = ownPth;


	int pos = findSymbolMirror(pathS, '\\');
	//printf("pos %d %s len %d\r\n", pos, str.c_str(), strlen(ownPth));
	if (pos != -1) {
		pathS = pathS.substr(0, pos);
	}

	//printf("path %s\r\n", pathS.c_str());
	/*
	if (merge.length() > 0) {
	pathS += "\\";
	pathS.append(merge.c_str(), merge.length());
	}
	printf("Usage %s\r\n", pathS.c_str());
	*/
	return pathS + "\\";
#endif


}

inline int Config::saveToFile(std::string path, std::string data) {

	std::ofstream output(path.c_str(), std::ios_base::out | std::ios::binary);
	if (output.is_open()) {
		xprintf("Writen %d bytes\r\n", data.length());
		output.write(data.data(), data.length());
	}
	else {
		xprintf("Error opened file %s\r\n", path.c_str());
		return 0;
	}
	output.close();

	return 1;
}


inline int Config::reload() {
	std::string cfg = Config::fileGetContents(localPath);

	if (cfg.length() > 0) {
		if (DOM.Parse(cfg.c_str()).HasParseError()) {
			DOM.Parse("{}");
		}
	}
	else {
		DOM.Parse("{}");
	}

	//this->parseCfg(cfg);

	xprintf("Loaded cfg keys: %d\r\n", DOM.Size());

	return 1;
}

inline Config* Config::load(std::string path) {
	localPath = path;

	this->reload();

	return this;
}

inline int Config::setVal(std::string key, std::string val) {


	if (!DOM.HasMember(key.c_str())) {
		Value uid;
		uid.SetString(val.data(), val.length(), DOM.GetAllocator());
		Value newKey(key.c_str(), DOM.GetAllocator());

		DOM.AddMember(newKey, uid, DOM.GetAllocator());
	}
	else {
		DOM[key.c_str()].SetString(val.data(), DOM.GetAllocator());
	}


	//this->saveJson();
	return 1;
}

inline int Config::saveJson(std::string path) {

	StringBuffer sb;
	PrettyWriter<StringBuffer> writer(sb);
	DOM.Accept(writer);


	if (path == ".") {
		xprintf("save json to default: %s\r\n", localPath.c_str());
		Config::saveToFile(localPath, sb.GetString());
	}
	else {
		xprintf("save json to: %s\r\n", path.c_str());
		Config::saveToFile(path, sb.GetString());
	}


	return 1;
}

inline void Config::gen_random(char *s, const int len) {
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";


	srand(std::time(0));
	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	s[len] = 0;
}

inline int Config::genUid(std::string rel) {

	if (!this->get("uid").length()) {
		char randomString[20];

		Config::gen_random(randomString, sizeof(randomString));

		this->setVal("uid", randomString);
	}

	if (!this->get("rel").length()) {
		this->setVal("rel", rel);
	}

	this->saveJson();

	return 1;
}

inline std::string Config::get(std::string key) {
	if (!DOM.HasMember(key.c_str())) {
		return "";
	}
	else {
		return DOM[key.c_str()].GetString();
	}
}


#endif
