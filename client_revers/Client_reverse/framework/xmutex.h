#ifndef XMUTEX_H
#define XMUTEX_H

#include <map>
#include <string>

#ifdef WIN32
#include <mutex>
#else
#include <pthread.h>
#endif

class LizzzMutex
{
public:
	static LizzzMutex* Instance(std::string exe);


	void lock();
	void unlock();
	bool isLock;

private:
	std::string currentIndex;
	



};

typedef std::map<std::string, LizzzMutex*> mutexList;
static mutexList mutex_list;


#ifdef WIN32
typedef std::map<std::string, std::mutex> mutexListO;
static mutexListO mutex_list_origin;

#else

typedef std::map<std::string, pthread_mutex_t> mutexListO;
static mutexListO mutex_list_origin;

#endif

//static std::mutex tmpMutex;

inline LizzzMutex* LizzzMutex::Instance(std::string exe)
{
	

	if (mutex_list[exe])
	{
		return mutex_list[exe];
	}

	LizzzMutex* clr = new LizzzMutex;

	clr->currentIndex = exe;
	clr->isLock = 0;

	mutex_list[exe] = clr;

	return clr;
}


inline void LizzzMutex::lock()
{
	
	
#ifdef WIN32
	//tmpMutex.lock();
	mutex_list_origin[LizzzMutex::currentIndex].lock();
#else 
	pthread_mutex_lock(&mutex_list_origin[LizzzMutex::currentIndex]);


#endif // LINUX
	//this->isLock = 1;
	//printf("Lock %s\r\n", LizzzMutex::currentIndex.c_str());
}


inline void LizzzMutex::unlock()
{
	
	
#ifdef WIN32
	//tmpMutex.unlock();
	mutex_list_origin[LizzzMutex::currentIndex].unlock();

#else 

	pthread_mutex_unlock(&mutex_list_origin[LizzzMutex::currentIndex]);

#endif // LINUX
	//this->isLock = 0;
	//printf("Unlock %s\r\n", LizzzMutex::currentIndex.c_str());
}



#endif