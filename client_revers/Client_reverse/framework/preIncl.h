#pragma once

#ifdef LINUX
	#define WSAGetLastError() 1
	

	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <time.h>
	#include <wait.h>
	#include <cstring>
	#include <netdb.h>
	#include <errno.h>

	#define SOCKET_ERROR -1
	#define INVALID_SOCKET 0

typedef unsigned long DWORD;
typedef void* (*LPTHREAD_START_ROUTINE)(void*);
#define FALSE false
#define TRUE true
typedef void* LPVOID;

#else

	#include <winsock2.h>
	#pragma comment(lib,"ws2_32.lib")
	#include <ws2tcpip.h>
	#include "windows.h"
	#include <tchar.h>
	#include "io.h"
	#include <fcntl.h>

#endif

#include <string>
#include <vector>
#include <stdarg.h>

