#pragma once
class connection
{
public:
	int index;
	int socket;
	std::string socket_ip;
	int socket_port;

	std::string first_request;

	std::string hostname;
	std::string remote_ip;
	int remote_port;
	int sremote;

	int unpack_socket;
	char *buffer;
	int len;

	int lock;
	int is_auth;
	int isClosed;

	int proxyType;

	int status;
	int state;
	int is_bot;
	int max_threads;

	connection *child;

	sockaddr_in saddr;
};

struct client {
	struct sockaddr_in addr;
	int port;
	int fd;
};

struct thread_t {
	HANDLE pt;
	struct client client;
	volatile int  done;
	volatile int is_free;
};