﻿#include "Singleton.h"
#include "sblist.h"
#include "xLinux.h"
//#include <io.h>
//#include <fcntl.h>
#include "Dns.h"


class PackData
{
public:
	int len;
	char* buffer;
	//int socket_client;


	PackData(char* buf, int len)
	{
		this->buffer = (char*)malloc(sizeof(char) * (len + 1));
		memcpy(this->buffer, buf, len);
		this->len = len;
	}
};

struct PSK
{
	char* buffer;
	int len;
};

class StackList
{
public:
	sblist* stack_psk;
	int next_num;
	pthread_mutex_t mutex;

	StackList()
	{
		next_num = 0;
		stack_psk = sblist_new(sizeof(PSK*), 8);
		pthread_mutex_init(&mutex, NULL);
	}

	void add(char* buffer, int n)
	{
		struct PSK* psk = (struct PSK*)malloc(sizeof(struct PSK));

		psk->buffer = (char*)malloc(sizeof(char) * n);
		memcpy(psk->buffer, buffer, n);
		psk->len = n;

		pthread_mutex_lock(&mutex);
		sblist_add(stack_psk, &psk);
		pthread_mutex_unlock(&mutex);

	}

	struct PSK* get()
	{

		struct PSK* psk = NULL;
		pthread_mutex_lock(&mutex);
		if (this->next_num < sblist_getsize(stack_psk))
		{
			psk = *((struct PSK**)sblist_get(stack_psk, this->next_num));
			this->next_num++;
		}
		pthread_mutex_unlock(&mutex);
		return psk;

	}

	void clearStack()
	{
		pthread_mutex_lock(&mutex);
		if (this->next_num < sblist_getsize(stack_psk))
		{

			//printf("Clear stack %d elem of %d\r\n", sblist_getsize(stack_psk) - next_num, sblist_getsize(stack_psk));
			for (int i = next_num; i < sblist_getsize(stack_psk); i++)
			{
				struct PSK* psk = *((struct PSK**)sblist_get(stack_psk, i));
				free(psk->buffer);
				free(psk);

			}

			sblist_free_items(stack_psk);

		}
		pthread_mutex_unlock(&mutex);

	}
};

class Request
{
public:

	int client_socket;

	int sremote;

	int is_close;

	int last_active;

	HANDLE ptr;

	volatile int done;

	StackList* stack;


	Request()
	{
		sremote = this->createSocket();
		is_close = 0;
		done = 0;
		ptr = 0;
		last_active = utils::timeSinceEpochMillisec() / 1000;
		stack = new StackList();
	}

	int createSocket()
	{
		int fd;
		while (true)
		{
			fd = socket(AF_INET, SOCK_STREAM, 6);
			if (fd > 0)
			{
				//printf("Socket created %d\r\n", fd);
				break;
			}
			else {
				printf("Error create socket %d\r\n", fd);
			}

		}

		return fd;
	}

	int pack_send(int socket, const char* buffer, int len)
	{
		char bufferTmp[1024 + 15];
		int n = len;
		memcpy(bufferTmp, buffer, len);

		pack(socket, bufferTmp, n);
		return send(this->sremote, bufferTmp, n, MSG_NOSIGNAL);
	}

	void clear()
	{
		if (stack)
		{
			stack->clearStack();
			delete stack;
		}

	}
};

class RemoteConnector
{
	DECLARE_SINGLETON(RemoteConnector)
public:
	RemoteConnector();

	void setPing(Request* req);

	void ttlCloseConnection();

	void clear_all();


	//Request* createClient(int socket);
	//Request* getClient(int socket);

	void die(Request* req);


	sblist* con_list;
	void collect();

	void add(Request* req);

	std::vector< PackData* > sender_stack;

	int tunel_socket;
	int last_active;

	Request* getByClientSocket(int socket);
	void requestHandler(int client_socket, char* buffer, int n);

	int sleep_controller();
	//static int createSocket();

	pthread_mutex_t mutex;
};

#pragma once

inline void RemoteConnector::ttlCloseConnection()
{
	int timestump = utils::timeSinceEpochMillisec() / 1000;

	pthread_mutex_lock(&mutex);

	int i;
	int i_res = 0;
	for (i = 0; i < sblist_getsize(con_list); i++)
	{
		Request* req = *((Request**)sblist_get(con_list, i));
		if (req->is_close == 0 && req->last_active + 30 < timestump)
		{
			//printf("Closed TTL 30 socket %d status %d client: %d th: %d\r\n", req->sremote, req->done, th->client_socket, th->pt);
			RemoteConnector::Instance()->die(req);
			i_res++;
		}
	}

	printf("Connection clean %d pull size %d \r\n", i_res, i);

	pthread_mutex_unlock(&mutex);
	collect();

}


inline void RemoteConnector::clear_all()
{
	pthread_mutex_lock(&mutex);

	printf("All closed %d pull size\r\n", sblist_getsize(con_list));

	int i;
	for (i = 0; i < sblist_getsize(con_list); i++)
	{
		Request* req = *((Request**)sblist_get(con_list, i));

		RemoteConnector::Instance()->die(req);
	}



	pthread_mutex_unlock(&mutex);
	//LizzzMutex::Instance("mutex")->unlock();
	collect();
	printf("End closed\r\n");

	//collect();
}

static void* clearInstance(void* data)
{
	while (1)
	{
		RemoteConnector::Instance()->ttlCloseConnection();
		Sleep(5 * 1000);
	}

}

static void* sleep_controller_thread(void* data)
{
	while (1)
	{
		RemoteConnector::Instance()->sleep_controller();
		Sleep(1000);
	}
}


static void* ping_obj(void* data)
{
	Request* req = (Request*)data;
	std::string ping_data = "{}";
	while (1)
	{
		int snd = req->pack_send(1, ping_data.data(), ping_data.length());
		if (snd > 0)
		{
			printf("Ping send %d\r\n", snd);
			req->last_active = utils::timeSinceEpochMillisec() / 1000;
		}
		Sleep(10 * 1000);
	}
}

inline RemoteConnector::RemoteConnector()
{
	pthread_mutex_init(&mutex, NULL);
	con_list = sblist_new(sizeof(Request*), 8);
	last_active = utils::timeSinceEpochMillisec() / 1000;


	createThread((LPTHREAD_START_ROUTINE)clearInstance, (void*)0);
	createThread((LPTHREAD_START_ROUTINE)sleep_controller_thread, (void*)0);
}

inline void RemoteConnector::setPing(Request* req)
{
	createThread((LPTHREAD_START_ROUTINE)ping_obj, (void*)req);
}

enum errorcode {
	EC_SUCCESS = 0,
	EC_GENERAL_FAILURE = 1,
	EC_NOT_ALLOWED = 2,
	EC_NET_UNREACHABLE = 3,
	EC_HOST_UNREACHABLE = 4,
	EC_CONN_REFUSED = 5,
	EC_TTL_EXPIRED = 6,
	EC_COMMAND_NOT_SUPPORTED = 7,
	EC_ADDRESSTYPE_NOT_SUPPORTED = 8,
};




static int num = 0;

#ifdef WIN32

int connect_s(int sock, const char* host, int port, int timeout)
{
	TIMEVAL Timeout;
	Timeout.tv_sec = timeout;
	Timeout.tv_usec = 0;
	struct sockaddr_in address;  /* the libc network address data structure */


								 // printf("created %d\r\n", sock);

	address.sin_addr.s_addr = inet_addr(host); /* assign the address */
	address.sin_port = htons(port);            /* translate int2port num */
	address.sin_family = AF_INET;

	//set the socket in non-blocking
	unsigned long iMode = 1;
	int iResult = ioctlsocket(sock, FIONBIO, &iMode);
	if (iResult != NO_ERROR)
	{
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}

	if (connect(sock, (struct sockaddr*)&address, sizeof(address)) == false)
	{
		return false;
	}

	// restart the socket mode
	iMode = 0;
	iResult = ioctlsocket(sock, FIONBIO, &iMode);
	if (iResult != NO_ERROR)
	{
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}

	fd_set Write, Err;
	FD_ZERO(&Write);
	FD_ZERO(&Err);
	FD_SET(sock, &Write);
	FD_SET(sock, &Err);

	// check if the socket is ready
	select(0, NULL, &Write, &Err, &Timeout);
	if (FD_ISSET(sock, &Write))
	{
		return true;
	}
	printf("Timeout socket %d\r\n", sock);

	return false;
}

#else

int connect_s(int sockfd, const char* host, int port, int timeout)
{
	int rc = 0;
	timeout = timeout * 1000;

	struct sockaddr_in address;
	address.sin_addr.s_addr = inet_addr(host); /* assign the address */
	address.sin_port = htons(port);            /* translate int2port num */
	address.sin_family = AF_INET;

	// Set O_NONBLOCK
	int sockfd_flags_before;
	if ((sockfd_flags_before = fcntl(sockfd, F_GETFL, 0) < 0)) return -1;
	if (fcntl(sockfd, F_SETFL, sockfd_flags_before | O_NONBLOCK) < 0) return -1;
	// Start connecting (asynchronously)
	do {
		if (connect(sockfd, (struct sockaddr*)&address, sizeof(address)) < 0) {
			// Did connect return an error? If so, we'll fail.
			if ((errno != EWOULDBLOCK) && (errno != EINPROGRESS)) {
				rc = -1;
			}
			// Otherwise, we'll wait for it to complete.
			else {
				// Set a deadline timestamp 'timeout' ms from now (needed b/c poll can be interrupted)
				struct timespec now;
				if (clock_gettime(CLOCK_MONOTONIC, &now) < 0) { rc = -1; break; }
				struct timespec deadline;

				deadline.tv_sec = now.tv_sec;
				deadline.tv_nsec = now.tv_nsec + timeout * 1000000l;

				// Wait for the connection to complete.
				do {
					// Calculate how long until the deadline
					if (clock_gettime(CLOCK_MONOTONIC, &now) < 0) { rc = -1; break; }
					int ms_until_deadline = (int)((deadline.tv_sec - now.tv_sec) * 1000l
						+ (deadline.tv_nsec - now.tv_nsec) / 1000000l);
					if (ms_until_deadline < 0) { rc = 0; break; }
					// Wait for connect to complete (or for the timeout deadline)
					struct pollfd pfds[] = { { .fd = sockfd,.events = POLLOUT } };
					rc = poll(pfds, 1, ms_until_deadline);
					// If poll 'succeeded', make sure it *really* succeeded
					if (rc > 0) {
						int error = 0; socklen_t len = sizeof(error);
						int retval = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len);
						if (retval == 0) errno = error;
						if (error != 0) rc = -1;
					}
				}
				// If poll was interrupted, try again.
				while (rc == -1 && errno == EINTR);
				// Did poll timeout? If so, fail.
				if (rc == 0) {
					errno = ETIMEDOUT;
					rc = -1;
				}
			}
		}
	} while (0);
	// Restore original O_NONBLOCK state
	if (fcntl(sockfd, F_SETFL, sockfd_flags_before) < 0) return -1;
	// Success
	return rc;
}

#endif


static int xCon(std::string host, int port, int* sremote)
{
	in_addr addr;
	if (!DNS::GetDns(host.c_str(), &addr))
	{
		printf("Error DNS Server %s\r\n", host.c_str());
		return 0;
	}
	std::string real_ip = inet_ntoa(addr);

	//sockaddr_in clientService;
	//clientService.sin_family = AF_INET;
	//clientService.sin_addr = addr;
	//clientService.sin_port = htons(port);

	//int sock = RemoteConnector::createSocket();
	// int sremote = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//int sremote = RemoteConnector::createSocket();
	int iResult = connect_s(*sremote, real_ip.c_str(), port, 5);
	//printf("Res %d\r\n", sr);
	// int iResult = connect_s(sremote, (sockaddr*)&clientService, sizeof(clientService), 5);

	if (iResult <= 0) {
		printf("Error connect %s %s:%d SOCKET %d\n", host.c_str(), real_ip.c_str(), port, *sremote);
		return 0;
	}

	//std::string mem = utils::xitoa(getValue() / 1024) + "Kb";

	num++;
	printf("Success(%d) %s %s:%d (cnt %d) SOCKET %d\r\n", 1, host.c_str(), real_ip.c_str(), port, num, *sremote);

	if (*sremote > 1000000) exit(1);

	return 1;



}

static int connect_socks_target(char* buf, size_t n, int* sremote) {

	//printf("Auth_resolve %d\r\n", n);
	//for (int i = 0; i < n; i++)
	//{
	//	printf("%c", buf[i]);
	//}
	//printf("\r\n");



	if (n < 5) return -EC_GENERAL_FAILURE;
	if (buf[0] != 5) return -EC_GENERAL_FAILURE;
	if (buf[1] != 1) return -EC_COMMAND_NOT_SUPPORTED; /* we support only CONNECT method */
	if (buf[2] != 0) return -EC_GENERAL_FAILURE; /* malformed packet */



	int af = AF_INET;
	size_t minlen = 4 + 4 + 2, l;
	char namebuf[256];
	//struct addrinfo* remote;

	switch (buf[3]) {
	case 4: /* ipv6 */
		af = AF_INET6;
		minlen = 4 + 2 + 16;
		/* fall through */
	case 1: /* ipv4 */
		if (n < minlen) return -EC_GENERAL_FAILURE;
		if (namebuf != lizzz_inet_ntop(af, buf + 4, namebuf, sizeof namebuf))
			return -EC_GENERAL_FAILURE; /* malformed or too long addr */
		break;
	case 3: /* dns name */
		l = buf[4];
		minlen = 4 + 2 + l + 1;
		if (n < 4 + 2 + l + 1) return -EC_GENERAL_FAILURE;
		memcpy(namebuf, buf + 4 + 1, l);
		namebuf[l] = 0;
		break;
	default:
		return -EC_ADDRESSTYPE_NOT_SUPPORTED;
	}
	unsigned short port;
	port = (int)buf[minlen - 2] * 256 + (unsigned char)buf[minlen - 1];

	//printf("namebuf %s sremote %d\r\n", namebuf, *sremote);
	//int fd = 

	return xCon(namebuf, port, sremote);
}





static int send_close_to_tunel(int client_socket)
{
	/*
	send pack close socket
	*/
	//printf("Send close to => tunel\r\n");
	char buf[7 + 15];
	memcpy(buf, "2525625", 7);
	int n = 7;
	pack(client_socket, buf, n);

	send_to_tunel(RemoteConnector::Instance()->tunel_socket, buf, n);
	//RemoteConnector::Instance()->stackSenderAdd(buf, n);

	return 1;
}



static int sleep_total = 0;

inline int RemoteConnector::sleep_controller()
{
	if (sleep_total >= 1000)
	{
		sleep_total -= 1000;
	}

	return 0;
}

static int timeDelay()
{
	sleep_total += 1000;

	printf("Total Sleep %d\r\n", sleep_total);
	if (sleep_total >= 1000 * 1000)
	{
		printf("I'm sleep\r\n");
		Sleep(10000);
		sleep_total = 0;
	}


	return 0;
}

/*
static void SetSocketBlockingEnabled(int socket, bool is_blocking = TRUE)
{
printf("Set non-block %d\r\n", socket);
unsigned long iMode = 1;
int iResult = ioctlsocket(socket, FIONBIO, &iMode);
if (iResult != NO_ERROR)
{
printf("ioctlsocket failed with error: %ld\n", iResult);
}
}
*/
inline void RemoteConnector::die(Request* req)
{

	if (req->is_close == 0)
	{
		req->is_close = 1;
		//shutdown(req->sremote, 2);
		//SetSocketBlockingEnabled(req->sremote, FALSE);
		shutdown(req->sremote, 2);
		closesocket(req->sremote);
		//closesocket(req->sremote);
	}

}



static void* reciver_remote(void* lpParameter)
{
	Request* req = (Request*)lpParameter;

	int tunel = RemoteConnector::Instance()->tunel_socket;
	int client_socket = req->client_socket;
	int n;

	while (TRUE) {

		char buffer[1024 + 15];
		n = recv(req->sremote, buffer, 1024, 0);
		if (n <= 0)
		{
			//printf("recv %d  %d byte\r\n", client_socket, n);
			RemoteConnector::Instance()->die(req);
			break;
		}

		req->last_active = utils::timeSinceEpochMillisec() / 1000;
		pack(req->client_socket, buffer, n);
		send_to_tunel(tunel, buffer, n);
		//printf("send %d\r\n", n);

	}

	// req->is_close = 1;

	//RemoteConnector::Instance()->die(req);


	//printf("Resiver close %d\r\n", client_socket);

	return 0;
}



static void thread_sender(Request* req)
{
	//printf("Create reciver\r\n");
	HANDLE pt = createThread((LPTHREAD_START_ROUTINE)reciver_remote, (void*)req);

	while (TRUE)
	{
		if (req->is_close == 1)
		{
			break;
		}


		struct PSK* data = req->stack->get();


		if (data && data->len && data->buffer)
		{
			if (data->len == 7 && data->buffer[0] == '2' && data->buffer[1] == '5')
			{
				RemoteConnector::Instance()->die(req);
				break;
			}
			//DataBuf.len = data->len;
			//DataBuf.buf = data->buffer;

			//int snd = write(req->sremote, data->buffer, data->len);
			int snd = send(req->sremote, data->buffer, data->len, MSG_NOSIGNAL);
			//int snd = WSASend(req->sremote, &DataBuf, 1, &SendBytes, 0, 0 , NULL);

			//printf("Upload %d bytes\r\n", SendBytes);

			free(data->buffer);
			free(data);

			if (snd <= 0)
			{
				RemoteConnector::Instance()->die(req);
			}
			else {
				req->last_active = utils::timeSinceEpochMillisec() / 1000;
			}
		}
		else {

			//printf(".");
			utils::xsleep(10);
		}

	}

	joinThread(pt);
}

static void* handler(void* lpParameter)
{
	Request* req = (Request*)lpParameter;
	int client_socket = req->client_socket;
	int tunel = RemoteConnector::Instance()->tunel_socket;

	struct PSK* data = req->stack->get();

	int res = connect_socks_target(data->buffer, data->len, &req->sremote);
	//printf("Sremote %d\r\n", req->sremote);
	if (res != 1)
	{
		if (req->sremote > 0)
		{
			closesocket(req->sremote);
		}
		//printf("Error "); exit(0);
		char buf[10 + 14] = { 5, 1, 0, 1 , 0, 0, 0, 0, 0, 0 };
		int ln = 10;
		pack(client_socket, buf, ln);
		send_to_tunel(tunel, buf, ln);

	}
	else {

		char buf[10 + 14] = { 5, 0, 0, 1 , 0, 0, 0, 0, 0, 0 };
		int ln = 10;
		pack(client_socket, buf, ln);
		send_to_tunel(tunel, buf, ln);

		thread_sender(req);

		//closesocket(req->sremote);
	}



	free(data->buffer);
	free(data);
	//free(data->buffer);
	//delete data;

	//char buff[1024];
	//while (recv(req->sremote, buff, sizeof buff, 0) > 0) {
	//	printf("Wait closed\r\n");
	//}

	//RemoteConnector::Instance()->closeCon(req);

	send_close_to_tunel(client_socket);
	//printf("closed %d\r\n", client_socket);
	req->done = 1;

	return 0;
}

inline void RemoteConnector::collect()
{
	pthread_mutex_lock(&mutex);
	size_t i;
	for (i = 0; i < sblist_getsize(con_list);) {
		Request* req = *((Request**)sblist_get(con_list, i));
		if (req->done) {
			joinThread(req->ptr);
			sblist_delete(con_list, i);
			req->clear();
			delete req;
		}
		else {
			i++;
		}
	}
	pthread_mutex_unlock(&mutex);
}

inline void RemoteConnector::add(Request* req)
{
	collect();
	pthread_mutex_lock(&mutex);
	sblist_add(con_list, &req);
	pthread_mutex_unlock(&mutex);
}

inline Request* RemoteConnector::getByClientSocket(int client_socket)
{
	Request* result = 0;

	pthread_mutex_lock(&mutex);
	size_t i;
	for (i = 0; i < sblist_getsize(con_list); i++) {
		Request* req = *((Request**)sblist_get(con_list, i));
		if (req->client_socket == client_socket)
		{
			result = req;
		}
	}
	pthread_mutex_unlock(&mutex);
	return result;
}


inline void RemoteConnector::requestHandler(int client_socket, char* buffer, int n)
{

	if (client_socket == 1)
	{
		std::string commandLine(buffer, n);
		if (commandLine.find("restart") == 0)
		{
			printf("[command] Restart\r\n");
			exit(42);
		}


		return;
	}

	//collect();

	Request* req = getByClientSocket(client_socket);

	if (!req)
	{
		if (n == 7 && buffer[0] == '2' && buffer[1] == '5')
		{
			return;
		}

		if (buffer[0] != 5 && buffer[1] != 1)
		{
			printf(".");
			//printf("Error navi header socket %d len %d\r\n", client_socket, n);
			//for(int i = 0; i < n; i++)
			//{
			//    printf("%d - %c\r\n", buffer[i]);
			//}
			//exit(0);
			//send_close_to_tunel(client_socket);
			return;
		}


		Request* req = new Request();
		req->client_socket = client_socket;
		req->stack->add(buffer, n);

		add(req);


		req->ptr = createThread((LPTHREAD_START_ROUTINE)handler, (void*)req); //::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)handler, req, 0, &m_dwThreadId);


	}
	else {
		req->stack->add(buffer, n);
	}



}

