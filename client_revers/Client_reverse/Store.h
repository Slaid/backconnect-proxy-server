
#include "model.h"
#include "Singleton.h"
#pragma once

class ConcurentStore
{
//	MODEL(Store)
	DECLARE_SINGLETON(ConcurentStore)
public:

	pthread_mutex_t mutex;

	ConcurentStore()
	{
		pthread_mutex_init(&mutex, NULL);
	}

	struct field_item
	{
		char* key;
		void* val;
		size_t len;
	};

	std::vector< field_item* > _list;

	field_item* get_t(char *key)
	{
		field_item *item = 0;
		for (int i = 0; i < _list.size(); i++)
		{
			if (strcmp(_list[i]->key, key) == 0)
			{
				item = _list[i];
				break;
			}
		}
		return item;
	}

	void set(char *field, void* val, int len = -1)
	{
		pthread_mutex_lock(&mutex);
		field_item *item = get_t(field);
		if (!item)
		{
			item = new field_item;
			item->key = field;
			_list.push_back(item);
		}
		item->val = val;
		item->len = (len <= 0) ? sizeof(val) : len;
		pthread_mutex_unlock(&mutex);
	}

	
	void set(char *field, std::string val)
	{
		this->set(field, (void*)val.data(), val.length());
	}

	std::string getString(char *field)
	{
		std::string res = "";
		pthread_mutex_lock(&mutex);
		field_item *item = get_t(field);
		pthread_mutex_unlock(&mutex);
		if (item)
		{
			res.append((char*)item->val, item->len);
		}
		return res;
	}

	std::string getJson()
	{
		//Computer::currMemInfo();
		//return _getJson();

		return "";
	}	
};