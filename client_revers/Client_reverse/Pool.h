#pragma once

struct mypollfd {
	SOCKET    fd;       /* file descriptor */
	short  events;   /* events to look for */
	short  revents;  /* events returned */
};

int mypoll(struct mypollfd* fds, unsigned int nfds, int timeout) {
	fd_set readfd;
	fd_set writefd;
	fd_set oobfd;
	struct timeval tv;
	unsigned i;
	int num;
	SOCKET maxfd = 0;

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = (timeout % 1000) * 1000;
	FD_ZERO(&readfd);
	FD_ZERO(&writefd);
	FD_ZERO(&oobfd);
	for (i = 0; i < nfds; i++) {
		if ((fds[i].events & POLLIN))FD_SET(fds[i].fd, &readfd);
		if ((fds[i].events & POLLOUT))FD_SET(fds[i].fd, &writefd);
		if ((fds[i].events & POLLPRI))FD_SET(fds[i].fd, &oobfd);
		fds[i].revents = 0;
		if (fds[i].fd > maxfd) maxfd = fds[i].fd;
	}
	if ((num = select(((int)(maxfd)) + 1, &readfd, &writefd, &oobfd, &tv)) < 1) return num;
	for (i = 0; i < nfds; i++) {
		if (FD_ISSET(fds[i].fd, &readfd)) fds[i].revents |= POLLIN;
		if (FD_ISSET(fds[i].fd, &writefd)) fds[i].revents |= POLLOUT;
		if (FD_ISSET(fds[i].fd, &oobfd)) fds[i].revents |= POLLPRI;
	}
	return num;
}