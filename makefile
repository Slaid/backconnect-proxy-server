CC = g++ -std=c++11 -w
SRCS := $(wildcard ./Chat/*.cpp ./Chat/geo/*.cpp)
OBJS := $(patsubst %cpp,%o,$(SRCS))

#https://dev.maxmind.com/geoip/geoip2/geolite2/
 
FRAMEWORK = -I./Chat  
 
GZIP= -I./Chat/lib/zlib/include -L./Chat/lib/zlib/lib/linux/x64 -lz
OPENSSL = 
#OPENSSL = -I./Chat/lib/openssl_linux_x64/include -L./Chat/lib/openssl_linux_x64/lib -lssl -lcrypto
BROTLI = -I./Chat/lib/brotli/include -L./Chat/lib/brotli/lib -lbrotlidec-static -lbrotlienc-static -lbrotlicommon-static
#MYSQL = -I/usr/include/mysql-cppconn -L/usr/lib64 -lmysqlcppconn #centos 8 mysql-connector-c%2B%2B-devel-8.0.18-1.el8.x86_64.rpm
#wget https://downloads.mysql.com/archives/get/p/20/file/mysql-connector-c%2B%2B-devel-8.0.18-1.el8.x86_64.rpm
#rpm -i mysql-connector-c%2B%2B-devel-8.0.18-1.el8.x86_64.rpm
MYSQL = -I/usr/include -L/usr/lib -lmysqlcppconn #mysql 5.7 connector 1.1.9 ubuntu
#MYSQL = -I./Chat/lib/mysql-linux3/include -L./Chat/lib/mysql-linux3/lib64 -lmysqlcppconn8-static
#MYSQL = -I./Chat/lib/mysql_8.0.19/include/mysql-cppconn-8/jdbc -L./Chat/lib -lmysqlcppconn-static
#MYSQL = -I./Chat/lib/mysql-connector-c++-1.1.9-linux-el7-x86-64bit/include -L./Chat/lib/mysql-connector-c++-1.1.9-linux-el7-x86-64bit/lib -lmysqlcppconn
#wget https://dev.mysql.com/get/Downloads/Connector-C++/mysql-connector-c++-1.1.9-linux-el7-x86-64bit.rpm
#rpm -Uvh mysql-connector-c++-1.1.9-linux-el7-x86-64bit.rpm
#yum install boost-devel

all : $(OBJS)
	$(CC) -g -o ./Release/xChat $^ -lpthread -lmaxminddb $(FRAMEWORK) $(OPENSSL) $(BROTLI) $(GZIP) $(MYSQL)

%.o: %.cpp
	$(CC) -g $(OPENSSL) $(BROTLI) $(GZIP) $(MYSQL) -c  $<    -o   $@   $(FRAMEWORK)   -DLINUX

clean :
	rm ./Chat/*.o
	rm ./Chat/geo/*.o
	